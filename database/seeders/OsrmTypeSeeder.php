<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Osrm;

class OsrmTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Osrm::query()
            ->create([
                'current_type' => 'MLD',
                'next_type' => 'MLD_2',
            ]);
    }
}
