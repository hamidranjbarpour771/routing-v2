<?php

namespace Database\Seeders;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // DB::statement('ALTER TABLE traffics ADD COLUMN nodes bigint[]');
        // DB::statement('ALTER TABLE traffics ADD COLUMN lats integer[]');
        // DB::statement('ALTER TABLE traffics ADD COLUMN lons integer[]');

        $this->call([
            OsrmTypeSeeder::class,
        ]);
    }
}
