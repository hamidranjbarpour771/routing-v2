<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrafficRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffic_requests', function (Blueprint $table) {
            $table->id();
            $table->string('action');
            $table->string('status')->default('pending'); // pending, queued, completed, failed
            $table->timestamps();
        });

        DB::statement('ALTER TABLE traffic_requests ADD COLUMN ways bigint[]');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traffic_requests');
    }
}
