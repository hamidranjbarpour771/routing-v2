<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoutingLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('routing_logs', function (Blueprint $table) {
            $table->id();
            $table->string('ip')->nullable();
            $table->string('src_name')->nullable();
            $table->double('src_lat')->nullable();
            $table->double('src_lon')->nullable();
            $table->string('dest_name')->nullable();
            $table->double('dest_lat')->nullable();
            $table->double('dest_lon')->nullable();
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routing_logs');
    }
}
