<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddGeometryToTrafficsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('traffics', function (Blueprint $table) {
            $table->geometry('geometry')->nullable();
            $table->integer('type')->nullable()->comment('1 => traffic, 2 => oneway, 3 => blockade');
            $table->integer('dir')->nullable()->comment('1 => direct, 0 => reverse');
            $table->integer('operator')->nullable();
            $table->string('name', 300)->nullable();
            // column nodes[] array type exists in DatabaseSeeder.php
        });

        DB::statement('ALTER TABLE traffics ADD COLUMN nodes bigint[]');
        DB::statement('ALTER TABLE traffics ADD COLUMN lats integer[]');
        DB::statement('ALTER TABLE traffics ADD COLUMN lons integer[]');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('traffics', function (Blueprint $table) {
            $table->dropColumn(['geometry', 'type', 'dir', 'operator', 'name', 'nodes', 'lats', 'lons']);
        });
    }
}
