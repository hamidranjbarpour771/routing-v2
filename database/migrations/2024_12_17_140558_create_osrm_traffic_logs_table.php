<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOsrmTrafficLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('osrm_traffic_logs', function (Blueprint $table) {
            $table->id();
            $table->string('action')->nullable();
            $table->string('osrm_type', 9)->nullable();
            $table->string('dir')->nullable();
            $table->text('ways')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('osrm_traffic_logs');
    }
}
