<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToRoutingLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routing_logs', function (Blueprint $table) {
            $table->string('src_place')->nullable();
            $table->string('src_place_tags')->nullable();
            $table->string('dest_place')->nullable();
            $table->string('dest_place_tags')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routing_logs', function (Blueprint $table) {
            $table->dropColumn(['src_place', 'src_place_tags', 'dest_place', 'dest_place_tags']);
        });
    }
}
