<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToTrafficsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('traffics', function (Blueprint $table) {
            $table->integer('status')->nullable()->comment('1 => روان,
                                                            2 => نیمه سنگین,
                                                            3 => سنگین,
                                                            4 => راه بندان,
                                                            5 => نامشخص');
            // $table->string('status_fa', 20)->nullable(); // [[status => 1, status_fa => روان], [status => 2, status_fa =>  نیمه سنگین], [status => 3, status_fa =>  سنگین], [status => 4, status_fa =>  راه بندان], [status => 5, status_fa =>  نامشخص]]
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('traffics', function (Blueprint $table) {
            $table->dropColumn(['status']);
        });
    }
}
