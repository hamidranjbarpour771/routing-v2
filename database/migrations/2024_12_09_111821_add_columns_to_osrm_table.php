<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsToOsrmTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('osrm', function (Blueprint $table) {
            $table->renameColumn('mld_type', 'current_type');
            $table->string('next_type', 9)->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('osrm', function (Blueprint $table) {
            $table->renameColumn('current_type', 'mld_type');
            $table->dropColumn('next_type');
        });
    }
}
