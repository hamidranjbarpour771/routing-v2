<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddStateToRoutingLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('routing_logs', function (Blueprint $table) {
            $table->string('src_place_state')->nullable();
            $table->string('dest_place_state')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('routing_logs', function (Blueprint $table) {
            $table->dropColumn(['src_place_state', 'dest_place_state']);
        });
    }
}
