<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTrafficsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('traffics', function (Blueprint $table) {
            $table->id();
            // $table->text('way_speed');   // Since laravel doesn't support array type, I added array column type with raw sql in DatabaseSeeder.php
            $table->string('way_id');
            $table->integer('current_speed')->nullable();
            $table->string('highway_type');
            $table->integer('highway_maxspeed')->nullable();
            $table->string('src_lat')->nullable();
            $table->string('src_lon')->nullable();
            $table->string('d_lat')->nullable();
            $table->string('d_lon')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('traffics');
    }
}
