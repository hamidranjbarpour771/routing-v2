<?php

return [
    'NOMINATIM_URL' => env('NOMINATIM_URL'),
    'NOMINATIM_DEFAULT_ZOOM' => 16,
    'API_KEY' => env('API_KEY'),

    'osrm_traffic_script_path' => env('OSRM_TRAFFIC_SCRIPT_PATH', "/srv/osrm/osrm-backend"),

    'mld' => [
        'url' => env('OSRM_MLD_URL'),
        'pbf_path' => env('OSRM_MLD_PBF_PATH'),
        'systemctl_name' => env('OSRM_MLD_SYSTEMCTL_NAME'),
    ],
    'mld_2' => [
        'url' => env('OSRM_MLD_2_URL'),
        'pbf_path' => env('OSRM_MLD_2_PBF_PATH'),
        'systemctl_name' => env('OSRM_MLD_2_SYSTEMCTL_NAME'),
    ],
];