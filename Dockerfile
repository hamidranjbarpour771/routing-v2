FROM alpine:3.18

ARG CONTAINER_PATH

RUN apk update && apk add --no-cache\
    bash \
    wget  \
    curl  \
    nano \
    unzip \
    libzip-dev \
    libxml2 \
    libxml2-dev \
    git \
    zlib-dev \
    libpng-dev \
    php81 \
    php81-common \
    php81-fpm \
    php81-pdo \
    php81-opcache \
    php81-soap \
    php81-zip \
    php81-phar \
    php81-iconv \
    php81-cli \
    php81-curl \
    php81-openssl \
    php81-mbstring \
    php81-tokenizer \
    php81-fileinfo \
    php81-json \
    php81-xml \
    php81-xmlwriter \
    php81-xmlreader \
    php81-simplexml \
    php81-dom \
    php81-pdo_pgsql \
    php81-pdo_sqlite \
    php81-tokenizer \
    php81-pecl-redis \
    php81-gd \
    php81-xdebug\
    postgresql \
    postgresql-dev \
    postgis


# Get latest Composer
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer

ENV COMPOSER_ALLOW_SUPERUSER=1

COPY . $CONTAINER_PATH
# Set working directory
WORKDIR $CONTAINER_PATH

CMD composer install ;\
    php artisan key:generate ;\
    echo "xdebug.mode=develop,debug,coverage" >> /etc/php/81/cli/php.ini;\
    php artisan migrate ;\
    php artisan serve --host=0.0.0.0
