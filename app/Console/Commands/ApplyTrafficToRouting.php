<?php

namespace App\Console\Commands;

use App\Services\NominatimService;
use Illuminate\Console\Command;


class ApplyTrafficToRouting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'osrm:apply-traffic';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Gets blocked points from Nikarayan and tries to apply traffic to ways between those points on OSRM routing engine';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle(NominatimService $nominatimService)
    {
        // 1. Get two points 
        // 2. Get ways of those (nominatim) 
        // 3. if two ways are equal do next steps only for one.
        // 4. Get nodes of those ways. 
        // 5. make traffic file from nodes
        // 6. execute osrm script

        // you should only have a list of unique ways in each time.

        $startLat = '36.10560698';
        $startLon = '52.27538152';
        $endLat = '36.09650454';
        $endLon = '52.26773217';


        $startWay = $nominatimService->getWayId($startLat, $startLon);
        $endWay = $nominatimService->getWayId($endLat, $endLon);

        if(is_null($startWay) || is_null($endWay)){
            \Log::channel('blockade')->error('Process Failed!', ['message' => "point: ($startLat, $startLon) or point ($endLat, $endLon) does not have a way on nominatim!"]);
            return 1;
        }
        elseif ($startWay == $endWay) {
            
        }
    }
}
