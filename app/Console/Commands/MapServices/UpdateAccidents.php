<?php

namespace App\Console\Commands\MapServices;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\MapService;
use App\Models\MapServicesV2;

class UpdateAccidents extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'map-services:update-accidents';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Accidents';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = array();
        // $response = Http::post('https://141.ir/api/roadaccident');

        // foreach ($response->json() as $r) {
        //     $data[] = [
        //         'lat'  => $r['lat'],
        //         'lon'  => $r['lng'],
        //         'type' => 'roadaccident',
        //         'org_id' => $r['id'],
        //         'attrs' => json_encode($r),
        //         'way_id' => MapService::setWayID($r['lat'], $r['lng']),
        //         'created_at' => now(),
        //         'updated_at' => now()
        //     ];
        // }

        // MapService::where('type', 'roadaccident')->delete();
        // MapService::insert($data);

        // BEGIN NEW REFACTOR
        $url = env('141_URL'). '/accidents';
        $response = Http::get($url);
        $result = array();
        $ids = array();

        foreach ($response->json()['data'] as $data) {
            MapServicesV2::updateOrCreate([
                'org_id' => $data[0],
                'type' => 'accidents',
            ], [
                'org_id' => $data[0],
                'type' => 'accidents',
                'lat' => $data[1],
                'lon' => $data[2],
            ]);

            $ids[] = $data[0];
        }

        MapServicesV2::where('type', 'accidents')
                  ->whereNotIn('org_id', $ids)
                  ->delete();
        // END NEW REFACTOR 
        
        $this->info(now(). ' Accidents updated succussfully');
    }
}
