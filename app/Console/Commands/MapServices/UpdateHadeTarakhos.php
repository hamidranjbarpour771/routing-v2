<?php

namespace App\Console\Commands\MapServices;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\MapServicesV2;

class UpdateHadeTarakhos extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'map-services:update-hadde-tarakhos';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Hadde Tarakhos';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = env('141_URL'). '/hadde_tarakhos';
        $response = Http::get($url);
        $result = array();
        $ids = array();
        
        foreach ($response->json()['data'] as $data) {
            MapServicesV2::updateOrCreate([
                'org_id' => $data[0],
                'type' => 'hadde_tarakhos',
            ], [
                'org_id' => $data[0],
                'type' => 'hadde_tarakhos',
                'lat' => $data[1],
                'lon' => $data[2],
            ]);

            $ids[] = $data[0];
        }

        MapServicesV2::where('type', 'hadde_tarakhos')
                  ->whereNotIn('org_id', $ids)
                  ->delete();
        
        $this->info(now() .' Hadde Tarakhos updated succussfully');
        return 0;
    }
}
