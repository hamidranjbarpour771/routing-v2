<?php

namespace App\Console\Commands\MapServices;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\MapServicesV2;

class UpdateTolls extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'map-services:update-tolls';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Tolls';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $url = env('141_URL'). '/tolls';
        $response = Http::get($url);
        $result = array();
        $ids = array();
        
        foreach ($response->json()['data'] as $data) {
            MapServicesV2::updateOrCreate([
                'org_id' => $data[0],
                'type' => 'tolls',
            ], [
                'org_id' => $data[0],
                'type' => 'tolls',
                'lat' => $data[1],
                'lon' => $data[2],
            ]);

            $ids[] = $data[0];
        }

        MapServicesV2::where('type', 'tolls')
                  ->whereNotIn('org_id', $ids)
                  ->delete();
        
        $this->info(now() .' Tolls updated succussfully');
        return 0;
    }
}
