<?php

namespace App\Console\Commands\MapServices;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\MapService;
use App\Models\MapServicesV2;

class UpdateWeathers extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'map-services:update-weathers';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Weathers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // BEGIN NEW REFACTOR
        $url = env('141_URL'). '/weather';
        $response = Http::get($url);
        $result = array();
        $ids = array();
        
        foreach ($response->json()['data'] as $data) {
            MapServicesV2::updateOrCreate([
                'org_id' => $data[0],
                'type' => 'weather',
            ], [
                'org_id' => $data[0],
                'type' => 'weather',
                'lat' => $data[1],
                'lon' => $data[2],
                'subtype' => $data[3],
            ]);

            $ids[] = $data[0];
        }

        MapServicesV2::where('type', 'weather')
                  ->whereNotIn('org_id', $ids)
                  ->delete();
        // END NEW REFACTOR 
        
        $this->info(now() .' Weathers updated succussfully');
    }
}
