<?php

namespace App\Console\Commands\MapServices;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\MapService;
use App\Models\MapServicesV2;

class UpdateCameras extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'map-services:update-cameras';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Cameras';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // $response = Http::post('https://141.ir/api/camera');
        // $ids = array();

        // foreach ($response->json() as $r) {
        //     $ms = MapService::where([
        //         ['type', 'cameras'],
        //         ['lat', $r['lat']],
        //         ['lon', $r['lng']],
        //     ])->first();

        //     if (isset($ms)) {
        //         $ms->update([
        //             'org_id' => $r['id'],
        //             'attrs'  => json_encode($r),
        //             'active' => 1
        //         ]);
        //     } else {
        //         $ms = MapService::create([
        //             'lat'  => $r['lat'],
        //             'lon'  => $r['lng'],
        //             'type' => 'cameras',
        //             'org_id' => $r['id'],
        //             'attrs' => json_encode($r),
        //             'active' => 1,
        //             'way_id' => MapService::setWayID($r['lat'], $r['lng']),
        //         ]);
        //     }

        //     $ids[] = $ms->id;
        // }

        // MapService::where('type', 'cameras')
        //           ->whereNotIn('id', $ids)
        //           ->update([
        //               'active' => 0
        //           ]);

        // BEGIN NEW REFACTOR
        $url = env('141_URL') . '/map_services/cameras';
        $response = Http::get($url);
        $ids = array();

        foreach ($response->json()['data'] as $data) {
            MapServicesV2::updateOrCreate([
                'org_id' => $data[0],
                'type' => 'cameras',
            ], [
                'org_id' => $data[0],
                'type' => 'cameras',
                'lat' => $data[1],
                'lon' => $data[2],
                'active' => 1,
            ]);

            $ids[] = $data[0];
        }

        MapServicesV2::where('type', 'cameras')
            ->whereNotIn('org_id', $ids)
            ->update([
                'active' => 0
            ]);
        // END NEW REFACTOR


        $this->info(now() . ' Cameras updated succussfully');
    }
}
