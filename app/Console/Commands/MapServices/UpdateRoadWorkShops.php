<?php

namespace App\Console\Commands\MapServices;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\MapService;
use App\Models\MapServicesV2;

class UpdateRoadWorkShops extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'map-services:update-roadworkshops';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Road-WorkShops';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = array();
        // $response = Http::post('https://141.ir/api/roadoperation');

        // foreach ($response->json() as $r) {
        //     $data[] = [
        //         'lat'  => $r['lat'],
        //         'lon'  => $r['lng'],
        //         'type' => 'roadoperation',
        //         'org_id' => $r['id'],
        //         'attrs' => json_encode($r),
        //         'way_id' => MapService::setWayID($r['lat'], $r['lng']),
        //         'created_at' => now(),
        //         'updated_at' => now()
        //     ];
        // }

        // MapService::where('type', 'roadoperation')->delete();
        // MapService::insert($data);

        // BEGIN NEW REFACTOR
        $url = env('141_URL'). '/road_workshops';
        $response = Http::get($url);
        $result = array();
        
        foreach ($response->json()['data'] as $data) {
            $result[] = [
                'org_id' => $data[0],
                'type' => 'road_workshops',
                'lat' => $data[1],
                'lon' => $data[2],
                'created_at' => now(),
                'updated_at' => now()
            ];
        }

        MapServicesV2::where('type', 'road_workshops')->delete();
        MapServicesV2::insert($result);
        // END NEW REFACTOR 
        
        $this->info(now() .' Road Operations updated succussfully');
    }
}
