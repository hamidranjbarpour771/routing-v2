<?php

namespace App\Console\Commands\MapServices;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\MapService;
use App\Models\MapServicesV2;

class UpdateWelfareServices extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'map-services:update-welfare';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Welfare Services';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        $data = array();
        // $response = Http::post('https://141.ir/api/complex');

        // foreach ($response->json() as $r) {
        //     $ms = MapService::where([
        //         ['type', 'welfare_services'],
        //         ['lat', $r['lat']],
        //         ['lon', $r['lng']],
        //     ])->first();

        //     if (isset($ms)) {
        //         $ms->update([
        //             'org_id' => $r['id'],
        //             'attrs'  => json_encode($r),
        //         ]);
        //     } else {
        //         $ms = MapService::create([
        //             'lat'  => $r['lat'],
        //             'lon'  => $r['lng'],
        //             'type' => 'welfare_services',
        //             'org_id' => $r['id'],
        //             'attrs' => json_encode($r),
        //             'way_id' => MapService::setWayID($r['lat'], $r['lng']),
        //         ]);
        //     }
        // }

        // BEGIN NEW REFACTOR
        $url = env('141_URL') . '/welfare_services';
        $response = Http::get($url);
        $result = array();
        $ids = array();

        foreach ($response->json()['data'] as $data) {
            MapServicesV2::updateOrCreate([
                'org_id' => $data[0],
                'type' => 'welfare_services',
            ], [
                    'org_id' => $data[0],
                    'type' => 'welfare_services',
                    'lat' => $data[1],
                    'lon' => $data[2],
                ]);

            $ids[] = $data[0];
        }

        MapServicesV2::where('type', 'welfare_services')
            ->whereNotIn('org_id', $ids)
            ->delete();
        // END NEW REFACTOR 

        $this->info(now() . ' Welfare Services updated succussfully');
    }
}