<?php

namespace App\Console\Commands\MapServices;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;
use App\Models\MapService;
use App\Models\MapServicesV2;

class UpdateCarAssistances extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'map-services:update-car-assistances';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Car Assistances';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
        // BEGIN NEW REFACTOR
        $url = env('141_URL'). '/car_assistances';
        $response = Http::get($url);
        $result = array();
        $iran_khodro_ids = array();
        $saipa_ids = array();
        foreach ($response->json()['data'] as $data) {

            if($data[3] == 1) {
                // iran khodro
                MapServicesV2::updateOrCreate([
                    'org_id' => $data[0],
                    'type' => 'iran_car_assistances',
                ], [
                    'org_id' => $data[0],
                    'type' => 'iran_car_assistances',
                    'lat' => $data[1],
                    'lon' => $data[2],
                    'subtype' => 1,
                    
                ]);

                $iran_khodro_ids[] = $data[0];
            }
            else{
                // saipa
                MapServicesV2::updateOrCreate([
                    'org_id' => $data[0],
                    'type' => 'saipa_car_assistances',
                ], [
                    'org_id' => $data[0],
                    'type' => 'saipa_car_assistances',
                    'lat' => $data[1],
                    'lon' => $data[2],
                    'subtype' => 2,
                ]);

                $saipa_ids[] = $data[0];
            }
        }

        MapServicesV2::where('type', 'iran_car_assistances')
                  ->whereNotIn('org_id', $iran_khodro_ids)
                  ->delete();

        MapServicesV2::where('type', 'saipa_car_assistances')
                ->whereNotIn('org_id', $saipa_ids)
                ->delete();                  
        // END NEW REFACTOR 
        
        $this->info(now() .' Car assistances updated succussfully');
    }
}
