<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\RoutingLog;
use App\Customs\Nominatim;
use \App\Services\Nominatim\ReverseService;
use App\Services\Nominatim\Exceptions\ReverseException;
class UpdateRoutingLog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'routing-logs:update-addresses';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update source and destination addresses of routing logs';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // 1. bulk update src lats
        // 2. update remaining src lats based on nominatim reverse service
        // 3.  bulk update src lons
        // 4.  update remaining dest lats based on nominatim reverse service



        $sourceLogs = RoutingLog::query()
            ->whereNull('src_place')
            ->groupBy('src_lat', 'src_lon')
            ->selectRaw('count(*), src_lat, src_lon')
            ->get();

        foreach ($sourceLogs as $sourceGroup) {
            
            try{
                $lat =  $sourceGroup->src_lat;
                $lon =  $sourceGroup->src_lon;
                dump($lat, $lon);
                
                $reverseService = new ReverseService($lat, $lon);
                $placeTag = $reverseService->getPlaceTag();
                $placeName = $reverseService->getPlaceName();
                $state = $reverseService->getState();

                if($placeTag && $placeName){
                    RoutingLog::query()
                        ->where(\DB::raw('src_lat::float'), $lat)
                        ->where(\DB::raw('src_lon::float'), $lon)
                        ->update([
                            'src_place' => $placeName,
                            'src_place_tags' => $placeTag,
                            'src_place_state' => $state
                        ]);

                    RoutingLog::query()
                        ->where(\DB::raw('dest_lat::float'), $lat)
                        ->where(\DB::raw('dest_lon::float'), $lon)
                        ->update([
                            'dest_place' => $placeName,
                            'dest_place_tags' => $placeTag,
                            'dest_place_state' => $state
                        ]);
                }
            }
            catch(ReverseException $exception){
                continue;
            }
            
        }

        return 0;
    }

    /**
     * THIS FUNCTION SELECTS LAT, LON AND GETS ITS CITY FROM NOMINATIM THEN UPDATES ROUTING LOG'S PLACE NAME.
     */

     /**
      * This function selects src_lat, src_lon from routing_logs AND gets its place name and place tag from Nominatim then updates routing logs's source place.
      * @param $tags list of place tags to update coordinates. in order: [county, city, town, state_district]
      * @param $side determines which side to update. either `source` or `dest`.
      */
    private function bulkUpdate($tags, $side){
        
        foreach ($tags as $tag) {
            
            // update tags in order 

            $sourceLogsSql = "UPDATE routing_logs set src_place = qq.city, src_place_tags = '$tag' 
            from (SELECT placename as city, src_lat, src_lon, rid, extratags_place, src_place from (
                SELECT place_id,
                    get_name_by_language(name,ARRAY['name:fa','name','brand','official_name:fa','short_name:fa','official_name','short_name','ref','type']) AS placename,
                    src_lat, src_lon, rid, src_place, extratags_place
                     FROM
                    (select src_lon,src_lat, src_place, routing_logs.id rid, place_id,
                            parent_place_id, rank_address, rank_search, country_code, geometry,name, extratags->'place' as extratags_place
                     FROM placex,routing_logs 
                     WHERE ST_GeometryType(geometry) in ('ST_Polygon', 'ST_MultiPolygon') 
                          And src_place is null
                         AND ( ((geometry && ST_SetSRID(ST_Point(src_lon,src_lat),4326)))
                         AND placex.type != 'postcode'
                          and ((extratags->'place') = '$tag')
                         AND name is not null
                          AND indexed_status = 0 and linked_place_id is null)
                      ) as a
                 WHERE ST_CONTAINS(geometry, ST_SetSRID(ST_Point(src_lon,src_lat),4326))
            )q )qq where qq.rid=routing_logs.id  and routing_logs.src_place is null";


            dump("(start) update tag: $tag");

            \DB::statement();

            dump("(end) update tag: $tag");
            // execute query

            // repeat for dest logs
        }
    }
}



