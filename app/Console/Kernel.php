<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')->hourly();
        $schedule->command('map-services:update-cameras')->everyFiveMinutes()->appendOutputTo(storage_path('logs/schedules/cameras.log'));
        $schedule->command('map-services:update-accidents')->everyFiveMinutes()->appendOutputTo(storage_path('logs/schedules/accidents.log'));
        $schedule->command('map-services:update-obstructions')->everyFiveMinutes()->appendOutputTo(storage_path('logs/schedules/obstructions.log'));
        $schedule->command('map-services:update-roadworkshops')->everyFiveMinutes()->appendOutputTo(storage_path('logs/schedules/roadworkshops.log'));
        $schedule->command('map-services:update-otfs')->everyFiveMinutes()->appendOutputTo(storage_path('logs/schedules/otfs.log'));
        $schedule->command('map-services:update-weathers')->everyFiveMinutes()->appendOutputTo(storage_path('logs/schedules/weathers.log'));
        $schedule->command('map-services:update-car-assistances')->everyFiveMinutes()->appendOutputTo(storage_path('logs/schedules/car-assistances.log'));

        $schedule->command('map-services:update-petrol-stations')->dailyAt('01:00')->appendOutputTo(storage_path('logs/schedules/petrol-stations.log'));
        $schedule->command('map-services:update-tollhouses')->dailyAt('01:00')->appendOutputTo(storage_path('logs/schedules/tollhouses.log'));
        $schedule->command('map-services:update-hospitals')->dailyAt('01:00')->appendOutputTo(storage_path('logs/schedules/hospitals.log'));
        $schedule->command('map-services:update-mosques')->dailyAt('01:00')->appendOutputTo(storage_path('logs/schedules/mosques.log'));
        $schedule->command('map-services:update-repairshops')->dailyAt('01:00')->appendOutputTo(storage_path('logs/schedules/repairshops.log'));
        $schedule->command('map-services:update-welfare')->dailyAt('01:00')->appendOutputTo(storage_path('logs/schedules/welfare.log'));

        $schedule->command('map-services:update-tolls')->monthly()->appendOutputTo(storage_path('logs/schedules/tolls.log'));
        $schedule->command('map-services:update-hadde-tarakhos')->monthly()->appendOutputTo(storage_path('logs/schedules/hadde-tarakhos.log'));
        
        
        // $schedule->command('telescope:prune')->daily();


    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
