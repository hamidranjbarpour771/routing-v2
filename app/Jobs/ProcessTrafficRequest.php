<?php

namespace App\Jobs;

use App\Models\TrafficRequest;
use App\Services\ApplyTrafficService;
use App\Services\Exceptions\IsUpdatingTrafficException;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldBeUnique;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Cache;

use App\Services\HandleOsrmTraffic;

class ProcessTrafficRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(
        private TrafficRequest $trafficRequest
    ) {
        //
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle(HandleOsrmTraffic $handleOsrmTraffic, ApplyTrafficService $applyTrafficService)
    {
        try {

            if (Cache::has('is-updating-traffic')) {
                throw new IsUpdatingTrafficException('Routing service is busy applying traffic. try later');
            }

            $ways = implode(',', $this->trafficRequest->ways);
            $directNodes = \DB::connection('pgsql3')
                ->select("SELECT id as way_id, unnest(nodes) as node from planet_osm_ways where id in ($ways)");

            if ($this->trafficRequest->action == 'blockade')
                $speed = 0;
            else
                $speed = null;


            $handleOsrmTraffic->setAction($this->trafficRequest->action);
            $handleOsrmTraffic->setWays($this->trafficRequest->ways);
            $handleOsrmTraffic->setNodes($directNodes);
            $handleOsrmTraffic->setSpeed($speed);

            $handleOsrmTraffic->run($applyTrafficService);

            $this->trafficRequest->update([
                'status' => 'completed'
            ]);

        } catch (\Throwable $exception) {

            $this->trafficRequest->update([
                'status' => 'failed'
            ]);

            throw $exception;
        }
    }
}
