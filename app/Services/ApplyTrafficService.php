<?php

namespace App\Services;

use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

class ApplyTrafficService
{
    const LOG_CHANNEL_NAME = 'osrm_traffic';

    private string $trafficPath;

    private string $osrmType;
    private string $pbfPath;
    private string $serviceName;


    public function setTrafficPath(string $trafficPath)
    {
        $this->trafficPath = $trafficPath;
    }

    public function setPbfPath(string $pbfPath)
    {
        $this->pbfPath = $pbfPath;
    }

    public function setServiceName(string $serviceName)
    {
        $this->serviceName = $serviceName;
    }

    public function setOsrmType(string $osrmType)
    {
        $this->osrmType = $osrmType;
    }


    public function run()
    {

        // Change directory to the script path
        $osrmScriptPath = config('global_variables.osrm_traffic_script_path');
        if (!chdir($osrmScriptPath)) {
            throw new RuntimeException("Failed to change directory to " . $osrmScriptPath);
        }

        if ($this->osrmType == 'MLD') {
            $pbfPath = config('global_variables.mld.pbf_path');
            $serviceName = config('global_variables.mld.systemctl_name');

        } elseif ($this->osrmType == 'MLD_2') {

            $pbfPath = config('global_variables.mld_2.pbf_path');
            $serviceName = config('global_variables.mld_2.systemctl_name');
        }


        $command = ['./osrm-script.sh', $pbfPath, $serviceName, '-t', $this->trafficPath];


        \Log::channel(self::LOG_CHANNEL_NAME)
            ->info("---------Trying to apply traffic to instance: $this->osrmType----------");

        // Execute the OSRM traffic script
        $this->runCommand($command, self::LOG_CHANNEL_NAME);
    }

    private function runCommand(array $command, $logPath)
    {
        $process = new Process($command);

        $process->setTimeout(null); // Remove time limit if the command might take time
        try {
            $process->mustRun();

            \Log::channel($logPath)
                ->info($process->getOutput());


        } catch (ProcessFailedException $e) {
            \Log::channel($logPath)
                ->error($e->getMessage());

            throw new ProcessFailedException($process);
        }
    }

}