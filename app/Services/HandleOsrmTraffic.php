<?php

namespace App\Services;

use App\Models\OsrmTrafficLog;
use Illuminate\Support\Facades\Cache;
use App\Services\ApplyTrafficService;
use App\Models\Osrm;

use App\Services\MakeTrafficFileService;


class HandleOsrmTraffic
{
    const LOG_CHANNEL_NAME = 'osrm_traffic';

    private string $action;
    private array $ways;
    private array $nodes;
    private int $speed;

    /**
     * Create a new job instance.
     *
    //  * @return void
     */
    // public function __construct(
    //     // private Osrm $osrm,
    //     // private string $trafficPath,
    //     private string $action,
    //     private array $ways,
    //     private array $nodes,
    //     private int $speed
    // ) {
    // }

    public function setAction(string $action)
    {
        $this->action = $action;
    }

    public function setWays(array $ways)
    {
        $this->ways = $ways;
    }

    public function setNodes(array $nodes)
    {
        $this->nodes = $nodes;
    }

    public function setSpeed(int $speed)
    {
        $this->speed = $speed;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function run(ApplyTrafficService $applyTrafficService)
    {
        try {
            Cache::add('is-updating-traffic', true, now()->addMinutes(6));

            $trafficFile = MakeTrafficFileService::create(
                $this->nodes,
                $this->speed,
            );

            /** determine to which osrm instance the traffic should be applied. */
            $osrm = Osrm::query()
                ->first();

            $applyTrafficService->setOsrmType($osrm->next_type);
            $applyTrafficService->setTrafficPath($trafficFile);
            $applyTrafficService->run();

            \Log::channel(self::LOG_CHANNEL_NAME)
                ->info("(HandleOsrmTraffic) OSRM preprocessing for instance {$osrm->next_type} completed successfully.");

            \DB::transaction(function () use ($osrm) {
                // at very last, store only those ways that you ensure you applied to osrm as blockade.
                OsrmTrafficLog::query()
                    ->create([
                        'action' => $this->action,
                        'osrm_type' => $osrm->next_type,
                        'dir' => 'both',
                        'ways' => implode(',', $this->ways),
                    ]);

                //switch to next osrm instance
                $osrm->update([
                    'current_type' => $osrm->next_type,
                    'next_type' => $osrm->current_type,
                ]);
            });


        } catch (ProcessFailedException | RuntimeException $exception) {
            throw $exception;
        } finally {
            Cache::forget('is-updating-traffic');
        }

    }
}
