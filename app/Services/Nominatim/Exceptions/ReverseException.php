<?php

namespace App\Services\Nominatim\Exceptions;

use Exception;

class ReverseException extends Exception
{
    private $lat, $lon; 

    public function __construct($lat, $lon, $message = "", $code = 400, \Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->lat = $lat;
        $this->lon = $lon;
    }

    /**
     * Report the exception.
     */
    public function report(): void
    {
        \Log::channel('nominatim')
            ->error(ReverseException::class, [$this->getMessage(), [], [
                'lat' => $this->lat,
                'lon' => $this->lon,
            ]]);
    }

}
