<?php

namespace App\Services\Nominatim;

use App\Services\Nominatim\Exceptions\ReverseException;
use Illuminate\Support\Facades\Http;

class ReverseService
{
    private string $url;
    private $lat, $lon;
    private int $zoom;
    private array $result;
    private ?string $placeName;
    private ?string $placeTag;
    private ?string $state;

    /**
     * @param mixed $lat latitude of point
     * @param mixed $lon longitude of point
     * @param string $url base url of nominatim server. 
     * @param string $zoom
     */
    public function __construct($lat, $lon, string $url = null, int $zoom = null)
    {
        $this->lat = $lat;
        $this->lon = $lon;

        $this->url = $url ?? config('global_variables.NOMINATIM_URL');
        $this->url = $this->url . '/reverse';

        $this->zoom = $zoom ?? config('global_variables.NOMINATIM_DEFAULT_ZOOM');

        $this->run();
        $this->setPlace();
        $this->setState();
    }

    public function run()
    {
        $response = Http::get($this->url, [
            'lat' => $this->lat,
            'lon' => $this->lon,
            'zoom' => $this->zoom,
            'format' => 'json',
        ]);

        $result = $response->json();

        if(array_key_exists('error', $result)){
            throw new ReverseException($this->lat, $this->lon, $result['error']);
        }

        $this->result = $result;
    }
    
    public function setPlace()
    {
        $address = $this->getAddress();

        if(array_key_exists('county', $address)){
            $this->placeTag = 'county';
            $this->placeName = $address['county'];
        }
        elseif(array_key_exists('city', $address)){
            $this->placeTag = 'city';
            $this->placeName = $address['city'];
        }
        elseif(array_key_exists('town', $address)){
            $this->placeTag = 'town';
            $this->placeName = $address['town'];
        }
        elseif(array_key_exists('state_district', $address)){
            $this->placeTag = 'state_district';
            $this->placeName = $address['state_district'];
        }
        else{
            $this->placeTag = null;
            $this->placeName = null;
        }
    }

    public function setState()
    {
        $address = $this->getAddress();

        if(array_key_exists('state', $address)){
            $this->state = $address['state'];
        }
        elseif(array_key_exists('province', $address)){
            $this->state = $address['province'];
        }else{
            $this->state = null;
        }
    }

    public function getAddress()
    {
        return $this->result['address'];
    }


    public function getPlaceTag()
    {
        return $this->placeTag;
    }

    public function getPlaceName()
    {
        return $this->placeName;
    }

    public function getState()
    {
        return $this->state;
    }
}
