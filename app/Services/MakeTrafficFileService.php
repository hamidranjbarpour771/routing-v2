<?php

namespace App\Services;

use Illuminate\Support\Facades\Storage;
use App\Services\Exceptions\InvalidWayIDsException;


class MakeTrafficFileService
{
    // const LOG_CHANNEL_NAME = 'shell_traffic_MLD';

    public static function create($directNodes, $speed)
    {
        $final_txt = '';

        for ($i = 0; $i < count($directNodes) - 1; $i++) {
            if ($directNodes[$i]->way_id == $directNodes[$i + 1]->way_id) {
                // write to file like nodes[$key] , nodes[$key + 1] , 0
                $final_txt .= $directNodes[$i]->node . ',' . $directNodes[$i + 1]->node . ',' . $speed . "\n";
            }
        }

        // also block ways in opposite direction
        for ($i = count($directNodes) - 1; $i > 0; $i--) {
            if ($directNodes[$i]->way_id == $directNodes[$i - 1]->way_id) {
                $final_txt .= $directNodes[$i]->node . ',' . $directNodes[$i - 1]->node . ',' . $speed . "\n";
            }
        }

        $fileName = 'traffic_' . date('Y-m-d-H:i') . '.csv';

        $result = Storage::disk('traffic')->put($fileName, $final_txt);

        if ($result) {
            $trafficFile = base_path() . Storage::disk('traffic')->url($fileName);
            return $trafficFile;

        } else {
            throw new \Exception("Could not create traffic file");
        }

    }

}