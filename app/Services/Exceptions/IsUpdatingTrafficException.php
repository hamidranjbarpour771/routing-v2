<?php

namespace App\Services\Exceptions;

use Exception;

class IsUpdatingTrafficException extends Exception
{
    private $metaData;
    private string $channelName;

    public function __construct(string $message = "", mixed $metaData = array(), int $code = 0, ?Throwable $previous = null)
    {
        $this->metaData = $metaData;
        parent::__construct($message, $code, $previous);
    }

    /**
     * Report the exception.
     */
    // public function report(): void
    // {
    //     \Log::channel($this->channelName)
    //         ->error(
    //             get_class($this),
    //             [
    //                 'message' => $this->message,
    //                 'meta' => $this->metaData
    //             ]
    //         );
    // }
}
