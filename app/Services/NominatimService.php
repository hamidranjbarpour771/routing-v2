<?php

namespace App\Services;

use Illuminate\Support\Facades\Http;
class NominatimService
{
    private string $url;

    public function __construct($url = null){
        $this->url = $url ?? env('NOMINATIM_URL');
    }

    public function getWayId($lat, $lon): int|null
    {
        
        $url = $this->url . '/reverse';

        $response = Http::get($url, [
            'lat' => $lat,
            'lon' => $lon,
            'format' => 'json',
            'zoom' => '16',
        ])->json();

        // if (isset($response['osm_id']) && $response['osm_type'] == 'way') {
        //     $startWay = $response['osm_id'];
        // }

        return (isset($response['osm_id']) && $response['osm_type'] == 'way') ? $response['osm_id']: null;
    }

    public function getAddress($lat, $lon)
    {
        $url = $this->url . '/reverse';

        $response = Http::get($url, [
            'lat' => $lat,
            'lon' => $lon,
            'format' => 'json',
            'zoom' => '16',
        ])->json();

        dd($response->json());
    }
}