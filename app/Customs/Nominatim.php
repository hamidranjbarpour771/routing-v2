<?php

namespace App\Customs;

use Illuminate\Support\Facades\DB;

class Nominatim
{
    public static function getCityName(array $lats, array $lons)
    {
        $andClause = '(';
        $whereClase = '';
        $selectClause = '';
        $selectPoints = '';

        for ($i=0; $i < count($lats); $i++) {
            if ($i != count($lats) - 1) {
                $andClause .= '(geometry && ST_SetSRID(ST_Point(' . $lons[$i] . ','. $lats[$i]. '),4326)) or ';
                $whereClase .= 'ST_CONTAINS(geometry, ST_SetSRID(ST_Point('. $lons[$i] . ','. $lats[$i]. '),4326)) or ';
                $selectPoints .= '(geometry && ST_SetSRID(ST_Point('. $lons[$i]. ','. $lats[$i]. '),4326)) as m'. $i. ',';
                $selectClause .= 'm'. $i. ',';
            } else {
                $andClause .= '(geometry && ST_SetSRID(ST_Point(' . $lons[$i] . ','. $lats[$i]. '),4326))';
                $whereClase .= 'ST_CONTAINS(geometry, ST_SetSRID(ST_Point('. $lons[$i] . ','. $lats[$i]. '),4326))';
                $selectPoints .= '(geometry && ST_SetSRID(ST_Point('. $lons[$i]. ','. $lats[$i]. '),4326)) as m'. $i;
                $selectClause .= 'm'. $i;
            }
        }

        $andClause .= ')';

        $sql = "SELECT $selectClause, placename as city, extratags_place from (
            SELECT $selectClause, place_id,
                get_name_by_language(name,ARRAY['name:fa','name','brand','official_name:fa','short_name:fa','official_name','short_name','ref','type']) AS placename,
                extratags_place
                 FROM
                (select $selectPoints, place_id, parent_place_id, rank_address, rank_search, country_code, geometry,name, extratags->'place' as extratags_place
                 FROM placex
                 WHERE ST_GeometryType(geometry) in ('ST_Polygon', 'ST_MultiPolygon')
                 AND ( $andClause
                 AND type != 'postcode'
                 and (
                    -- (extratags->'place') = 'county' 
                    -- or (extratags->'place') = 'city' 
                    -- or (extratags->'place') = 'town' 
                    -- or (extratags->'place') = 'state_district'
                    (extratags->'place') = 'state'
                    or (extratags->'place') = 'province'
                    )
                 AND name is not null
                 AND indexed_status = 0 and linked_place_id is null)) as a
                 WHERE $whereClase
        )q";

        // and ((extratags->'place') = 'county' or (extratags->'place') = 'district' or (extratags->'place') = 'town')
        // dd($sql);
        
        $data = DB::connection('pgsql2')->select($sql);
    
        $i = 0;
        $col = 'm' . $i;
        $cities = [];
        for ($i=0; $i < count($lats); $i++) {
            $col = 'm' . $i;

            foreach ($data as $d) {
                $break =false;

                foreach ($d as $key => $value) {
            
                    if ($key == $col && $value == true && !in_array($d->city, $cities)) {
                        $cities[] = array(
                            'city' => $d->city,
                            'extratags_place' => $d->extratags_place
                        );
                        $break = true;
                        break;
                    }
                }
                
                if ($break) {
                    break;
                }
            }
        }
        // $cities['sql'] = $sql;
        return $cities;
    }

    public static function getCities(array $lats, array $lons, $destName = null)
    {
        $whereClause = '(';
        $selectClause = '';

        for ($i=0; $i < count($lats); $i++) {
            if ($i != count($lats) - 1) {
                $whereClause .= 'ST_CONTAINS(geometry, ST_SetSRID(ST_Point('. $lons[$i] . ','. $lats[$i]. '),4326)) or ';
                $selectClause .= "ST_Distance('SRID=4326;POINT($lons[$i] $lats[$i])',
                    geometry) as m$i, ";
            } else {
                $selectClause .= "ST_Distance('SRID=4326;POINT($lons[$i] $lats[$i])',
                    geometry) as m$i";
            
                $whereClause .= 'ST_CONTAINS(geometry, ST_SetSRID(ST_Point('. $lons[$i] . ','. $lats[$i]. '),4326))';
            }
        }

        $whereClause .= ')';


        $sql = "SELECT $selectClause, get_name_by_language(name,
            array['name:fa',
            'name',
            'brand',
            'official_name:fa',
            'short_name:fa',
            'official_name',
            'short_name',
            'ref',
            'type']) as city
            
            from 
                placex
            where
                $whereClause and (((extratags->'place') = 'district') or ((extratags->'place') = 'city')) and get_name_by_language(name,
            array['name:fa',
            'name',
            'brand',
            'official_name:fa',
            'short_name:fa',
            'official_name',
            'short_name',
            'ref',
            'type']) not like 'بخش مرکزی%'
            ";

        // return $sql;
        
        $data = DB::connection('pgsql2')->select($sql);
        // return $data;

        $i = 0;
        $col = 'm' . $i;
        $cities = [];
        for ($i=0; $i < count($lats); $i++) {
            $col = 'm' . $i;

            foreach ($data as $d) {
                $break =false;

                foreach ($d as $key => $value) {
                    $cityArray = explode(" ", $d->city);
                    if (count($cityArray) > 1) {
                        $cityArray = array_slice($cityArray, 1);
                    }
                    $city = implode(" ", $cityArray);

                    if ($key == $col && $value == 0 && !in_array($city, $cities)) {
                        $cities[] = $city;
                        
                        if(isnull($destName) && $city == $destName)
                            return $cities;
                        
                        $break = true;
                        break;
                    }
                }
                
                if ($break) {
                    break;
                }
            }
        }

        return $cities;
    }
}
