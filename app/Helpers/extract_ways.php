<?php

if (!function_exists('extract_ways')) {

    /** Do not use this for one way. because directions are reverse */

    function extractWaysByDir($ways, $directions, $oneWay=false)
    {
        $directWays  = array();
        $reverseWays = array();

        for ($i=0; $i < count($ways); $i++) {
            if ($oneWay) {
                if ($directions[$i] == 1) {
                    $reverseWays[] = $ways[$i];
                } else {
                    $directWays[] = $ways[$i];
                }
            } else {
                if ($directions[$i] == 1) {
                    $directWays[] = $ways[$i];
                } else {
                    $reverseWays[] = $ways[$i];
                }
            }
        }

        return [
            'direct' => $directWays,
            'reverse' => $reverseWays
        ];
    }
}