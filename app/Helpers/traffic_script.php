<?php

if (!function_exists('execute_traffic_script')) {
    function execute_traffic_script($osrmType, $logPath, $trafficFile, $reset=false)
    {
        file_put_contents($logPath, "");

        exec('sudo systemctl stop osrm-routed-'. $osrmType. " 2>&1 | tee -a ". $logPath ." 2>/dev/null >/dev/null");
        
        $osrm_script_path = config('global_variables.osrm_traffic_script_path');

        chdir($osrm_script_path);
        if ($reset) {
            $shell_cmd = './osrm-routing.sh -m '. $osrmType .' -a MLD -e -c';
        } else {
            $shell_cmd = './osrm-routing.sh -m '. $osrmType .' -a MLD -t '. $trafficFile;
        }
        
        exec($shell_cmd. " 2>&1 | tee -a ". $logPath ." 2>/dev/null >/dev/null");
        /** check if osrm-routed is truely running */
        exec('sudo systemctl start osrm-routed-'. $osrmType. " 2>&1 | tee -a ". $logPath ." 2>/dev/null >/dev/null");
    }
}
