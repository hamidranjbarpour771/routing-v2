<?php

namespace App\Http\Controllers;

use App\Http\Controllers\NominatimController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use App\Models\Traffic;
use App\Models\MapService;
use App\Models\Osrm;
use App\Models\RoutingLog;
use App\Customs\Nominatim;

use \App\Services\Nominatim\ReverseService;

class OsrmController extends Controller
{
    
    public function routingForSMS(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'source' => 'required',
            'dest' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 422,
                'message' => $validator->errors(),
            ], 422);
        }

        $nominatim = new NominatimController();

        $r1 = new Request();
        $r1->merge(['search' => $request->source]);
        $response1 = $nominatim->textSearch($r1)->getData();

        $r2 = new Request();
        $r2->merge(['search' => $request->dest]);
        $response2 = $nominatim->textSearch($r2)->getData();

        if (empty($response1) || empty($response2)) {
            return response()->json();
        }

        // // dd($response1->getData());
        // var_dump($response1[0]->lat);
        // dd();

        $response1 = $response1[0];
        $response2 = $response2[0];

        $srcLat = $response1->lat;
        $srcLon = $response1->lon;

        $destLat = $response2->lat;
        $destLon = $response2->lon;

        $mld_type = env('MLD_TYPE');
        switch ($mld_type) {
            case 'MLD':
                $url = env('OSRM_MLD_URL');
                break;
            
            case 'MLD_2':
                $url = env('OSRM_MLD_2_URL');
                break;
        }

        // annotations=speed&geometries=geojson
        $url .= $srcLon. ','. $srcLat. ';'. $destLon. ','. $destLat . '?alternatives=true&steps=true';
        $response = Http::get($url);
    
        if ($response->status() != 200) {
            return response()->json([
                'message' => $response->body()
            ], $response->status());
        }

        RoutingLog::create([
            'ip' => $request->ip(),
            'src_name' => $request->source,
            // 'src_lat' => $request->src_name ? null : $src_pair[1], 
            // 'src_lon' => $request->src_name ? null : $src_pair[0],
            'dest_name' => $request->dest,
            'type' => 'sms',
            // 'dest_lat' => $request->dest_name ? null : $dest_pair[1],
            // 'dest_lon' => $request->dest_name ? null : $dest_pair[0],
        ]);

        $response = $response->json();
        for ($i = 0; $i < min(count($response['routes']), 2); $i++) {
            $lats = array();
            $lons = array();
            foreach ($response['routes'][$i]['legs'] as $k2 => $leg) {
                foreach ($leg['steps'] as $k3 => $step) {
                    $lons[] = $step['maneuver']['location'][0];
                    $lats[] = $step['maneuver']['location'][1];
                }
            }

            $response['routes'][$i]['cities'] = Nominatim::getCities($lats, $lons, $request->source, $request->dest);
        }
        
        return $response;
    }

    public function routingWithoutTraffic(Request $request)
    {
        $validator = Validator::make($request->all(), [
            // 'coordinates' => 'required|regex:/^[0-9]+.[0-9]+,[0-9]+.[0-9]+;[0-9]+.[0-9]+,[0-9]+.[0-9]+/',
            'coordinates' => 'required',
            'alternatives' => 'boolean',
        ], [
            'coordinates' => 'give coordinates in format:<lon>,<lat>;<lon>,<lat>',
        ])->validate();

        // annotations=speed&geometries=geojson
        $url = env('OSRM_MLD_org_URL') . $request->coordinates . '?overview=full';
        $url .= ($request->has('alternatives')) ? ($request->alternatives ? '&alternatives=true' : '&alternatives=false') : '';
        $response = Http::get($url);
    
        if ($response->status() != 200) {
            return response()->json([
                'message' => $response->body()
            ], $response->status());
        }

        return $response->json();
    }

    public function routingV2(Request $request){

        $validator = Validator::make($request->all(), [
            'coordinates' => 'required|regex:/^[0-9]+.[0-9]+,[0-9]+.[0-9]+;[0-9]+.[0-9]+,[0-9]+.[0-9]+/',
            'src_name' => 'nullable|string',
            'dest_name' => 'nullable|string',
        ], [
            'regex' => 'give coordinates in format:<lon>,<lat>;<lon>,<lat>',
        ])->validate();

        // $mld_type = env('MLD_TYPE');
        $mld_type = Osrm::query()
            ->first()->current_type;

        switch ($mld_type) {
            case 'MLD':
                $url = env('OSRM_MLD_URL');
                break;
            
            case 'MLD_2':
                $url = env('OSRM_MLD_2_URL');
                break;
        }

        // annotations=speed&geometries=geojson
        $url .= $request->coordinates . '?overview=full&alternatives=2&steps=true';
        $response = Http::get($url);
    
        if ($response->status() != 200) {
            return response()->json([
                'message' => $response->body()
            ], $response->status());
        }

        $coordinates = explode(';', $request->coordinates);
        $src_pair = explode(',', $coordinates[0]);
        $dest_pair = explode(',', $coordinates[1]);

        try{

            /** must be deleted  */
            $src_lat = $src_pair[1];
            $src_lon = $src_pair[0];
            $srcReverseService = new ReverseService($src_lat, $src_lon);

            $dest_lat = $dest_pair[1];
            $dest_lon = $dest_pair[0];
            $destReverseService = new ReverseService($dest_lat, $dest_lon);
            /** must be deleted  */


            $routingLog = RoutingLog::create([
                'ip' => $request->ip(),
                'src_name' => $request->src_name,
                'src_lat' => $src_pair[1], 
                'src_lon' => $src_pair[0],
                'dest_name' => $request->dest_name,
                'dest_lat' => $dest_pair[1],
                'dest_lon' => $dest_pair[0],

                'src_place' => $srcReverseService->getPlaceName(),
                'src_place_tags' => $srcReverseService->getPlaceTag(),
                'src_place_state' => $srcReverseService->getState(),

                'dest_place' => $destReverseService->getPlaceName(),
                'dest_place_tags' =>$destReverseService->getPlaceTag(),
                'dest_place_state' => $destReverseService->getState(),
            ]);

            // $routingLog->update([
            //     'src_place' => $srcReverseService->getPlaceName(),
            //     'src_place_tags' => $srcReverseService->getPlaceTag(),
            //     'src_place_state' => $srcReverseService->getState(),
    
            //     'dest_place' => $destReverseService->getPlaceName(),
            //     'dest_place_tags' =>$destReverseService->getPlaceTag(),
            //     'dest_place_state' => $destReverseService->getState(),
            // ]);

        }
        catch(ReverseException $exception){
            
        }

        $response = $response->json();
        for ($i = 0; $i < min(count($response['routes']), 2); $i++) {
            $ways = array();
            // $lats = array();
            // $lons = array();
            foreach ($response['routes'][$i]['legs'] as $k2 => $leg) {
                foreach ($leg['steps'] as $k3 => $step) {
                    $wayID = $step['pronunciation'];
                    $ways[] = $wayID;

                    // $lons[] = $step['maneuver']['location'][0];
                    // $lats[] = $step['maneuver']['location'][1];
                }

                $traffic = Traffic::whereIn('way_id', $ways)
                                  ->selectRaw('way_id as w,status as s')
                                  ->get();

                $response['routes'][$i]['legs'][$k2]['ways'] = $traffic;
            }
        }
        
        return $response;

    }

    public function getWaysBetweenPoints(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'coordinates' => 'required|regex:/^[0-9]+.[0-9]+,[0-9]+.[0-9]+;[0-9]+.[0-9]+,[0-9]+.[0-9]+/',
        ], [
            'regex' => 'give coordinates in format:<lon>,<lat>;<lon>,<lat>',
        ])->validate();

        // annotations=speed&geometries=geojson
        $url = env('OSRM_MLD_org_URL');
        $url .= $request->coordinates . '?overview=false&steps=true';
        $response = Http::get($url);
        
        if ($response->status() != 200) {
            return response()->json([
                'message' => $response->body()
            ], $response->status());
        }

        $response = $response->json();
    
        $data = array();
        $sort = array();
        $ways = "(";

        foreach ($response['routes'][0]['legs'][0]['steps'] as $k3 => $step) {
            $ways .= $step['pronunciation']. ',';
            $names[] = $step['name'];
            $sort[] = $step['pronunciation'];
            
            // Traffic::where('way_id', $step['pronunciation'])
            //        ->whereNull('name')
            //        ->update(['name' => $step['name']]);
        }

        $ways = substr($ways, 0, -1);
        $ways .= ')';
        
        $data = DB::connection('pgsql3')->select("SELECT lat, lon, t1.id as way_id from 
                                                        planet_osm_nodes inner join (select id, unnest(nodes) as nodes 
                                                        from planet_osm_ways where id IN $ways) t1 on planet_osm_nodes.id = t1.nodes");
        
        return response()->json([
            'status' => 200,
            'data' => $data,
            'way_ids' => $sort,
            'way_names' => $names
        ]);
    }

    public function GetCoordinatesOfWays(Request $request)
    {
        Validator::make($request->all(), [
            'ways' => 'required|string', // should be comma seprated
        ])->validate();

        $data = DB::connection('pgsql3')->select("SELECT lat, lon, t1.id as way_id from 
                                                        planet_osm_nodes inner join (select id, unnest(nodes) as nodes 
                                                        from planet_osm_ways where id in ($request->ways)) t1 on planet_osm_nodes.id = t1.nodes");
        
        return response()->json([
            'status' => 200,
            'data' => $data,
        ]);
    }

    public function getAppliedWaysByOperator(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'type' => 'required|in:1,2,3',
        ], [
            'type.in' => 'type must be one of following: 1, 2, 3 (traffic, oneway, block)',
        ])->validate();

        /** type => {
         *      1 => traffic,
         *      2 => one-way,
         *      3 => blockade
         * }  */
        if ($request->type == 1) {
            $data = DB::select("SELECT t1.way_id, t1.name, t1.status, unnest(t1.correct_lats_order) as lat, unnest(t1.correct_lons_order) as lon from (select way_id, name, dir, status, lats, 
                                    case dir
                                        when 1 then lats 
                                        when 0 then array_reverse(lats)
                                    end correct_lats_order,
                                    lons, 
                                    case dir
                                        when 1 then lons  
                                        when 0 then array_reverse(lons)
                                    end correct_lons_order
                                from traffics where type = 1 and operator =1)t1");
        } elseif ($request->type == 2) {
            $data = DB::select("SELECT t1.way_id, t1.name, unnest(t1.correct_lats_order) as lat, unnest(t1.correct_lons_order) as lon from (select way_id, name,dir,lats, 
                                    case dir
                                        when 1 then lats 
                                        when 0 then array_reverse(lats)
                                    end correct_lats_order,
                                    lons, 
                                    case dir
                                        when 1 then lons  
                                        when 0 then array_reverse(lons)
                                    end correct_lons_order
                                from traffics where type = 2 and operator =1)t1");
        } elseif ($request->type == 3) {
            $data = Traffic::where([['operator', 1], ['type', 3]])
                           ->selectRaw('way_id, name, unnest(lats) as lat, unnest(lons) as lon')
                           ->get();
        }

        return response()->json([
            'status' => 200,
            'data' => $data
        ]);
    }

    public function reset(Request $request)
    {
        if (Cache::has('is-updating-traffic')) {
            return response()->json([
                'status' => 422,
                'errors' => [
                    'traffic' => ['سامانه در حال به روزرسانی ترافیک است.']
                ]
            ], 422);
        }
 
        if (env('MLD_TYPE') == 'MLD') {
            $currentType = 'MLD';
            $nextType = 'MLD_2';
        } else {
            $currentType = 'MLD_2';
            $nextType = 'MLD';
        }

        $logNextType = storage_path() .'/logs/shell_traffic_'. $nextType . '.log';
        execute_traffic_script($nextType, $logNextType, $trafficFile='', $reset=true);
        
        $envFilePath = base_path(). '/.env';
        file_put_contents($envFilePath, str_replace("MLD_TYPE=". $currentType, "MLD_TYPE=". $nextType, file_get_contents($envFilePath)));

        // /** after updating and switching into next osrm instance, we should also update previous instance. */
        $logCurrentPath = storage_path() .'/logs/shell_traffic_'. $currentType . '.log';
        execute_traffic_script($currentType, $logCurrentPath, $trafficFile='', $reset=true);

        // one way, blockade
        $oneWayAndBlockadeWays = Traffic::where('type', 2)
                                        ->orWhere('type', 3)
                                        ->update([
                                            'type' => null,
                                            'operator' => null,
                                            'dir' => null,
                                        ]);

        // traffic
        $traffics = Traffic::where('type', 1)->get();
        foreach ($traffics as $t) {
            $t->update([
                'current_speed' => $t->highway_maxspeed,
                'status' => 1,
                'type' => null,
                'operator' => null,
                'dir' => null,
            ]);
        }

        // Cache::forget('is-updating-traffic');
        return response()->json([
            'status'=> 200,
            'message'=> 'Traffic resets succussfully.',
        ]);
    }
}
