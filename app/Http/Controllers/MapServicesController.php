<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Builder;
use App\Models\MapService;
use App\Models\MapServicesV2;

class MapServicesController extends Controller
{
    public function getMapServicesInRoute(Request $request)
    {
        $request->validate([
            'geometry' => 'required',
            'types' => 'required|string',
            'distance' => 'numeric|gt:0'
        ]);

        $distance = $request->distance ?? 100;

        if ($request->types == 'car_assistances') {
            $request->types = 'iran_car_assistances,saipa_car_assistances';
        }

        $types = explode(',', $request->types);

        $points = \Polyline::decode($request->geometry);

        if (count($points) % 2 == 1) {
            unset($points[count($points) - 1]);
        }

        $linestring = '';
        for ($i=0; $i < count($points) - 1; $i+=2) {
            $linestring .= $points[$i+1]. ' '.  $points[$i];

            if ($i + 1 != count($points) - 1) {
                $linestring .= ', ';
            }
        }

        $linestring = "SRID=4326;LINESTRING($linestring)";
        $data = MapServicesV2::
                     when($request->types === 'cameras', function(Builder $query){
                        $query->where('active', 1);
                     })
                     ->whereIn('type', $types)
                     ->whereRaw("lat != '' and lon != '' and lat is not null and lon is not null")
                     ->whereRaw("ST_distance(ST_SetSRID(ST_MakePoint(lon::float,lat::float),4326)::geography, ?::geography) < ?", [$linestring, $distance])
                     ->selectRaw('org_id, lat, lon, type, subtype,
                                 ST_distance(ST_SetSRID(ST_MakePoint(lon::float, lat::float), 4326)::geography, ?::geography) as distance', [$linestring])
                    //  ->orderBy(\DB::raw("length(make_line(st_pointn(foo.geometry,1), a.geom)) asc"))            
                     ->get();

        
        $paired_points = \Polyline::pair($points);

        $res = array();
        foreach ($data as $map_service) {

            $minDist = 100000;
            $closestPoint = -1;
            $pointIdx = -1;
            foreach ($paired_points as $idx => $point) {
                $dist = self::getDistanceBetweenPoints($map_service->lat, $map_service->lon, $point[0], $point[1], "K");
                             
                if($dist < $minDist){
                    $minDist = $dist;
                    $closestPoint = $point;
                    $pointIdx = $idx;
                }
            }

            $res[] = [
                'map_services' => $map_service,
                'point' => $closestPoint,
                'idx' => $pointIdx
            ];
            
        }
            // return $res;
        usort($res, fn ($a, $b) => $a['idx'] - $b['idx']);
        
        return response()->json([
            'count' => count($res),
            'data' => array_column($res, 'map_services'),
        ]);
    }

    public function updateWayIDs()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '30096M');

        $mapServices = MapService::whereNull('is_updated')->get();
        foreach ($mapServices as $ms) {
            $way_id = MapService::setWayID($ms->lat, $ms->lon);
            if ($way_id) {
                $ms->way_id = $way_id;
                $ms->is_updated = 1;
                $ms->save();
            }
        }

        return 'ok';
    }

    public function getDistanceBetweenPoints($lat1, $lon1, $lat2, $lon2, $unit)
    {
        // function distance($lat1, $lon1, $lat2, $lon2, $unit) { ////// distance between two poings in lat and lng
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } elseif ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }
}
