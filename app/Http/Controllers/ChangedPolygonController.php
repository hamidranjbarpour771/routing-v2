<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Http\Request;
use App\Models\City;
use App\Customs\DataTable;

class ChangedPolygonController extends Controller
{
    public function show(Request $request)
    {
        $fields = ['cities.id', 'cities.place_id', 'cities.name', 'cities.updated_at'];
        $query = City::selectRaw('id, place_id, name, updated_at');        

        $results = DataTable::processDataTable($request , '', $fields, $query);
        return response()->json($results, $results['status']);
    }

    public function details(Request $request, City $city)
    {
        return response()->json([
            'status' => 200,
            'data' => $city
        ]);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'place_id' => 'required|integer',
            'geometry' => 'required'
        ])->validate();

        City::create([
            'name' => $request->name,
            'place_id' => $request->place_id,
            'geometry' => $request->geometry,
            'polygon' => $request->geometry
        ]);

        return response()->json([
            'status' => 200,
            'message' => 'city stored succussfully.'
        ]);
    }
}
