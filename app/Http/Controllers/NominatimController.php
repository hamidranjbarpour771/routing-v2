<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\DB;
use App\Customs\Nominatim;
use App\Models\Place;

class NominatimController extends Controller
{
    public function directSearch(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'osm_ids' => 'required|starts_with:W,N,R'
        ], [
            'osm_ids' => 'You must give ids like this: osm_ids=[N|W|R]<value>'
        ])->validate();

        
        $query_array = $request->query();
        $query_params = http_build_query($query_array);

        $url = env('NOMINATIM_URL'). '/lookup?';
        $url .= $query_params;
        $url .= '&format=json';
        
        $response = Http::get($url);
        return $response->json();
    }

    public function reverseSearch(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'lat' => 'required|numeric',
            'lon' => 'required|numeric',
        
        ], [
            'required' => 'You must provide :attribute like this: lat=<value>&lon=<value>&<params>',
        ])->validate();

        // if ($validator->fails()) {
        //     $status_code = 400;
            
        //     return response()->json([
        //         'status'=> $status_code,
        //         'errors'=> $validator->messages()
        //     ], 400);
        // }

        $query_array = $request->query();
        $query_params = http_build_query($query_array);
        
        $url = env('NOMINATIM_URL'). '/reverse?';
        $url .= $query_params;
        $url .= '&format=json&zoom=16';
    
        $response = Http::get($url);
        return $response->json();
    }

    public function textSearch(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'search' => 'required'
        ])->validate();

        $search = $request->search;
        
        $data = Place::whereNotNull('type')
            ->whereNotNull('rank')
            ->where(function($query) use($search){
                return $query->where(function($q1) use ($search){
                    return $q1->where('type', 'marz')
                        ->whereRaw('to_tsvector(name) @@ plainto_tsquery(?)', $search);
                })
                ->orWhere(function($q2) use ($search){
                    return $q2->where('type', '!=', 'marz')
                        ->where('name', 'like', $search. '%');
                });
            })
            ->orderBy('rank', 'desc')
            ->select(['id', 'name', 'lat', 'lon', 'state', 'type']);

        if($request->develop)
            dd($data->toSql(), $data->getBindings());

        return response()->json(
            $data->get()
        );
    }

    public function changeNameInSearchDB(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'osm_id' => 'required|regex:/^[NWR]{1}[0-9]+/',
            'names'  => 'required|string'
        ], [
            'regex' => 'You must provide osm id with following format: [N|W|R]<value>',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'=> 400,
                'errors'=> $validator->messages()
            ], 400);
        }

        $planet_osm_table = \Str::startsWith($request->osm_id, 'W') ?
                            'planet_osm_ways': (\Str::startsWith($request->osm_id, 'N')?
                            'planet_osm_nodes': 'planet_osm_rels');

        $osm_id = substr($request->osm_id, 1);
        $connection = DB::connection('pgsql2');
        $get_osm_query = $connection->select("SELECT * FROM $planet_osm_table WHERE id='$osm_id'");
        
        if (empty($get_osm_query)) {
            return response()->json([
                'status'=> 400,
                'errors'=> "This osm_id is either invalid or doesn't exist. Please provide valid one."
            ], 400);
        }

        $get_place_id_query = $connection->select("SELECT place_id FROM placex WHERE osm_id='$osm_id'");
        if (empty($get_place_id_query)) {
            return response()->json([
                'status'=> 400,
                'errors'=> "This osm_id doesn't have any places associated with it!",
            ], 400);
        }

        $names = explode(',', $request->names);
        $limit_cnt = 0;
        foreach ($names as $name) {
            
            /* Make words in word_table */
            $insert_word = $connection->insert("INSERT INTO word (word_id, word_token, word)
                SELECT max(word_id) + 1, make_standard_name('$name'), '$name' FROM word");
            
            $limit_cnt += $insert_word;
        }
        
        $place_id = $get_place_id_query[0]->place_id;
        $add_names = $connection->update("UPDATE search_name SET 
            name_vector=(SELECT ARRAY(SELECT word_id FROM word ORDER BY word_id DESC LIMIT '$limit_cnt')) WHERE place_id='$place_id'");

        return response()->json([
            'status'=> 200,
            'message'=> 'All names inserted successfully!',
        ], 200);
    }

    public function addNameToSearchDB(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'osm_id' => 'required|regex:/^[NWR]{1}[0-9]+/',
            'names'  => 'required|string'
        ], [
            'regex' => 'You must provide osm id with following format: [N|W|R]<value>',
        ])->validate();

        $planet_osm_table = \Str::startsWith($request->osm_id, 'W') ?
                            'planet_osm_ways': (\Str::startsWith($request->osm_id, 'N')?
                            'planet_osm_nodes': 'planet_osm_rels');

        $osm_id = substr($request->osm_id, 1);
        $connection = DB::connection('pgsql2');
        $get_osm_query = $connection->select("SELECT * FROM $planet_osm_table WHERE id='$osm_id'");
        
        if (empty($get_osm_query)) {
            return response()->json([
                'status'=> 400,
                'errors'=> "This osm_id is either invalid or doesn't exist."
            ], 400);
        }

        $get_place_id_query = $connection->select("SELECT place_id FROM placex WHERE osm_id='$osm_id'");
        if (empty($get_place_id_query)) {
            return response()->json([
                'status'=> 400,
                'errors'=> "This osm_id doesn't have any places associated with it!",
            ], 400);
        }

        $names = explode(',', $request->names);
        
        $place_id = $get_place_id_query[0]->place_id;

        foreach ($names as $name) {
            $get_word_query = $connection->select("SELECT max(word_id) + 1 as max_id, make_standard_name('$name') as word_token, '$name' as name FROM word");
            $result_array =json_decode(json_encode($get_word_query[0]), true);

            /* Make words in word_table */
            $insert_word = $connection->insert("INSERT INTO word (word_id, word_token, word)
                                                VALUES ('$result_array[max_id]', '$result_array[word_token]', '$result_array[name]')");
            
            $add_names = $connection->update("UPDATE search_name SET 
                name_vector=array_append(name_vector, '$result_array[max_id]') WHERE place_id='$place_id'");
        }

        return response()->json([
            'status'=> 200,
            'message'=> 'All names inserted successfully!',
        ], 200);
    }

    public function deleteNameFromSearchDB(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'osm_id' => 'required|regex:/^[NWR]{1}[0-9]+/',
            'name'  => 'required|string'
        ], [
            'regex' => 'You must provide osm id with following format: [N|W|R]<value>',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status'=> 400,
                'errors'=> $validator->messages()
            ], 400);
        }

        $planet_osm_table = \Str::startsWith($request->osm_id, 'W') ?
                            'planet_osm_ways': (\Str::startsWith($request->osm_id, 'N')?
                            'planet_osm_nodes': 'planet_osm_rels');

        $osm_id = substr($request->osm_id, 1);
        $connection = DB::connection('pgsql2');
        $get_osm_query = $connection->select("SELECT * FROM $planet_osm_table WHERE id='$osm_id'");
        
        if (empty($get_osm_query)) {
            return response()->json([
                'status'=> 400,
                'errors'=> "This osm_id is either invalid or doesn't exist. Please provide valid one."
            ], 400);
        }

        $get_place_id_query = $connection->select("SELECT place_id FROM placex WHERE osm_id='$osm_id'");
        if (empty($get_place_id_query)) {
            return response()->json([
                'status'=> 400,
                'errors'=> "This osm_id doesn't have any places associated with it!",
            ], 400);
        }

        $place_id = $get_place_id_query[0]->place_id;
        $get_word_query = $connection->select("SELECT * FROM word WHERE word_id in (SELECT unnest(name_vector) FROM search_name WHERE place_id='$place_id') AND word_token=(SELECT make_standard_name('$request->name'))");
        
        if (empty($get_word_query)) {
            return response()->json([
                'status'=> 400,
                'errors'=> "This osm_id doesn't have name='$request->name' associated with it. Please provide valid name."
            ], 400);
        }

        $result_array =json_decode(json_encode($get_word_query[0]), true);
        $remove_name = $connection->update("UPDATE search_name SET 
            name_vector=array_remove(name_vector, '$result_array[word_id]') WHERE place_id='$place_id'");

        return response()->json([
            'status'=> 200,
            'message'=> 'Provided name removed succesfully!',
        ], 200);
    }

    public function reverseSearchByQuery(Request $request)
    {
        // $data = DB::connection('pgsql2')->select("SELECT place_id, parent_place_id, rank_address, rank_search FROM
        //                         (select place_id, parent_place_id, rank_address, rank_search, country_code, geometry
        //                         FROM placex
        //                         WHERE ST_GeometryType(geometry) in ('ST_Polygon', 'ST_MultiPolygon')
        //                         AND rank_address Between 5 AND 17
        //                         AND geometry && ST_SetSRID(ST_Point($request->lon, $request->lat),4326)
        //                         AND type != 'postcode'
        //                         AND name is not null
        //                         AND indexed_status = 0 and linked_place_id is null
        //                         ORDER BY rank_address DESC LIMIT 50 ) as a
        //                         WHERE ST_CONTAINS(geometry, ST_SetSRID(ST_Point($request->lon, $request->lat),4326) )
        //                         ORDER BY rank_address DESC LIMIT 1");

        // $place_id = $data[0]->place_id;

        // $res = DB::connection('pgsql2')->select("SELECT osm_type, osm_id, class, type, admin_level, rank_search, rank_address, 
        //                                         min(place_id) AS place_id, min(parent_place_id) AS parent_place_id, -1 as housenumber, 
        //                                         country_code, get_name_by_language(name,ARRAY['name:fa','name','brand','official_name:fa','short_name:fa','official_name','short_name','ref','type']) AS placename,
        //                                         get_name_by_language(name, ARRAY['ref']) AS ref, avg(ST_X(centroid)) AS lon, avg(ST_Y(centroid)) AS lat, 
        //                                         COALESCE(importance,0.75-(rank_search::float/40)) AS importance, (SELECT max(ai_p.importance * (ai_p.rank_address + 2)) FROM place_addressline ai_s, placex ai_p 
        //                                         WHERE ai_s.place_id = min(CASE WHEN placex.rank_search < 28 THEN placex.place_id ELSE placex.parent_place_id END) AND ai_p.place_id = ai_s.address_place_id AND ai_s.isaddress AND ai_p.importance is not null) AS addressimportance, 
        //                                         COALESCE(extratags->'place', extratags->'linked_place') AS extra_place FROM placex WHERE place_id in ($place_id) AND ( placex.rank_address between 0 and 30 OR (extratags->'place') = 'city' ) AND 
        //                                         linked_place_id is null GROUP BY osm_type, osm_id, class, type, admin_level, rank_search, rank_address, housenumber, country_code, importance, placename, ref, extra_place");
        // return $res;
        
        return Nominatim::getCityName([35.013982049999996], [50.246997690247504]);
    }
}
