<?php

namespace App\Http\Controllers\trafficManagement;

use App\Http\Controllers\Controller;
use App\Jobs\ProcessTrafficRequest;
use App\Models\TrafficRequest;
use App\Services\ApplyTrafficService;
use App\Services\MakeTrafficFileService;
use App\Services\Exceptions\InvalidWayIDsException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use App\Models\Traffic;
use App\Models\MapService;
use App\Models\Osrm;

use App\Http\Traits\ApiResponse;

class BlockadeController extends Controller
{
    use ApiResponse;

    public function store(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'ways' => 'required|array',
            'ways.*' => 'numeric',
        ])->validate();

        $ways = implode(',', $request->ways);
        $existedWays = DB::connection('pgsql3')
            ->select("SELECT id as way_id from planet_osm_ways where id in ($ways)");

        if (empty($existedWays)) {
            return $this->errorResponse("No related nodes found for given way ids");
        }

        $existedWays = array_column($existedWays, 'way_id');

        $trafficRequest = TrafficRequest::query()
            ->create([
                'action' => 'blockade',
                'status' => 'queued',
                'ways' => $existedWays,
            ]);

        ProcessTrafficRequest::dispatch(
            $trafficRequest
        );

        $message = 'block request queued successfully and may be applied in 3-5 minutes.';
        return $this->successResponse($trafficRequest, $message);

    }

    // public function delete(Request $request)
    // {
    //     if (Cache::has('is-updating-traffic')) {
    //         return response()->json([
    //             'status' => 422,
    //             'errors' => [
    //                 'traffic' => ['سامانه در حال به روزرسانی ترافیک است.']
    //             ]
    //         ], 422);
    //     }

    //     $validator = Validator::make($request->all(), [
    //         'ways' => 'required|regex:/^(\d+)(,(\d+))*$/',
    //     ], [
    //         'ways.regex' => 'give ways in format: <way>,...',
    //     ])->validate();

    //     Cache::add('is-updating-traffic', true, now()->addMinutes(6));
    //     $final_txt = '';

    //     $ways = explode(',', $request->ways);

    //     foreach ($ways as $w) {
    //         $traffic = Traffic::where('way_id', $w)->first();
    //         $traffic->update([
    //             'type' => null,
    //             'operator' => 0,
    //             'dir' => null,
    //         ]);
    //     }

    //     $direct_nodes_query = Traffic::whereIn('way_id', $ways)
    //         ->selectRaw('way_id, unnest(nodes) as node, current_speed as speed')
    //         ->get();

    //     for ($i = 0; $i < count($direct_nodes_query) - 1; $i++) {
    //         if ($direct_nodes_query[$i]->way_id == $direct_nodes_query[$i + 1]->way_id) {
    //             // write to file like nodes[$key] , nodes[$key + 1] , 0
    //             $final_txt .= $direct_nodes_query[$i]->node . ',' . $direct_nodes_query[$i + 1]->node . ',' . $direct_nodes_query[$i]->speed . "\n";
    //         }
    //     }

    //     $reverse_nodes_query = Traffic::whereIn('way_id', $ways)
    //         ->selectRaw('way_id, unnest(array_reverse(nodes)) as node, current_speed as speed')
    //         ->get();

    //     for ($i = 0; $i < count($reverse_nodes_query) - 1; $i++) {
    //         if ($reverse_nodes_query[$i]->way_id == $reverse_nodes_query[$i + 1]->way_id) {
    //             // write to file like nodes[$key] , nodes[$key + 1] , 0
    //             $final_txt .= $reverse_nodes_query[$i]->node . ',' . $reverse_nodes_query[$i + 1]->node . ',' . $reverse_nodes_query[$i]->speed . "\n";
    //         }
    //     }

    //     if (empty($final_txt)) {
    //         return response()->json([
    //             'status' => 422,
    //             'message' => 'Way ids are invalid.',
    //         ]);
    //     }

    //     Storage::disk('traffic')->put('traffic.csv', $final_txt);
    //     $trafficFile = base_path() . Storage::disk('traffic')->url('traffic.csv');

    //     /** determine to which osrm instance the traffic should be applied. */
    //     if (env('MLD_TYPE') == 'MLD') {
    //         $currentType = 'MLD';
    //         $nextType = 'MLD_2';
    //     } else {
    //         $currentType = 'MLD_2';
    //         $nextType = 'MLD';
    //     }

    //     $logNextType = storage_path() . '/logs/shell_traffic_' . $nextType . '.log';
    //     execute_traffic_script($nextType, $logNextType, $trafficFile);

    //     $envFilePath = base_path() . '/.env';
    //     file_put_contents($envFilePath, str_replace("MLD_TYPE=" . $currentType, "MLD_TYPE=" . $nextType, file_get_contents($envFilePath)));

    //     // /** after updating and switching into next osrm instance, we should also update previous instance. */
    //     $logCurrentPath = storage_path() . '/logs/shell_traffic_' . $currentType . '.log';
    //     execute_traffic_script($currentType, $logCurrentPath, $trafficFile);

    //     Cache::forget('is-updating-traffic');

    //     return response()->json([
    //         'status' => 200,
    //         'message' => 'Traffic applied succussfully.',
    //     ]);
    // }
}
