<?php

namespace App\Http\Controllers\trafficManagement;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Str;
use App\Models\Traffic;
use App\Models\MapService;
use App\Models\Osrm;

class OneWayController extends Controller
{
    public function store(Request $request)
    {
        if (Cache::has('is-updating-traffic')) {
            return response()->json([
                'status' => 422,
                'errors' => [
                    'traffic' => ['سامانه در حال به روزرسانی ترافیک است.']
                ]
            ], 422);
        }

        $validator = Validator::make($request->all(), [
            'ways' => 'required|regex:/^(\d+)(,(\d+))*$/',
            'directions' => 'required|regex:/^([01])(,[01])*$/',
            'operator' => 'required|in:1,0',
        ], [
            'ways.regex' => 'give ways in format: <way>,...',
            'directions.regex' => 'give ways in format: 1, 0, 1, ...',
            'operator.in' => 'operator must be one of following: 1, 0',
        ])->validate();

        $ways = explode(',', $request->ways);
        $directions = explode(',', $request->directions);

        if (count($ways) != count($directions)) {
            return response()->json([
                'status' => 422,
                'message' => '<ways> and <directions> are not from same size!'
            ], 422);
        }

        Cache::add('is-updating-traffic', true, now()->addMinutes(6));

        $speed = 0;
        $final_txt = '';

        $bothWays = extractWaysByDir($ways, $directions);
        $direct_nodes_query = Traffic::whereIn('way_id', $bothWays['direct'])
                                    ->selectRaw('way_id, unnest(nodes) as node')
                                    ->get();

        for ($i=0; $i < count($direct_nodes_query) - 1; $i++) {
            if ($direct_nodes_query[$i]->way_id == $direct_nodes_query[$i + 1]->way_id) {
                // write to file like nodes[$key] , nodes[$key + 1] , 0
                $final_txt .= $direct_nodes_query[$i]->node .','. $direct_nodes_query[$i+1]->node . ','. $speed. "\n";
            }
        }

        $reverse_nodes_query = Traffic::whereIn('way_id', $bothWays['reverse'])
                                    ->selectRaw('way_id, unnest(array_reverse(nodes)) as node')
                                    ->get();
      
        for ($i=0; $i < count($reverse_nodes_query) - 1; $i++) {
            if ($reverse_nodes_query[$i]->way_id == $reverse_nodes_query[$i + 1]->way_id) {
                // write to file like nodes[$key] , nodes[$key + 1] , 0
                $final_txt .= $reverse_nodes_query[$i]->node .','. $reverse_nodes_query[$i+1]->node . ','. $speed. "\n";
            }
        }
     
        if (empty($final_txt)) {
            return response()->json([
                'status'=> 422,
                'message'=> 'Way ids are invalid.',
            ]);
        }

        Storage::disk('traffic')->put('traffic.csv', $final_txt);
        $trafficFile = base_path(). Storage::disk('traffic')->url('traffic.csv');
    
        /** determine to which osrm instance the traffic should be applied. */
        if (env('MLD_TYPE') == 'MLD') {
            $currentType = 'MLD';
            $nextType = 'MLD_2';
        } else {
            $currentType = 'MLD_2';
            $nextType = 'MLD';
        }
        
        $logNextType = storage_path() .'/logs/shell_traffic_'. $nextType . '.log';
        execute_traffic_script($nextType, $logNextType, $trafficFile);
        
        $envFilePath = base_path(). '/.env';
        file_put_contents($envFilePath, str_replace("MLD_TYPE=". $currentType, "MLD_TYPE=". $nextType, file_get_contents($envFilePath)));

        // update traffic table after traffic has been applied.
        for ($i=0; $i < count($ways); $i++) {
            $traffic = Traffic::where('way_id', $ways[$i])
                              ->first();

            $traffic->type = 2;
            $traffic->operator = intval($request->operator);
            $traffic->dir = intval($directions[$i]);
            $traffic->save();
        }

        // /** after updating and switching into next osrm instance, we should also update previous instance. */
        $logCurrentPath = storage_path() .'/logs/shell_traffic_'. $currentType . '.log';
        execute_traffic_script($currentType, $logCurrentPath, $trafficFile);

        Cache::forget('is-updating-traffic');

        return response()->json([
            'status'=> 200,
            'message'=> 'Traffic applied succussfully.',
        ]);
    }

    public function delete(Request $request)
    {
        if (Cache::has('is-updating-traffic')) {
            return response()->json([
                'status' => 422,
                'errors' => [
                    'traffic' => ['سامانه در حال به روزرسانی ترافیک است.']
                ]
            ], 422);
        }

        $validator = Validator::make($request->all(), [
            'ways' => 'required|regex:/^(\d+)(,(\d+))*$/',
        ], [
            'ways.regex' => 'give ways in format: <way>,...',
        ])->validate();

        Cache::add('is-updating-traffic', true, now()->addMinutes(6));
        $final_txt = '';

        $ways = explode(',', $request->ways);
        
        foreach ($ways as $w) {
            $traffic = Traffic::where('way_id', $w)->first();
            $traffic->update([
                'type' => null,
                'operator' => 0,
                'dir' => null,
            ]);
        }
        
        $direct_nodes_query = Traffic::whereIn('way_id', $ways)
                                    ->selectRaw('way_id, unnest(nodes) as node, current_speed as speed')
                                    ->get();

        for ($i=0; $i < count($direct_nodes_query) - 1; $i++) {
            if ($direct_nodes_query[$i]->way_id == $direct_nodes_query[$i + 1]->way_id) {
                // write to file like nodes[$key] , nodes[$key + 1] , 0
                $final_txt .= $direct_nodes_query[$i]->node .','. $direct_nodes_query[$i+1]->node . ','. $direct_nodes_query[$i]->speed. "\n";
            }
        }

        if (empty($final_txt)) {
            return response()->json([
                'status'=> 422,
                'message'=> 'Way ids are invalid.',
            ]);
        }
        
        Storage::disk('traffic')->put('traffic.csv', $final_txt);
        $trafficFile = base_path(). Storage::disk('traffic')->url('traffic.csv');
    
        /** determine to which osrm instance the traffic should be applied. */
        if (env('MLD_TYPE') == 'MLD') {
            $currentType = 'MLD';
            $nextType = 'MLD_2';
        } else {
            $currentType = 'MLD_2';
            $nextType = 'MLD';
        }

        $logNextType = storage_path() .'/logs/shell_traffic_'. $nextType . '.log';
        execute_traffic_script($nextType, $logNextType, $trafficFile);
        
        $envFilePath = base_path(). '/.env';
        file_put_contents($envFilePath, str_replace("MLD_TYPE=". $currentType, "MLD_TYPE=". $nextType, file_get_contents($envFilePath)));

        // /** after updating and switching into next osrm instance, we should also update previous instance. */
        $logCurrentPath = storage_path() .'/logs/shell_traffic_'. $currentType . '.log';
        execute_traffic_script($currentType, $logCurrentPath, $trafficFile);

        Cache::forget('is-updating-traffic');

        return response()->json([
            'status'=> 200,
            'message'=> 'Traffic applied succussfully.',
        ]);
    }
}
