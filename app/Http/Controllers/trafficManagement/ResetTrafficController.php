<?php

namespace App\Http\Controllers\trafficManagement;

use App\Http\Controllers\Controller;
use App\Jobs\HandleOsrmTraffic;
use App\Services\MakeTrafficFileService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Cache;
use App\Models\Osrm;

use App\Http\Traits\ApiResponse;

class ResetTrafficController extends Controller
{
    use ApiResponse;

    public function resetTraffic(Request $request)
    {
        if (Cache::has('is-updating-traffic')) {
            $message = 'Routing service is busy applying traffic. try later';
            return $this->errorResponse($message, statusCode: 400);
        }

        $validator = Validator::make($request->all(), [
            'reset' => 'in:1'
        ])->validate();

        $ways = [];
        $directNodes = [];
        $speed = 0;

        HandleOsrmTraffic::dispatch(
            // $osrm,
            // $trafficFile
            'reset-traffic',
            $ways,
            $directNodes,
            $speed
        );

        return $this->successResponse(message: 'Traffic may be applied in 3-5 minutes');
    }

    public function rebuildGraph(Request $request)
    {
    }
}
