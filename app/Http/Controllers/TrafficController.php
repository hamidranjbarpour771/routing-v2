<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\Models\Traffic;
use App\Models\MapService;
use App\Models\Osrm;

class TrafficController extends Controller
{
    // public function getLiveTraffic()
    // {
    //     // ini_set('max_execution_time', 0);
    //     ini_set('memory_limit', '30096M');
    //     ini_set('display_errors', '1');
    //     ini_set('display_startup_errors', '1');
    //     error_reporting(E_ALL);
    //     $traffic = Traffic::selectRaw("src_lat as a, src_lon as b, d_lat as c, d_lon as d, way_id, highway_type, id")
    //                                 //  ->where('highway_type', '<>', 'residential')
    //                                 //  ->where('highway_type', '<>', 'service')
    //                                  ->where('highway_type', 'path')
    //                                 //  ->where('highway_type', 'motorway')
    //                                 //  ->where([['highway_type', 'primary'],
    //                                 //          ['highway_type', 'motorway']])
    //                                 //  ->where('highway_type', 'secondary')
    //                                 //  ->where('highway_type', 'tertiary')
    //                                 //  ->limit(10)
    //                                  ->get();
                    
    //     // $data = \DB::select(\DB::raw("select * from traffics WHERE traffics.way_id && ST_Transform(
    //     //                                 ST_MakeEnvelope(50.1254, 35.5422, 51.2156, 36.808264,
    //     //                                 4326),3857
    //     //                                 ) limit 100;"));


    //     $traffic = \DB::connection('osmdb')->select(\DB::raw("select ST_AsText(way) from planet_osm_roads limit 100"));
        
    //     // $traffic = Traffic::find(2807534);
        
    //     return response()->json([
    //         'status' => 200,
    //         'data' => $traffic
    //     ]);
    // }

    public function importGis()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '30096M');
        $ways = DB::connection('pgsql3')
                              ->select("SELECT src_lat,src_lon,dst_lat,dst_lon,highway_maxspeed,highway_type,tblasliiiiii1.way_id as way_id  from
                              (
                              SELECT id as way_id, 
                              case 
                                  when tags[array_position(tags, 'highway') + 1] = 'secondary' and tags[array_position(tags, 'maxspeed') + 1] is null THEN '85'
                              when tags[array_position(tags, 'highway') + 1] = 'secondary' and tags[array_position(tags, 'maxspeed') + 1] is not null THEN tags[array_position(tags, 'maxspeed') + 1]
                              
                              when tags[array_position(tags, 'highway') + 1] = 'primary' and tags[array_position(tags, 'maxspeed') + 1] is null THEN '85'
                              when tags[array_position(tags, 'highway') + 1] = 'primary' and tags[array_position(tags, 'maxspeed') + 1] is not null THEN tags[array_position(tags, 'maxspeed') + 1]
                              
                              when tags[array_position(tags, 'highway') + 1] = 'motorway' and tags[array_position(tags, 'maxspeed') + 1] is null THEN '120'
                              when tags[array_position(tags, 'highway') + 1] = 'motorway' and tags[array_position(tags, 'maxspeed') + 1] is not null THEN tags[array_position(tags, 'maxspeed') + 1]
                              
                              when tags[array_position(tags, 'highway') + 1] = 'trunk' and tags[array_position(tags, 'maxspeed') + 1] is null THEN '110'
                              when tags[array_position(tags, 'highway') + 1] = 'trunk' and tags[array_position(tags, 'maxspeed') + 1] is not null THEN tags[array_position(tags, 'maxspeed') + 1]
                                  end highway_maxspeed, 
                              tags[array_position(tags, 'highway') + 1] as highway_type
                              from planet_osm_ways pow where 'highway'=ANY(tags)
                              ) tblasliiiiii1
                              join (
                                select tbl1.way_id as waay_id,src_lat,src_lon,dst_lat,dst_lon  from
                              (
                               SELECT planet_osm_ways.id as way_id,nodes[1],nodes[array_upper(nodes, 1)] as last_node, lat as src_lat,lon as src_lon FROM planet_osm_ways
                            inner join planet_osm_nodes on planet_osm_nodes.id = planet_osm_ways.nodes[1]
                              ) tbl1
                              join (
                                SELECT planet_osm_ways.id as way_id,nodes[1],nodes[array_upper(nodes, 1)], lat as dst_lat,lon as dst_lon FROM planet_osm_ways
                            inner join planet_osm_nodes on planet_osm_nodes.id = planet_osm_ways.nodes[array_upper(nodes, 1)]
                              ) tbl2
                              on tbl1.way_id = tbl2.way_id
                              
                              ) tblasliiiiiii2
                              on tblasliiiiii1.way_id = tblasliiiiiii2.waay_id;
                            ");
        $array = array();
        $sql = "INSERT INTO traffics (way_id, highway_type, highway_maxspeed, src_lat, src_lon, d_lat, d_lon) VALUES";
        
        foreach ($ways as $key => $value) {
            $sql .=  "('".$value->way_id."','".$value->highway_type."','".$value->highway_maxspeed."','".$value->src_lat."','".$value->src_lon."','".$value->dst_lat."','".$value->dst_lon."'),";
        }
        $sql = substr($sql, 0, -1);

        $sql .= ';';
        $myfile = fopen("newfile.sql", "w") or die("Unable to open file!");
        fwrite($myfile, $sql);
        fclose($myfile);
        // return $sql;
        dd("hello");
        // $ways_array = json_decode(json_encode($ways), true);
        // Traffic::insert($array);
        DB::select(DB::raw($sql));
        dd("done");
    }

    public function importNodesIntoTraffics()
    {
        ini_set('max_execution_time', 0);
        ini_set('memory_limit', '30096M');

        $traffics = Traffic::get();

        foreach ($traffics as $t) {
            $planet_osm_ways = DB::connection('pgsql3')->select("SELECT id, nodes from planet_osm_ways WHERE id=$t->way_id");
            $t->nodes = $planet_osm_ways[0]->nodes;
            $data = DB::connection('pgsql3')->select("SELECT array_agg(lat) as lats , array_agg(lon) as lons, t1.id as way_id 
                                          from planet_osm_nodes inner join (select id, unnest(nodes) as nodes 
                                          from planet_osm_ways where id=$t->way_id) t1 on planet_osm_nodes.id = t1.nodes group by t1.id");
            $t->lats = $data[0]->lats;
            $t->lons = $data[0]->lons;
            $t->save();
        }

        dd('ok');
    }
}
