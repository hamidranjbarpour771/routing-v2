<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\RoutingLog;
use App\Customs\Nominatim;

class RoutingLogController extends Controller
{
    public function fillSrcDestCities(Request $request)
    {
        ini_set('memory_limit', '-1');
        $routing_logs = RoutingLog::whereRaw('src_lat is not null and src_lon is not null and dest_lat is not null and dest_lon is not null')
                         ->get();

        foreach ($routing_logs as $log) {
            $src_city = Nominatim::getCities([$log->src_lat], [$log->src_lon])[0] ?? null;
            $dest_city = Nominatim::getCities([$log->dest_lat], [$log->dest_lon])[0] ?? null;

            $log->update([
                'src_name' => $src_city,
                'dest_name' => $dest_city,
            ]);
        }

        return response()->json($routing_logs);
    }
}
