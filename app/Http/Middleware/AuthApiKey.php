<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class AuthApiKey
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */

    public function handle(Request $request, Closure $next)
    {
        $incoming_key = $request->header('ApiKey');
        if (!$incoming_key || $incoming_key !== config('global_variables.API_KEY'))
        {
            abort(403,'INVALID_API_KEY');
        }
        return $next($request);
        
    }
    
}

