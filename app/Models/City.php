<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use MStaack\LaravelPostgis\Eloquent\PostgisTrait;
use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    use HasFactory, PostgisTrait;

    protected $guarded = ['id'];

    protected $postgisFields = [
        'geometry'
    ];
}
