<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TrafficRequest extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public function setWaysAttribute($value) {
        $this->attributes['ways'] = str_replace(['[', ']'], ['{', '}'], json_encode($value));
    }

    public function getWaysAttribute($value) {
        return json_decode(str_replace(['{', '}'], ['[', ']'], $value));
    }
}
