<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MapServicesV2 extends Model
{
    use HasFactory;

    protected $table = 'map_services_v2';
    protected $guarded = ['id'];

}
