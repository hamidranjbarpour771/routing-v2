<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Place extends Model
{
    use HasFactory;

    protected $appends = ['display_name'];

    public function getDisplayNameAttribute()
    {
        return $this->name;
    }
}
