<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Osrm extends Model
{
    protected $table = 'osrm';
    protected $guarded = ['id'];

    use HasFactory;
}
