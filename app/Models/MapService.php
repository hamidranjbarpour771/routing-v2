<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Http;

class MapService extends Model
{
    use HasFactory;

    protected $guarded = ['id'];

    public static function setWayID($lat, $lon)
    {
        $url = env('NOMINATIM_URL'). '/reverse?';
        $url .= 'lat=' . $lat. '&lon=' . $lon;
        $url .= '&format=json&zoom=16';

        $response = Http::get($url)->json();
        $way_id = '';
        if (isset($response['osm_id']) && $response['osm_type'] == 'way') {
            $way_id = $response['osm_id'];
        }

        return $way_id;
    }
}
