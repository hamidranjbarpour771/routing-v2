<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\NominatimController;
use App\Http\Controllers\OsrmController;
use App\Http\Controllers\TrafficController;
use App\Http\Controllers\ChangedPolygonController;
use App\Http\Controllers\MapServicesController;
use App\Http\Controllers\RoutingLogController;
use App\Http\Controllers\trafficManagement\BlockadeController;
use App\Http\Controllers\trafficManagement\OneWayController;
use App\Http\Controllers\trafficManagement\setTrafficController;
use App\Http\Controllers\trafficManagement\ResetTrafficController;
use App\Customs\Nominatim;
use App\Http\Middleware\AuthApiKey;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

/** Nominatim (start) */
Route::get('direct', [NominatimController::class, 'directSearch']);
Route::get('reverse', [NominatimController::class, 'reverseSearch']);
Route::get('reverse_by_query', [NominatimController::class, 'reverseSearchByQuery']);
Route::get('text', [NominatimController::class, 'textSearch']);
Route::post('change_name', [NominatimController::class, 'changeNameInSearchDB']);
Route::post('add_name', [NominatimController::class, 'addNameToSearchDB']);
Route::post('delete_name', [NominatimController::class, 'deleteNameFromSearchDB']);
/** Nominatim (end) */

/** OSRM (start) */
// Route::get('routing', [OsrmController::class, 'routing']);
Route::get('routing_v2', [OsrmController::class, 'routingV2']);
Route::get('routing_sms', [OsrmController::class, 'routingForSMS']);
Route::get('routing_org', [OsrmController::class, 'routingWithoutTraffic']);

// Route::prefix('routing_logs')->group(function () {
//     Route::get('fill_src_dest_cities', [RoutingLogController::class, 'fillSrcDestCities']);
// });

Route::get('get_ways', [OsrmController::class, 'getWaysBetweenPoints']);
Route::get('get_applied_ways', [OsrmController::class, 'getAppliedWaysByOperator']);
Route::post('reset_everything', [OsrmController::class, 'reset'])->middleware(AuthApiKey::class);

// Route::post('one_way/store', [OneWayController::class, 'store']);
// Route::post('one_way/delete', [OneWayController::class, 'delete']);

Route::post('blockade/store', [BlockadeController::class, 'store'])->middleware(AuthApiKey::class);
// Route::post('blockade/delete', [BlockadeController::class, 'delete']);

Route::post('reset_traffic', [ResetTrafficController::class, 'resetTraffic'])->middleware(AuthApiKey::class);
Route::post('rebuild_graph', [ResetTrafficController::class, 'rebuildGraph'])->middleware(AuthApiKey::class);
// Route::post('traffic/store', [setTrafficController::class, 'store']);
// Route::post('traffic/update', [setTrafficController::class, 'update']);
// Route::post('traffic/delete', [setTrafficController::class, 'delete']);


Route::get('city', [ChangedPolygonController::class, 'show']);
Route::get('city/{city}', [ChangedPolygonController::class, 'details']);
Route::post('city/store', [ChangedPolygonController::class, 'store']);

// Route::get('routing/get_map_services', [MapServicesController::class, 'getMapServicesInRoute']);
Route::post('routing/get_map_services', [MapServicesController::class, 'getMapServicesInRoute']);

/** OSRM (end) */

/** Operator APIs (start) */

/** Operator APIs (end) */

Route::get('test', function () {
    Cache::forget('is-updating-traffic');

    dd(App\Models\Traffic::first());
});

// Route::get('import_gis', [OsrmController::class, 'importGis']);
// Route::get('live_traffic', [TrafficController::class, 'getLiveTraffic']);
Route::get('import_nodes_into_traffics', [TrafficController::class, 'importNodesIntoTraffics']);


Route::get('ways/get_coordinates', [OsrmController::class, 'GetCoordinatesOfWays']);
