<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Cache;
use App\Http\Controllers\MapServicesController;

use App\Models\MapService;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('offline_map');
});

// Route::get('test', function () {
//     $mapServices = MapService::where([['type', 'cameras'], ['way_id', null]])->get();
//     // $ms->updateWayID();
//     foreach ($mapServices as $ms) {
//         $ms->way_id = MapService::setWayID($ms->lat, $ms->lon);
//         $ms->save();
//     }

//     return $ms;
// });

Route::get('is-updating-traffic', function () {
    if (Cache::has('is-updating-traffic')) {
        $data = true;
    } else {
        $data = false;
    }

    return response()->json([
        'status' => 200,
        'data' => $data,
    ], 200);
});

Route::get('clear_cache', function () {
    Artisan::call('cache:clear');
    dd('ok');
});

Route::get('map_services/update_way_ids', [MapServicesController::class, 'updateWayIDs']);

Route::get('excel', function () {
    $borders = [
        ['مهران', 33.120376	,46.050412],
        ['خسروي', 34.382848	,45.467332],
        ['چزابه', 31.853046	,47.802574],
        ['شلمچه', 30.504689	,48.026964],
        ['تمرچين', 36.665326, 45.068714],
        ['باشماق', 35.614128, 46.016497],
    ];

    $provinces = [
        ['آذربایجان شرقی', 38.07, 46.29],
        ['آذربایجان غربی', 37.54, 45.06],
        ['اردبیل', 38.24, 48.28],
        ['اصفهان', 32.67, 51.66],
        ['البرز', 35.81, 51],
        ['ایلام', 33.63, 46.42],
        ['بوشهر', 28.93, 50.84],
        ['تهران', 35.7, 51.34],
        ['چهارمحال و بختیاری', 32.32, 50.84],
        ['خراسان جنوبی', 32.86, 59.21],
        ['خراسان رضوی', 36.29, 59.6],
        ['خراسان شمالی', 37.47, 57.33],
        ['خوزستان', 31.32, 48.67],
        ['زنجان', 36.67, 48.5],
        ['سمنان', 35.58, 53.38],
        ['سیستان و بلوچستان', 29.49, 60.86],
        ['فارس', 29.6, 52.53],
        ['قزوین', 36.28, 50.01],
        ['قم', 34.64, 50.88],
        ['کردستان', 35.31, 46.29],
        ['کرمان', 30.29, 57.06],
        ['کرمانشاه', 34.32, 47.07],
        ['کهگیلویه و بویراحمد', 30.66, 51.58],
        ['گلستان', 36.83, 54.42],
        ['گیلان', 37.27, 49.58],
        ['لرستان', 33.48, 48.35],
        ['مازندران', 36.55, 53.07],
        ['مرکزی', 34.08, 49.71],
        ['هرمزگان', 27.17, 56.27],
        ['همدان', 34.79, 48.51],
        ['یزد', 31.88, 54.36],
    ];

    $osrm = new App\Http\Controllers\OsrmController();

    $final = array();
    foreach ($provinces as $p_idx => $province) {
        $temp_arr = array();
        foreach ($borders as $b_idx => $border) {
            $r1 = new Illuminate\Http\Request();
            $coordinates = $province[2] . ','. $province[1] . ';' . $border[2]. ',' . $border[1];

            $r1->merge(['coordinates' =>  $coordinates]);
            $response = $osrm->routingWithoutTraffic($r1);
            $distance = intval($response['routes'][0]['distance'] / 1000);
            $temp_arr[] = $distance;
        }

        $final[] = $temp_arr;
    }

    return $final;
});


Route::get('test', function () {
    dd(\App\Customs\Nominatim::getCityName([33.63594703978464], [50.076371189460254]));
    // $reverseService = new \App\Services\Nominatim\ReverseService(25.29008908, 60.66943771);
    // dd($reverseService->getPlaceTag(), $reverseService->getPlaceName());
    // \Illuminate\Support\Facades\Artisan::call('routing-logs:update-addresses');
});
