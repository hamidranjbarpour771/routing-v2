
// Make top menu responsive
function topMenu() {
    var x = document.getElementById("navbarmenu_top");
    var a = document.getElementById("routeBoxToggle");
    if (x.className === "menu_top") {
        x.className += " responsive";
    } else {
        x.className = "menu_top";
    }
}

//If the button is clicked, the menu will close
$(".menu_top button").on("click", function () {
    $(this).parent().removeClass('responsive');
    if ($(this).hasClass('topMenuActiveBtn'))
        $(this).removeClass('topMenuActiveBtn');
    else {
        $('.topMenuActiveBtn').removeClass('topMenuActiveBtn');
        $(this).addClass('topMenuActiveBtn');
    }
});

$(".menu_top .dropdown-content a").on("click", function () {
    $('.menu_top').removeClass('responsive');
});

// WebApp Install Prompt
var deferredPrompt;
var buttonInstall = document.getElementById('webapp-icon');

window.addEventListener('beforeinstallprompt', (e) => {
    // Prevent the mini-infobar from appearing on mobile
    e.preventDefault();
    // Stash the event so it can be triggered later.
    deferredPrompt = e;
    // Update UI notify the user they can install the PWA
    //   showInstallPromotion();
});

if (buttonInstall) {
    buttonInstall.addEventListener('click', (e) => {
        // Hide the app provided install promotion
        // hideMyInstallPromotion();
        // Show the install prompt
        deferredPrompt.prompt();
        // Wait for the user to respond to the prompt
        deferredPrompt.userChoice.then((choiceResult) => {
            if (choiceResult.outcome === 'accepted') {
                console.log('User accepted the install prompt');
            } else {
                console.log('User dismissed the install prompt');
            }
        })
    });
}
