// Spinner elements
const cameraSpinner = document.getElementById("camera-spinner");
const trafficSpinner = document.getElementById("traffic-spinner");
const layerSpinner = document.getElementById("layer-spinner");
const trafficOnlineSpinner = document.getElementById("onlinetraffic-spinner");

// Layer & activeLayer elements
var layerButtonElements = document.querySelectorAll('.layerButton');
var activeLayerElements = document.querySelectorAll('.activeLayer');

const windowWidth = $(window).width();

var isLoading = false;
var isCoridor = false;

var popupArrays = [];
var routeArray = [];
var routeControls = [];
var onlineTrafficLayer = [];
// Routing  coordinates & markers
var autoCompleteOriginElement = document.getElementById('origin-input');
var autoCompleteDestinationElement = document.getElementById('destination-input');
var originLat, originLng, destinationLat, destinationLng;
var originMarker = null;
var destinationMarker = null;
var routes = null;

// Triptime variables
var tripTimeData = null;
var tripTimeSelectedData = [];
var numberOfPoints = 0;
var numberOfCreatedRouts = 0;
var isFirstRoute = false;
var selectedRoadName;
var sum = 0;

// Mosaferyar
var pelak_number = '';
var invoice_number = '';
var verify_code = '';
var captcha_code = '';
var mobile = '';

// Index for camera image slider
var cameraSlideIndex = 1;

// layerFlags is an array of 0 and 1's and each element is for each layer.
var layerFlags = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

// markerLayers holds map layer instance for showing desired layer on map.
var markerLayers = null;

// markerList is an array of markers to show on map.
var markerList = [];

// IRAN bounds, by this order:
// [North, East]
// [South, West]
var bounds = [
  [42.9130026312, 75.6166317076],
  [20.5782370061, 35.5092252948]
];

// Create leaflet map client
var map = L.map('map', {
  maxBounds: bounds,
  minZoom: MAP_MIN_ZOOM
}).setView([32.620, 52.471], 6);

// Create map tile layers
// var baselayers = {
//   "Default": L.tileLayer('https://maptile.rmto.ir/tile/v1/1/{z}/{x}/{y}'),
//   "Dark": L.tileLayer('https://maptile.rmto.ir/tile/v1/2/{z}/{x}/{y}'),
//   "Bright": L.tileLayer('https://maptile.rmto.ir/tile/v1/3/{z}/{x}/{y}')
// };

// L.control.layers(baselayers, {}).addTo(map);
// baselayers["Default"].addTo(map);

var defaultTileLayer = L.tileLayer('https://maptile.rmto.ir/tile/v1/1/{z}/{x}/{y}', {}).addTo(map);
var darkTileLayer = L.tileLayer('https://maptile.rmto.ir/tile/v1/2/{z}/{x}/{y}', {});
var brightTileLayer = L.tileLayer('https://maptile.rmto.ir/tile/v1/3/{z}/{x}/{y}', {});

function switchTileLayer() {
  if (map.hasLayer(defaultTileLayer)) {
    // Switch to dark
    $('.switch-tile-container img').attr('src', 'assets/new_template/images/tile-icons/bright.jpg');
    $('.switch-tile-container img').attr('title', 'حالت روشن');
    map.removeLayer(defaultTileLayer);
    map.addLayer(darkTileLayer);
  } else if (map.hasLayer(darkTileLayer)) {
    // Switch to bright
    $('.switch-tile-container img').attr('src', 'assets/new_template/images/tile-icons/default.jpg');
    $('.switch-tile-container img').attr('title', 'حالت پیش فرض');
    map.removeLayer(darkTileLayer);
    map.addLayer(brightTileLayer);
  } else if (map.hasLayer(brightTileLayer)) {
    // Switch to default
    $('.switch-tile-container img').attr('src', 'assets/new_template/images/tile-icons/dark.jpg');
    $('.switch-tile-container img').attr('title', 'حالت تاریک');
    map.removeLayer(brightTileLayer);
    map.addLayer(defaultTileLayer);
  }
}

if (windowWidth <= '1200') {
  // Prompt user to show his current location.
  map.locate({
    setView: true,
    maxZoom: 16
  });
}

var sourceIcon = L.icon({
  iconUrl: 'assets/new_template/images/icons/maps-end.png',
  iconSize: [24, 24]
});

var midIcon = L.icon({
  iconUrl: 'assets/new_template/images/icons/maps-mid.png',
  iconSize: [24, 24]
});

var destinationIcon = L.icon({
  iconUrl: 'assets/new_template/images/icons/maps-start.png',
  iconSize: [24, 24]
});

// function showRoutingBox() {
//   routeControls.push(L.Routing.control({
//     routeWhileDragging: true,
//     reverseWaypoints: true,
//     showAlternatives: true,
//     collapsible: true,
//     show: false,
//     geocoder: L.Control.Geocoder.nominatim(),
//     lineOptions: {
//       styles: [{
//         color: '#5789f2',
//         opacity: 1,
//         weight: 5
//       }]
//     },
//     altLineOptions: {
//       styles: [{
//         color: 'black',
//         opacity: 0.15,
//         weight: 9
//       }, {
//         color: 'white',
//         opacity: 0.8,
//         weight: 6
//       }, {
//         color: 'gray',
//         opacity: 0.5,
//         weight: 4
//       }]
//     },
//     createMarker: function (i, wp, nWps) {
//       if (i == 0) {
//         return L.marker(wp.latLng, { icon: sourceIcon, draggable: true });
//       } else {
//         if (nWps == 3) {
//           if (i == 1)
//             return L.marker(wp.latLng, { icon: midIcon, draggable: true });
//           else
//             return L.marker(wp.latLng, { icon: destinationIcon, draggable: true });
//         }
//         return L.marker(wp.latLng, { icon: destinationIcon, draggable: true });
//       }
//     },
//   }).on('routesfound', function (e) {
//     // console.log(e);
//     var divideBy = 2;

//     for (let index = 0; index < popupArrays.length; index++) {
//       map.removeLayer(popupArrays[index]);
//     }

//     var customOptions = {
//       'maxWidth': '300',
//       'maxHeight': '150',
//       'className': 'customRouteEstimate'
//     }
//     for (let index = 0; index < e.routes.length; index++) {
//       var popup = new L.Popup(customOptions);

//       if (index % 2 != 0) {
//         divideBy = 6;
//       }

//       var popupLocation = new L.LatLng(e.routes[index].coordinates[parseInt(e.routes[index]
//         .coordinates
//         .length /
//         divideBy)].lat,
//         e.routes[index].coordinates[parseInt(e.routes[index].coordinates.length / divideBy)]
//           .lng);

//       var popupContent = "<span style=\"color:#FF5630 ; font-size:11px\">مسافت : </span>" + parseInt(e.routes[index].summary.totalDistance / 1000) + " کیلومتر" +
//         "<br> " + "<span style=\"color:#FF5630 ;font-size:11px\">زمان : </span>" + secondsToHourAndMinute(e.routes[index].summary.totalTime) + "";

//       popup.setLatLng(popupLocation);
//       popup.setContent(popupContent);

//       popupArrays.push(popup);
//       map.addLayer(popup);
//     }
//   }).addTo(map));
// }

function secondsToHourAndMinute(d) {
  d = Number(d);
  var h = Math.floor(d / 3600);
  var m = Math.floor(d % 3600 / 60);
  var s = Math.floor(d % 3600 % 60);

  var hDisplay = h > 0 ? h + (h == 1 ? " ساعت و " : " ساعت و ") : "";
  var mDisplay = m > 0 ? m + (m == 1 ? " دقیقه " : " دقیقه ") : "";
  var sDisplay = s > 0 ? s + (s == 1 ? " second" : " seconds") : "";
  return hDisplay + mDisplay;
}

// Base function for show every layer on map based on its code.
function showLayer(code, url, element) {
  if (windowWidth <= '768') {
    $('.ResponsiveVerticalMenu').show();
    $(element).parent().css('transform', 'scale(0)');
    $('.parent_menu_vertical').css('display', 'none');
  }

  if (layerFlags[code] == 0 && !isLoading) {
    let activeLayerElements = document.querySelectorAll('.activeLayer');

    if ($(activeLayerElements).length) {
      $(activeLayerElements).each(function (index, element) {
        $(element).find('img').attr('src', returnLayerImage(code, 1));
        $(element).removeClass('activeLayer');
      });
    }
    if (windowWidth > '768') {
      $(layerButtonElements[code]).addClass('activeLayer');
      $(layerButtonElements[code]).find('img').attr('src', returnLayerImage(code, 0));
    } else {
      $('.active-tooltip').removeClass('active-tooltip');
      $(layerButtonElements[code]).find('.tooltip').addClass('active-tooltip');
    }
    // This is for the case when user clicks on another layer and
    // there is an enabled layer on the map, too.
    for (let index = 0; index < layerFlags.length; index++) {
      if (layerFlags[index] == 1) {
        if (windowWidth > '768') {
          $(layerButtonElements[index]).find('img').attr('src', returnLayerImage(index, 1));
        }
      }
    }
    if (markerLayers != null) {
      map.removeLayer(markerLayers);
      markerLayers = null;
      markerList = [];
    }
    layerFlags = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    layerFlags[code] = 1;
    // console.log(layerFlags);
    // Make an API call to get layer details...
    var newUrl = isCoridor ? '/getcooridordata' : url;

    if (isCoridor) {
      showCoridorLayer(code, newUrl);
    } else {
      showAllDataLayer(code, newUrl);
    }
  } else {
    // When layer is enabled and we wanna remove it...
    if (markerLayers != null) {
      map.removeLayer(markerLayers);
      markerLayers = null;
      markerList = [];
    }
    layerFlags[code] = 0;
    let activeLayerElements = document.querySelectorAll('.activeLayer');

    if ($(activeLayerElements).length) {
      $(activeLayerElements).each(function (index, element) {
        $(element).find('img').attr('src', returnLayerImage(code, 1));
        $(element).removeClass('activeLayer');
      });
    }
  }
}

function returnType(code) {
  switch (code) {
    case 0:
      return 4;
    case 1:
      return 2;
    case 2:
      return 12;
    case 3:
      return 10;
    case 4:
      return 13;
    case 5:
      return 8;
    case 6:
      return 3;
    case 7:
      return 5;
    case 8:
      return 1;
    case 9:
      return 11;
    case 10:
      return 7;
    case 11:
      return 9;
    case 12:
      return 6;
  }
}

function showCoridorLayer(code, url) {
  isLoading = true;
  layerSpinner.removeAttribute('hidden');
  $.post(API_ENDPOINT + url,
    {
      'latlng': routeArray,
      'type': returnType(code),
      'device_type': returnDeviceType()
    },
    function (data) {
      // console.log('data#####: ', data);
      isLoading = false;
      layerSpinner.setAttribute('hidden', '');

      markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });

      // Loop through each entry of JSON to create an array of markers.
      if (code == 0) {
        // We don't want to show popup for cameras, instead we wanna
        // show image carousel for each camera.
        for (var i = 0; i < data.length; i++) {
          var marker = L.marker(L.latLng(data[i].lat, data[i].lng), {
            icon: getMarkerIcon(code, data[i]),
            title: data[i].Title,
            cameraId: data[i].IPInfo
          }).on('click', onCameraMarkerClick);
          markerList.push(marker);
        }
      }
      else if (code == 4) {
        // Emdad Fani's JSON is different from others...
        for (var i = 0; i < data.ikco.length; i++) {
          var marker = L.marker(L.latLng(data.ikco[i].lat, data.ikco[i].lng), {
            icon: getMarkerIcon(code, data.ikco[i]),
            // title: data[i].title
          }).bindPopup(getPopupContent(code, data.ikco[i]), getPopupCustomOptions(code));
          markerList.push(marker);
        }
        for (var i = 0; i < data.saipa.length; i++) {
          var marker = L.marker(L.latLng(data.saipa[i].lat, data.saipa[i].lng), {
            icon: getMarkerIcon(code, data.saipa[i]),
            // title: data[i].title
          }).bindPopup(getPopupContent(code, data.saipa[i]), getPopupCustomOptions(code));
          markerList.push(marker);
        }
      } else {
        for (var i = 0; i < data.length; i++) {
          var marker = L.marker(L.latLng(data[i].lat, data[i].lng), {
            icon: getMarkerIcon(code, data[i]),
            // title: data[i].title
          }).bindPopup(getPopupContent(code, data[i]), getPopupCustomOptions(code));
          markerList.push(marker);
        }
      }
      // Add array of markers to the map.
      markerLayers.addLayers(markerList);
      map.addLayer(markerLayers);
    });

  // Add slides / photos of camera to slideshow
  var onCameraMarkerClick = function (e) {
    let cameraTitle = this.options.title;
    layerSpinner.removeAttribute('hidden');
    $.post(API_ENDPOINT + '/camera' + '/' + this.options.cameraId,
      {
        'device_type': returnDeviceType()
      },
      function (data) {
        layerSpinner.setAttribute('hidden', '');
        if (data.status) {
          let dataLength = data.five_sorted.length;
          $("#slideshow-slide").find('.mySlides').remove();
          $("#slideshow-dot").find('.dot').remove();
          $(data.five_sorted).each(function (index, value) {
            let cameraDate = moment.unix(data.five_sorted[index].mtime).format("jYYYY/jM/jD");
            let cameraTime = moment.unix(data.five_sorted[index].mtime).format("HH:mm:ss");
            let divSlide = '<div class="mySlides"><div class="numbertext">'
              + dataLength + ' / ' + (index + 1) + '</div><img src="'
              + value['down_link'] + '"><div class="text">نام دوربین: ' + cameraTitle + ' - تاریخ: ' + cameraDate + '، ساعت: ' + cameraTime + '</div></div>';
            $('#slideshow-slide').append(divSlide);
            $('#slideshow-dot').append('<span class="dot" onclick="currentCameraSlide(' + (index + 1) + ')"></span>');
          });
          if (dataLength > 0) {
            cameraSlideIndex = 1;
            cameraShowSlides(cameraSlideIndex);
            $("#provincialCameraModal").css('display', 'block');
          }
        }
      });
  }
  // END Add slides / photos of camera to slideshow
}

function showAllDataLayer(code, url) {
  clearMosaferyar();
  if (code == 14) {
    // Passenger
    $('#passenger-modal').modal('show');
  } else {
    isLoading = true;
    layerSpinner.removeAttribute('hidden');
    $.post(API_ENDPOINT + url,
      {
        'device_type': returnDeviceType()
      },
      function (data) {
        // console.log(data);
        isLoading = false;
        layerSpinner.setAttribute('hidden', '');
        // Disable clustering for specific layers, including:
        // [Road operation], [Road block], [Road accident], and [Weather]
        switch (code) {
          case 2:
          case 3:
          case 5:
          case 9:
          case 13: {
            markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });
            break;
          }
          default: {
            markerLayers = L.markerClusterGroup();
            break;
          }
        }

        switch (code) {
          case 0: {
            // We don't want to show popup for cameras, instead we wanna
            // show image carousel for each camera.
            for (var i = 0; i < data.length; i++) {
              var marker = L.marker(L.latLng(data[i].lat, data[i].lng), {
                icon: getMarkerIcon(code, data[i]),
                title: data[i].Title,
                cameraId: data[i].IPInfo
              }).on('click', onCameraMarkerClick);
              markerList.push(marker);
            }
            break;
          }
          case 4: {
            // Emdad Fani's JSON is different from others...
            for (var i = 0; i < data.ikco.length; i++) {
              var marker = L.marker(L.latLng(data.ikco[i].lat, data.ikco[i].lng), {
                icon: getMarkerIcon(code, data.ikco[i]),
                // title: data[i].title
              }).bindPopup(getPopupContent(code, data.ikco[i]), getPopupCustomOptions(code));
              markerList.push(marker);
            }
            for (var i = 0; i < data.saipa.length; i++) {
              var marker = L.marker(L.latLng(data.saipa[i].lat, data.saipa[i].lng), {
                icon: getMarkerIcon(code, data.saipa[i]),
                // title: data[i].title
              }).bindPopup(getPopupContent(code, data.saipa[i]), getPopupCustomOptions(code));
              markerList.push(marker);
            }
            break;
          }
          case 5: {
            for (var i = 0; i < data.length; i++) {
              if (data[i].situation != "") {
                var marker = L.marker(L.latLng(data[i].lat, data[i].lng), {
                  icon: getMarkerIcon(code, data[i]),
                  // title: data[i].title
                }).bindPopup(getPopupContent(code, data[i]), getPopupCustomOptions(code));
                markerList.push(marker);
              }
            }
            break;
          }
          case 13: {
            for (var i = 0; i < data.length; i++) {
              var marker = L.marker(L.latLng(data[i].v_lat, data[i].v_lng), {
                icon: getMarkerIcon(code, data[i]),
                title: data[i].v_Title,
                picture1: data[i].v_VMSSendImage_Part1,
                picture2: data[i].v_VMSSendImage_Part2
              }).on('click', onvmsmodal);
              // console.log(title)
              markerList.push(marker);
            }
            break;
          }
          case 14: {
            // Passenger
            // $('#reportModal').modal('show');
            break;
          }
          default: {
            // console.log(data);
            for (var i = 0; i < data.length; i++) {
              var marker = L.marker(L.latLng(data[i].lat, data[i].lng), {
                icon: getMarkerIcon(code, data[i]),
                // title: data[i].title
              }).bindPopup(getPopupContent(code, data[i]), getPopupCustomOptions(code));
              markerList.push(marker);
            }
            break;
          }
        }

        // Add array of markers to the map.
        markerLayers.addLayers(markerList);
        map.addLayer(markerLayers);
      });

    // Add slides / photos of camera to slideshow
    var onCameraMarkerClick = function (e) {
      let cameraTitle = this.options.title;
      layerSpinner.removeAttribute('hidden');
      $.post(API_ENDPOINT + url + '/' + this.options.cameraId,
        {
          'device_type': returnDeviceType()
        },
        function (data) {
          //console.log('data', data);
          layerSpinner.setAttribute('hidden', '');
          if (data.status) {
            let dataLength = data.five_sorted.length;
            $("#slideshow-slide").find('.mySlides').remove();
            $("#slideshow-dot").find('.dot').remove();
            $(data.five_sorted).each(function (index, value) {
              let cameraDate = moment.unix(data.five_sorted[index].mtime).format("jYYYY/jM/jD");
              let cameraTime = moment.unix(data.five_sorted[index].mtime).format("HH:mm:ss");
              let divSlide = '<div class="mySlides"><div class="numbertext">'
                + dataLength + ' / ' + (index + 1) + '</div><img src="'
                + value['down_link'] + '"><div class="text">نام دوربین: ' + cameraTitle + ' - تاریخ: ' + cameraDate + '، ساعت: ' + cameraTime + '</div></div>';
              $('#slideshow-slide').append(divSlide);
              $('#slideshow-dot').append('<span class="dot" onclick="currentCameraSlide(' + (index + 1) + ')"></span>');
            });
            if (dataLength > 0) {
              cameraSlideIndex = 1;
              cameraShowSlides(cameraSlideIndex);
              $("#provincialCameraModal").css('display', 'block');
            }
          }
        });
    }
    // END Add slides / photos of camera to slideshow

    // VMS Modal
    var onvmsmodal = function (e) {
      let pof = " "

      $('#header-vms').empty();
      $("#texyvms").empty();
      let title = this.options.title
      let tt = "<h4 class=\"modal-title modalvmstitle\" > " + title + "</h4><button type=\"button\" class=\"close\" data-dismiss=\"modal\" style=\"color: white\">&times;</button>"
      $('#header-vms').append(tt);
      let picture1 = this.options.picture1
      let picture2 = this.options.picture2
      //   console.log("test",picture2)
      if (picture2 == "") {
        console.log("test")
        pof = "<img class=\"picsize\" src=\"data:image/png;base64," + picture1 + "\">"

      }
      else {
        pof = "<img class=\"picsize\" src=\"data:image/png;base64," + picture1 + "\"><img class=\"picsize2\" src=\"data:image/png;base64," + picture2 + "\">"

      }
      $('#texyvms').append(pof);
      $('#Vmsmodal').modal('show')
    }
  }
}

function returnLayerImage(code, type) {
  var isActive = '';
  if (type == 0) {
    isActive = '/active';
  } else {
    isActive = '';
  }
  switch (code) {
    case 0: {
      // Camera Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/camera.svg';
    }
    case 1: {
      // Traffic Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/taraddod.svg';
    }
    case 2: {
      // Road Operation Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/construction.svg';
    }
    case 3: {
      // Road Block Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/roadBlock.svg';
    }
    case 4: {
      // Emdad Fani Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/emdad.svg';
    }
    case 5: {
      // Weather Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/weather.svg';
    }
    case 6: {
      // Fuel Station Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/gazStation.svg';
    }
    case 7: {
      // Complex Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/complex.svg';
    }
    case 8: {
      // Mosque Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/mosque.svg';
    }
    case 9: {
      // Road Accident Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/accident.svg';
    }
    case 10: {
      // Toll House Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/tollHouse.svg';
    }
    case 11: {
      // Hospitals Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/hospital.svg';
    }
    case 12: {
      // Repair Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/repair.svg';
    }
    case 13: {
      // VMS Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/panel.svg';
    }
    case 14: {
      // Passenger Layer
      return 'assets/new_template/images/layer-icons' + isActive + '/passenger.svg';
    }
  }
}

// show camera slides
function cameraShowSlides(n) {
  let slides = $("#provincialCameraModal #slideshow-slide .mySlides");
  let dots = $("#provincialCameraModal #slideshow-dot .dot");

  if (n > slides.length) { cameraSlideIndex = 1 }
  if (n < 1) { cameraSlideIndex = slides.length }

  $(slides).each(function () {
    $(this).css('display', 'none')
  });
  $(dots).each(function () {
    $(this).removeClass('active')
  });

  $(slides[cameraSlideIndex - 1]).css('display', 'block');
  $(dots[cameraSlideIndex - 1]).addClass('active');
}

// return icon for each layer based on its code.
function getMarkerIcon(code, data) {
  switch (code) {
    case 0: {
      // Camera Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/camera.png'
      });
    }
    case 1: {
      // Traffic Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/traffic.png'
      });
    }
    case 2: {
      // Road Operation Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/roadOperation.png'
      });
    }
    case 3: {
      // Road Block Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/roadBlock.png'
      });
    }
    case 4: {
      // Emdad Fani Layer
      if ("onvan" in data) {
        return L.icon({
          iconUrl: 'assets/new_template/images/markers/IranKhodro.png'
        });
      }
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/saipa.png'
      });
    }
    case 5: {
      // Weather Layer
      switch (data.situation) {
        case 'صاف':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/sun.png',
            iconSize: [40, 24],
          });
        case 'کمي ابري':
        case 'قسمتي ابري':
        case 'نیمه ابري':
        case 'بتدریج ابري':
        case 'رشدابردرارتفاعات':
        case 'کاهش ابر':
        case 'افزایش ابر':
        case 'ابری با احتمال بارش':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/cloudySun.png',
            iconSize: [40, 24],
          });
        case 'ابري':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/cloud.png',
            iconSize: [40, 24],
          });
        case 'رعدوبرق':
        case 'رگبارورعدوبرق':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/thunder.png',
            iconSize: [40, 24],
          });
        case 'رعدوبرق بابارش':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/thunderWithRain.png',
            iconSize: [40, 24],
          });
        case 'غبارآلود':
        case 'غبارمحلي':
        case 'غبارصبحگاهي':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/dusty.png',
            iconSize: [40, 24],
          });
        case 'مه آلود':
        case 'مه رقيق':
        case 'مه صبحگاهي':
        case 'مه غليظ':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/foggy.png',
            iconSize: [40, 24],
          });
        case 'بارش خفيف باران':
        case 'رگبار باران':
        case 'بارش باران':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/rainy.png',
            iconSize: [40, 24],
          });
        case 'بارش باران و برف':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/rainySnow.png',
            iconSize: [40, 24],
          });
        case 'بارش برف':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/snowy.png',
            iconSize: [40, 24],
          });
        case 'رگبار برف':
        case 'کولاک برف':
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/hardSnow.png',
            iconSize: [40, 24],
          });
        default:
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/sun.png',
            iconSize: [40, 24],
          });
      }
    }
    case 6: {
      // Fuel Station Layer
      switch (data.SType) {
        case '1':
          // GasOil
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/gasOil.png'
          });
        case '4':
          // CNG
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/CNG.png'
          });
        default:
          // Gasoline
          return L.icon({
            iconUrl: 'assets/new_template/images/markers/gasoline.png'
          });
      }
    }
    case 7: {
      // Complex Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/complex.png'
      });
    }
    case 8: {
      // Mosque Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/mosque.png'
      });
    }
    case 9: {
      // Road Accident Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/roadAccident.png'
      });
    }
    case 10: {
      // Toll House Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/tollHouse.png'
      });
    }
    case 11: {
      // Hospitals Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/hospital.png'
      });
    }
    case 12: {
      // Repair Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/repair.png'
      });
    }
    case 13: {
      // VMS Layer
      return L.icon({
        iconUrl: 'assets/new_template/images/markers/vms.png',
      });
    }
    default:
      break;
  }
}

// Generate and return popup content for each layer...
function getPopupContent(code, data) {
  switch (code) {
    case 1: {
      // Traffic Layer
      return "\
        <div class=\"popup-marker\">\
          <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/taraddod.svg\" alt=\"\">\
            <span class=\"popupHeader\">دستگاه های تردد شمار بر خط</span>\
          </div>\
          <div class=\"popupContent\">\
          <div class=\"popUpBox\" style=\"width:75px\"><span>استان</span><div class=\"popUpBoxContent\">"  + data.o_n + "</div> </div>\
          <div class=\"popUpBox\" style=\"width:75px\"><span>محور</span><div class=\"popUpBoxContent\">"+ data.m_n + "</div></div>\
          <div class=\"popUpBox\" style=\"width:75px\"><span>وضعیت ترافیک</span><div class=\"popUpBoxContent\">"+ data.trf + "</div></div>\
          <div class=\"popUpBox\" style=\"width:75px\"><span>میانگین سرعت</span><div class=\"popUpBoxContent\">"+ data.spd + " کیلومتربرساعت</div></div>\
        </div>";
    }
    case 2: {
      // customDateFormat
      var roadOperationDate = data.StartDate;
      roadOperationDate = roadOperationDate.slice(0, 4) + "/" + roadOperationDate.slice(4,
        6) + "/" +
        roadOperationDate.slice(6, 8);
      // console.log(roadOperationDate);


      var roadOperationEndDate = data.EndDate;
      roadOperationEndDate = roadOperationEndDate.slice(0, 4) + "/" + roadOperationEndDate.slice(4,
        6) + "/" +
        roadOperationEndDate.slice(6, 8);
      // console.log(roadOperationEndDate);

      // customTimeFormat
      var roadOperationTime = data.StartTime;
      roadOperationTime = roadOperationTime.slice(0, 2) + ":" + roadOperationTime.slice(2, 4)
      // console.log("roadOperationTime : ", roadOperationTime);
      var roadOperationEndTime = data.EndTime;
      roadOperationEndTime = roadOperationEndTime.slice(0, 2) + ":" + roadOperationEndTime.slice(2, 4)
      // console.log("roadOperationEndTime : ", roadOperationEndTime);
      // Road Operation Layer
      return "\
        <div class=\"popup-marker\">\
        <div class=\"popupTitle\">\
        <img src=\"assets/new_template/images/layer-icons/construction.svg\" alt=\"\">\
        <span class=\"popupHeader\">کارگاه های جاده ای</span>\
        </div>\
        <div class=\"popupHeaderContent\"> "+ data.Title + "\<div class=\"popUpBanner\"></div>\
        <div class=\"popupContent\">\
        <div class=\"popUpBox\"><span>استان</span><div class=\"popUpBoxContent\">"+ data.ProvinceNmae + "</div></div>\
        <div class=\"popUpBox\"><span>تاریخ شروع</span><div class=\"popUpBoxContent\">"+ roadOperationDate + "</div></div>\
        <div class=\"popUpBox\"><span>تاریخ اتمام</span><div class=\"popUpBoxContent\">"+ roadOperationEndDate + "</div></div>\
        <div class=\"popUpBox\"><span>ساعت شروع</span><div class=\"popUpBoxContent\">"+ roadOperationTime + "</div></div>\
        <div class=\"popUpBox\"><span>ساعت اتمام</span><div class=\"popUpBoxContent\">"+ roadOperationEndTime + "</div></div>\
        <div class=\"popUpBox\"><span>وضعیت تردد</span><div class=\"popUpBoxContent\">"+ data.PassingPossibilityName + "</div></div>\
        </div>\
        <div class=\"popupFooter\"><span>نوع عملیات راهداری</span><div class=\"popUpBoxContent\">"+ data.OperationName + "</div></div>\
        </div>";
    }
    case 3: {
      var roadBlockDate = data.StartDate;
      roadBlockDate = roadBlockDate.slice(0, 4) + "/" + roadBlockDate.slice(4,
        6) + "/" +
        roadBlockDate.slice(6, 8);
      // console.log(roadBlockDate);
      // customTimeFormat
      var roadBlockTime = data.StartTime;
      roadBlockTime = roadBlockTime.slice(0, 2) + ":" + roadBlockTime.slice(2, 4)
      // console.log("roadBlockTime : ", roadBlockTime);
      // Road Block Layer
      return "\
            <div class=\"popup-marker\">\
            <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/roadBlock.svg\" alt=\"\">\
            <span class=\"popupHeader\">انسدادها</span>\
            </div>\
            <div class=\"popupHeaderContent\"><span> "+ data.Title + "</span>\<div class=\"popUpBanner\"></div>\
            <div class=\"popupContent\">\
            <div class=\"popUpBox\"><span>استان</span><div class=\"popUpBoxContent\">"+ data.ProvinceNmae + "</div></div>\
            <div class=\"popUpBox\"><span>تاریخ شروع</span><div class=\"popUpBoxContent\">"+ roadBlockDate + "</div></div>\
            <div class=\"popUpBox\"><span>جهت</span><div class=\"popUpBoxContent\">"+ data.DirectionName + "</div></div>\
            <div class=\"popUpBox\"><span>ساعت شروع</span><div class=\"popUpBoxContent\">"+ roadBlockTime + "</div></div>\
            </div>\
            <div class=\"popupFooter\"><span>علت انسداد</span><div class=\"popUpBoxContent\">"+ data.BlockTitle + "</div></div>\
            </div>";
    }
    case 4: {
      if ("onvan" in data) {
        // Iran Khodro JSON
        return "\
            <div class=\"popup-marker\">\
            <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/emdad.svg\" alt=\"\">\
            <span class=\"popupHeader\">امداد فنی خودرو</span>\
            </div>\
            <div class=\"popupHeaderContent\">\
            <img src=\"assets/new_template/images/markers/IranKhodro.png\" alt=\"\">\
            <span>"+ data.onvan + "</span>\
            <div class=\"popUpBanner\"></div>\
            <div class=\"popupContent\">\
            <div class=\"popUpBox\"><span>شماره تماس</span><div class=\"popUpBoxContent\">096440</div></div>\
            <div class=\"popUpBox\"><span>تاریخ</span><div class=\"popUpBoxContent\">"+ returnDate(data.updated_at) + "</div></div>\
            <div class=\"popUpBox\"><span>زمان</span><div class=\"popUpBoxContent\">"+ returnTime(data.updated_at) + "</div></div>\
            </div>\
            </div>";
      }
      // Saipa JSON
      return "\
            <div class=\"popup-marker\">\
            <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/emdad.svg\" alt=\"\">\
            <span class=\"popupHeader\">امداد فنی خودرو</span>\
            </div>\
            <div class=\"popupHeaderContent\">\
            <img src=\"assets/new_template/images/markers/saipa.png\" alt=\"\">\
            <span>امداد خودرو سایپا</span>\
            <div class=\"popUpBanner\"></div>\
            <div class=\"popupContent\">\
            <div class=\"popUpBox\"><span>شماره تماس</span><div class=\"popUpBoxContent\">096550</div></div>\
            <div class=\"popUpBox\"><span>تاریخ</span><div class=\"popUpBoxContent\">"+ returnDate(data.updated_at) + "</div></div>\
            <div class=\"popUpBox\"><span>زمان</span><div class=\"popUpBoxContent\">"+ returnTime(data.updated_at) + "</div></div>\
            </div>\
            </div>";
    }
    case 5: {
      // Weather Layer
      return "\
            <div class=\"popup-marker\">\
            <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/weather.svg\" alt=\"\">\
            <span class=\"popupHeader\">وضعیت آب و هوا</span>\
            </div>\
            <div class=\"popupHeaderContent\"> "+ data.Station_Name + "\<div class=\"popUpBanner\"></div>\
            <div class=\"popupFooter\">\
            <div class=\"popUpBoxContent\" style=\"font-size:12px\">"+ (data.situation == "" ? "-" : data.situation) + "</div>\
            <span style=\"direction: rtl;\">وضعیت آب و هوا  :  </span>\
            </div>\
            <div class=\"popupContent\">\
            <div class=\"popUpBox\" style = \"width:45px\"><span>دمای فعلی</span><div class=\"popUpBoxContent\">"+ data.temperature.substr(0, 2) + " درجه</div></div>\
            <div class=\"popUpBox\" style = \"width:45px\"><span>رطوبت نسبی</span><div class=\"popUpBoxContent\">"+ data.wet.substr(0, 2) + "%</div></div>\
            <div class=\"popUpBox\" style = \"width:45px\"><span>سرعت باد</span><div class=\"popUpBoxContent\">" + data.speed.substr(0, 2) + " کیلومتر بر ساعت</div></div>\
            <div class=\"popUpBox\" style = \"width:45px\"><span>جهت باد</span><div class=\"popUpBoxContent\">" + data.wind + "</div></div>\
             </div>\
            </div>";
    }
    case 6: {
      // Fuel Station Layer
      return "\
            <div class=\"popup-marker\"  style=\"display: contents;\">\
            <div class=\"popupTitle\">\
            <img src=\"./assets/new_template/images/layer-icons/gazStation.svg\" alt=\"\">\
            <span class=\"popupHeader\">جایگاه سوخت</span>\
            </div>\
            <div class=\"popupHeaderContent\"> "+ data.station_name + "\<div class=\"popUpBanner\"></div>\
            <div class=\"popupContent\">\
            <div class=\"popUpBox\" style=\"width:100%\"><span>نوع جایگاه</span><div class=\"popUpBoxContent\">"  + returnStationType(data.SType) + "</div> </div>\
            <div class=\"popUpBox\" style=\"width:100%\"><span>آدرس</span><div class=\"popUpBoxContent\">"+ data.station_address + "</div></div>\
            </div>\
            </div>";
    }
    case 7: {
      // Complex Layer
      return "\
            <div class=\"popup-marker\" style=\"display: contents;\">\
            <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/complex.svg\" alt=\"\">\
            <span class=\"popupHeader\">مجتمع های خدمات رفاهی</span>\
            </div>\
            <div class=\"popupContent\">\
            <div class=\"popUpBox\" style=\"width:100%\"><span>نام مجتمع</span><div class=\"popUpBoxContent\">"  + (data.complex_name == "" ? "-" : data.complex_name) + "</div> </div>\
            <div class=\"popUpBox\" style=\"width:100%\"><span>محور</span><div class=\"popUpBoxContent\">"  + data.axis + "</div> </div>\
            <div class=\"popUpBox\" style=\"width:100%\"><span>استان</span><div class=\"popUpBoxContent\">"  + data.province + "</div> </div>\
            </div>\</div>";
    }
    case 8: {
      // Mosque Layer
      return "\
            <div class=\"popup-marker\" style=\"display: contents;\">\
            <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/mosque.svg\" alt=\"\">\
            <span class=\"popupHeader\">مساجد</span>\
            </div>\
            <div class=\"popupContent\">\
            <div class=\"popUpBox\" style=\"width:100%\"><span>نام مسجد</span><div class=\"popUpBoxContent\">"  + (data.mosque_name == "" ? "-" : data.mosque_name) + "</div> </div>\
            </div>\</div>";
    }
    case 9: {
      // customDateFormat
      var accidentDate = data.StartDate;
      accidentDate = accidentDate.slice(0, 4) + "/" + accidentDate.slice(4, 6) +
        "/" +
        accidentDate.slice(6, 8);
      // console.log(accidentDate);

      var accidentTime = data.StartTime.slice(0, 2) + ":" + data.StartTime.slice(2, 4);
      // console.log(accidentTime);

      // Road Accident Layer
      return "\
            <div class=\"popup-marker\">\
            <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/accident.svg\" alt=\"\">\
            <span class=\"popupHeader\">تصادفات</span>\
            </div>\
            <div class=\"popupHeaderContent\"> "+ data.Title + "\<div class=\"popUpBanner\"></div>\
            <div class=\"popupContent\">\
            <div class=\"popUpBox\"><span>استان</span><div class=\"popUpBoxContent\">"  + data.ProvinceName + "</div> </div>\
            <div class=\"popUpBox\"><span>تاریخ</span><div class=\"popUpBoxContent\">"+ accidentDate + "</div></div>\
            <div class=\"popUpBox\"><span>ساعت</span><div class=\"popUpBoxContent\">"+ accidentTime + "</div></div>\
            <div class=\"popUpBox\"><span>وسیله های درگیر سانحه</span><div class=\"popUpBoxContent\">"+ data.VehicleTypeNames + "</div></div>\
            <div class=\"popUpBox\"><span>نوع سانحه</span><div class=\"popUpBoxContent\">"+ data.CollisionShapeTitle + "</div></div>\
            </div>\</div>";
    }
    case 10: {
      // Toll House Layer
      return "\
        <div class=\"popup-marker\">\
          <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/tollHouse.svg\" alt=\"\">\
            <span class=\"popupHeader\">راهدارخانه</span>\
          </div>\
          <div class=\"popupHeaderContent\"><span> "+ data.toll_name + "</span>\<div class=\"popUpBanner\"></div>\
          <div class=\"popupContent\">\
            <div class=\"popUpBox\"><span>استان</span><div class=\"popUpBoxContent\">"+ data.province + "</div></div>\
            <div class=\"popUpBox\"><span>نام اداره</span><div class=\"popUpBoxContent\">"+ data.organization + "</div></div>\
            <div class=\"popUpBox\"><span>نوع راهدارخانه</span><div class=\"popUpBoxContent\">"+ data.t_type + "</div></div>\
            <div class=\"popUpBox\"><span>وضعیت</span><div class=\"popUpBoxContent\">"+ data.situation + "</div></div>\
          </div>\
        </div>";
    }
    case 11: {
      // Hospitals Layer
      return "\
            <div class=\"popup-marker\" style=\"display: contents;\">\
            <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/hospital.svg\" alt=\"\">\
            <span class=\"popupHeader\">بیمارستان ها</span>\
            </div>\
            <div class=\"popupContent\">\
            <div class=\"popUpBox\" style=\"width:100%\"><span>نام بیمارستان</span><div class=\"popUpBoxContent\">"+ (data.hos_name == "" ? "-" : data.hos_name) + "</div></div>\
            </div>\
            </div>";
    }
    case 12: {
      // Repair Layer
      return "\
            <div class=\"popup-marker\" style=\"display: contents;\">\
            <div class=\"popupTitle\">\
            <img src=\"assets/new_template/images/layer-icons/repair.svg\" alt=\"\">\
            <span class=\"popupHeader\">تعمیرگاه ها</span>\
            </div>\
            <div class=\"popupContent\">\
            <div class=\"popUpBox\" style=\"width:100%\"><div class=\"popUpBoxContent\">"+ (data.repair_name == "" ? "-" : data.repair_name) + "</div></div>\
            </div>\
            </div>";
    }
    case 13: {
      // Mosaferyar Layer
      return "\
        <div class=\"popup-marker\">\
        <div class=\"popupTitle\">\
        <img src=\"assets/new_template/images/layer-icons/passenger.svg\" alt=\"\">\
        <span class=\"popupHeader\">اطلاعات اتوبوس</span>\
        </div>\
        <div class=\"popupContent\">\
        <div class=\"popUpBox\" style = \"width:45px\"><span>نوع اتوبوس</span><div class=\"popUpBoxContent\">"+ data.type + "</div></div>\
        <div class=\"popUpBox\" style = \"width:45px\"><span>نام شرکت تعاونی</span><div class=\"popUpBoxContent\">"+ data.company + "</div></div>\
        <div class=\"popUpBox\" style = \"width:45px\"><span>مبدا</span><div class=\"popUpBoxContent\">"+ data.origin + "</div></div>\
        <div class=\"popUpBox\" style = \"width:45px\"><span>مقصد</span><div class=\"popUpBoxContent\">"+ data.destination + "</div></div>\
        <div class=\"popUpBox\" style = \"width:45px\"><span>شماره پلاک</span><div class=\"popUpBoxContent\">" + returnPelakNumber(pelak_number) + "</div></div>\
        <div class=\"popUpBox\" style = \"width:45px\"><span>زمان بروزرسانی</span><div class=\"popUpBoxContent\">" + data.updated_at.substring(data.updated_at.length - 8, data.updated_at.length) + "</div></div>\
          </div>\
        </div>";
    }
    default:
      break;
  }
}

function returnPelakNumber(pelak) {
  let pelak_array = pelak.split('-');
  return pelak_array[0] + ' ع ' + pelak_array[1] + ' ایران ' + pelak_array[2];
}

function returnStationType(type) {
  switch (type) {
    case "1":
      return "گازوئیل";
    case "2":
      return "بنزین";
    case "3":
      return "بنزین / گازوئیل";
    case "4":
      return "CNG";
    case "6":
      return "بنزین / CNG";
    case "7":
      return "بنزین / گازوئیل / CNG";

    default:
      return "نامشخص";
  }
}

// Custom popup styles
function getPopupCustomOptions(code) {
  switch (code) {
    case 1: {
      return {
        'className': 'customtraffic'
      }
    }
    case 2: {
      return {
        'className': 'customRoadOperation'
      }
    } case 3: {
      return {
        'className': 'customRoadBlock'
      }
    } case 4: {
      return {
        'className': 'customEmdadFani'
      }
    } case 5: {
      return {
        'className': 'customWeather'
      }
    } case 6: {
      return {
        'className': 'customgasStation'
      }
    } case 7: {
      return {
        'className': 'customComplex'
      }
    } case 8: {
      return {
        'className': 'customMosque'
      }
    } case 9: {
      return {
        'className': 'customRoadAccident'
      }
    } case 10: {
      return {
        'className': 'customTollhouse'
      }
    } case 11: {
      return {
        'className': 'customHospital'
      }
    }
    case 12: {
      return {
        'className': 'customRepair'
      }
    }
    default:
      break;
  }
}

// START Gregorian to Jalali helper functions
function div(a, b) {
  return parseInt((a / b));
}

function gregorian_to_jalali(g_y, g_m, g_d) {
  var g_days_in_month = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  var j_days_in_month = [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29];
  var jalali = [];
  var gy = g_y - 1600;
  var gm = g_m - 1;
  var gd = g_d - 1;

  var g_day_no = 365 * gy + div(gy + 3, 4) - div(gy + 99, 100) + div(gy + 399, 400);

  for (var i = 0; i < gm; ++i) g_day_no += g_days_in_month[i];
  if (gm > 1 && ((gy % 4 == 0 && gy % 100 != 0) || (gy %
    400 == 0)))
    /* leap and after Feb */
    g_day_no++;
  g_day_no += gd;

  var j_day_no = g_day_no - 79;

  var j_np = div(j_day_no, 12053);
  /* 12053 = 365*33 + 32/4 */
  j_day_no = j_day_no % 12053;

  var jy = 979 + 33 * j_np + 4 * div(j_day_no, 1461);
  /* 1461 = 365*4 + 4/4 */

  j_day_no %= 1461;

  if (j_day_no >= 366) {
    jy += div(j_day_no - 1, 365);
    j_day_no = (j_day_no - 1) % 365;
  }
  for (var i = 0; i < 11 && j_day_no >= j_days_in_month[i]; ++i)
    j_day_no -= j_days_in_month[i];
  var jm = i + 1;
  var jd = j_day_no + 1;
  jalali[0] = jy;
  jalali[1] = jm;
  jalali[2] = jd;
  return jalali;
  //return jalali[0] + "_" + jalali[1] + "_" + jalali[2];
  //return jy + "/" + jm + "/" + jd;
}
// END Gregorian to Jalali helper functions

function returnDate(dateTimeString) {
  const year = dateTimeString.substr(0, 4);
  const month = dateTimeString.substr(5, 2);
  const day = dateTimeString.substr(8, 2);
  const jalaliDate = gregorian_to_jalali(year, month, day);
  // console.log('jalaliDate', jalaliDate);
  return jalaliDate[0] + "/" + jalaliDate[1] + "/" + jalaliDate[2];
}

function returnTime(dateTimeString) {
  return dateTimeString.substr(11, 8);
}

// Close VerticalMenu
$('.menu-vertical-close').on('click', function () {
  let x = $('#RespVerticalMenuId');
  x.parent().css('display', 'none');
  x.css("transform", "scale(0)");
  $('.ResponsiveVerticalMenu').show();
});

// Open VerticalMenu in mobile width
function RespVerticalMenu(element) {
  let x = $('#RespVerticalMenuId');
  $(element).hide();
  $('.menu-vertical-close').show();

  if (x.parent().css('display') == 'none') {
    x.parent().css('display', 'block');
    x.css("transform", "scale(1)");

    $('.provincialcamera').css('left', '-310px');
    $('.traffic-box').css('left', '-310px');
    $('.maincoridr').css('left', '-95%');
    $('.emergencyNum').css('left', '-95%');
    $('.relatedWebsites').css('left', '-95%');
    $('.onlinetraffic').css('left', '-95%');

    if (!$('.leaflet-routing-container-hide').length > 0)
      $('.leaflet-routing-collapse-btn').click();
  } else {
    x.parent().css('display', 'none');
    x.css("transform", "scale(0)");
  }
}
// END open VerticalMenu in mobile width

// handle close cameraBox
function closeProvincialcameraBox() {
  $('.topMenuActiveBtn').removeClass('topMenuActiveBtn');

  if (windowWidth <= 768) {
    $('.provincialcamera').css('left', '-310px');
  } else {
    $('.provincialcamera').css('left', '-40%');
  }
}
// END handle close cameraBox

// handle close cameraBox
function closeTraficonlineBox() {
  $('.topMenuActiveBtn').removeClass('topMenuActiveBtn');

  if (windowWidth <= 768) {
    $('.onlinetraffic').css('left', '-310px');
  } else {
    $('.onlinetraffic').css('left', '-40%');
  }
}

// handle close trafficBox
function closeProvincialtrafficBox() {
  $('.topMenuActiveBtn').removeClass('topMenuActiveBtn');

  if (windowWidth <= 768) {
    $('.traffic-box').css('left', '-310px');
  } else {
    $('.traffic-box').css('left', '-40%');
  }
}
// END handle close trafficBox

// Open Routing Box
function openRoutingBox() {
  $('.routing-icon').hide('slow');
  $('.routing-box').show('slow');
}

// handle close routing box
function closeRoutingBox() {
  $('.topMenuActiveBtn').removeClass('topMenuActiveBtn');

  // if (windowWidth <= 768) {
  //   $('.routing-box').css('left', '-95%');
  // } else {
  //   $('.routing-box').css('left', '-40%');
  // }

  // Remove route details
  if ($('.route-detail-container').length) {
    $('.route-detail-container').remove();

    $('.routing-box').css({
      'border-bottom-left-radius': '7px',
      'border-bottom-right-radius': '7px'
    });
  }

  $('.routing-box').hide('slow');
  $('.routing-icon').show('slow');
}
// END handle close routing box

// handle close coridorBox
function closeProvincialcoridorBox() {
  $('.topMenuActiveBtn').removeClass('topMenuActiveBtn');

  if (windowWidth <= 768) {
    $('.maincoridr').css('left', '-95%');
  } else if (windowWidth <= 992) {
    $('.maincoridr').css('left', '-65%');
  } else {
    $('.maincoridr').css('left', '-40%');
  }
}
// END handle close coridorBox

// handle close triptime box
function closeTripTimeBox() {
  $('.topMenuActiveBtn').removeClass('topMenuActiveBtn');

  if (windowWidth <= 768) {
    $('.tripTime').css('left', '-726px');
  } else {
    $('.tripTime').css('left', '-60%');
  }
}
// END handle close triptime box

var fisheyeMenu = function (options) {
  // get all items from fisheye-menu
  var items = Array.prototype.slice.call(document.querySelectorAll('.fisheye-menu button.layerButton'));
  var item = items[0];
  //get horizontal center point of each item
  var itemsMiddle = items.map(function (item) { return item.offsetLeft + (item.offsetWidth / 2); });
  //get vertical center point of each item
  var itemsVertMiddle = items.map(function (item) { return item.offsetTop + (item.offsetHeight / 2); });

  var itemWidth = item.offsetWidth;
  // vertical limits of function running
  var topLimit = item.offsetParent.offsetTop - (options.verticalLimit || 10);
  var bottomLimit = item.offsetParent.offsetTop + item.offsetHeight + (options.verticalLimit || 10);
  // ratio of new size to default
  var growRatio = options.growRatio || 1.8;
  // horizontal limit of grow reaction for single item
  var limit = item.offsetWidth * (options.horizontalLimit || 1.5);

  var sizeDiff = Math.round(itemWidth * (growRatio - 1));

  var removeGoDown = function (evt) {
    evt.target.parentNode.classList.remove('go-down');
  };
  var setListeners = function () {
    items.forEach(function (itm, index) {
      itm.addEventListener("transitionend", removeGoDown);
    });
  }();

  var makeItemBigger = function (x, y) {
    var verticallyCorrect = (y > topLimit && y < bottomLimit) ? true : false;
    //console.log(item.offsetParent.offsetTop);


    // if mouse out from vertically cirrect area make items small with transition
    if (!verticallyCorrect && !item.parentNode.classList.contains('go-down')) {
      item.parentNode.classList.add('go-down');
    }

    // if (verticallyCorrect) {
    //    let menuWidth = $('.menu_vertical').css('width');
    //    $('.menu_vertical').css('width', 'calc(' + menuWidth + ' + 50px')
    // }

    items.forEach(function (item, index) {
      var middlePoint = itemsMiddle[index];
      // horizontal distance from center point to cursor
      var dist = Math.abs(x - middlePoint);

      // if distance is bigger than limit in any direction
      // ratio is equal to 1 (item size not changed)
      var ratio = (dist > limit) ? 1 : (dist / limit);

      ratio = verticallyCorrect ? ratio : 1;

      var newSize = itemWidth + ((1 - ratio) * sizeDiff);
      // add negative top margin to item equal to half size difference
      // to stay item vertical centered
      var newMarginTop = -((1 - ratio) * (sizeDiff / 2));
      item.style.width = newSize + 'px';
      item.style.height = newSize + 'px';
      item.style.marginTop = newMarginTop + 'px';
    });
  };

  document.body.addEventListener('mousemove', function (e) {
    // position of cursor
    var pointerX = e.pageX;
    var pointerY = e.pageY;
    makeItemBigger(pointerX, pointerY);
  });
};

function originInputAutoComplete(inp) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function (e) {
    var a, b, i, val = this.value;
    // console.log(val);
    /*close any already open lists of autocompleted values*/
    closeAllLists();
    if (!val) { return false; }
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items");
    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/
    if (val.length > 1) {
      // Request for location name
      let searchUrl = 'https://routingapi.rmto.ir/v1/search?term=' + val;
      $.ajax({
        url: searchUrl,
        type: "GET",
        dataType: "json",
        success: function (result) {
          // console.log(result);
          if (result.length) {
            for (i = 0; i < result.length; i++) {
              /*create a DIV element for each matching element:*/
              b = document.createElement("DIV");
              /*make the matching letters bold:*/
              b.innerHTML = "<strong>" + result[i].name.substr(0, val.length) + "</strong>";
              b.innerHTML += result[i].name.substr(val.length);
              /*insert a input field that will hold the current array item's value:*/
              b.innerHTML += "<input type='hidden' value='" + result[i].name + "'>";
              b.setAttribute('id', result[i].location[1] + ',' + result[i].location[0]);
              $(b).css('font-size', '12px');
              /*execute a function when someone clicks on the item value (DIV element):*/
              b.addEventListener("click", function (e) {
                /*insert the value for the autocomplete text field:*/
                inp.value = this.getElementsByTagName("input")[0].value;
                /*close the list of autocompleted values,
                (or any other open lists of autocompleted values:*/
                closeAllLists();
                // Hide instruction box & zoom to selected place
                let placeCoordinates = this.id.split(',');
                originLat = parseFloat(placeCoordinates[0]);
                originLng = parseFloat(placeCoordinates[1]);

                // if (markerLayers == null) {
                //   map.flyTo([originLat, originLng], 8);
                //   markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });
                // } else {
                //   console.log('removing old markers...');
                //   // map.removeLayer(markerLayers);
                //   // markerLayers = null;
                //   // markerList = [];
                //   clearMap();
                //   markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });
                //   destinationMarker = L.marker([destinationLat, destinationLng], {
                //     icon: L.icon({
                //       iconUrl: 'assets/new_template/images/icons/maps-end.png',
                //       iconSize: [24, 24]
                //     }),
                //   });
                //   markerList.push(destinationMarker);
                //   // markerLayers.addLayers(markerList);
                //   // map.addLayer(markerLayers);
                // }

                addOriginMarker();
              });
              a.appendChild(b);
            }
          }
        }
      });
    }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function (e) {
    var x = document.getElementById(this.id + "autocomplete-list");
    if (x) x = x.getElementsByTagName("div");
    if (e.keyCode == 13) {
      /*If the ENTER key is pressed, prevent the form from being submitted,*/
      e.preventDefault();
      if (currentFocus > -1) {
        /*and simulate a click on the "active" item:*/
        if (x) x[currentFocus].click();
      }
    }
  });
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });
}

function destinationInputAutoComplete(inp) {
  /*the autocomplete function takes two arguments,
  the text field element and an array of possible autocompleted values:*/
  var currentFocus;
  /*execute a function when someone writes in the text field:*/
  inp.addEventListener("input", function (e) {
    var a, b, i, val = this.value;
    // console.log(val);
    /*close any already open lists of autocompleted values*/
    closeAllLists();
    if (!val) { return false; }
    currentFocus = -1;
    /*create a DIV element that will contain the items (values):*/
    a = document.createElement("DIV");
    a.setAttribute("id", this.id + "autocomplete-list");
    a.setAttribute("class", "autocomplete-items-dest");
    /*append the DIV element as a child of the autocomplete container:*/
    this.parentNode.appendChild(a);
    /*for each item in the array...*/
    if (val.length > 1) {
      // Request for location name
      let searchUrl = 'https://routingapi.rmto.ir/v1/search?term=' + val;
      $.ajax({
        url: searchUrl,
        type: "GET",
        dataType: "json",
        success: function (result) {
          for (i = 0; i < result.length; i++) {
            /*create a DIV element for each matching element:*/
            b = document.createElement("DIV");
            /*make the matching letters bold:*/
            b.innerHTML = "<strong>" + result[i].name.substr(0, val.length) + "</strong>";
            b.innerHTML += result[i].name.substr(val.length);
            /*insert a input field that will hold the current array item's value:*/
            b.innerHTML += "<input type='hidden' value='" + result[i].name + "'>";
            b.setAttribute('id', result[i].location[1] + ',' + result[i].location[0]);
            $(b).css('font-size', '12px');
            /*execute a function when someone clicks on the item value (DIV element):*/
            b.addEventListener("click", function (e) {
              /*insert the value for the autocomplete text field:*/
              inp.value = this.getElementsByTagName("input")[0].value;
              /*close the list of autocompleted values,
              (or any other open lists of autocompleted values:*/
              closeAllLists();
              // Hide instruction box & zoom to selected place
              let placeCoordinates = this.id.split(',');
              destinationLat = parseFloat(placeCoordinates[0]);
              destinationLng = parseFloat(placeCoordinates[1]);
              // map.flyTo([parseFloat(placeCoordinates[0]), parseFloat(placeCoordinates[1])], 13);
              // if (markerLayers == null) {
              //   markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });
              // } else {
              //   map.removeLayer(markerLayers);
              //   markerLayers = null;
              //   markerList = [];
              //   markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });
              //   originMarker = L.marker([originLat, originLng], {
              //     icon: L.icon({
              //       iconUrl: 'assets/new_template/images/icons/maps-start.png',
              //       iconSize: [24, 24]
              //     }),
              //   });
              //   markerList.push(originMarker);
              //   // markerLayers.addLayers(markerList);
              //   // map.addLayer(markerLayers);
              // }

              addDestinationMarker();
            });
            a.appendChild(b);
          }
        }
      });
    }
  });
  /*execute a function presses a key on the keyboard:*/
  inp.addEventListener("keydown", function (e) {
    var x = document.getElementById(this.id + "autocomplete-list");
    if (x) x = x.getElementsByTagName("div");
    if (e.keyCode == 13) {
      /*If the ENTER key is pressed, prevent the form from being submitted,*/
      e.preventDefault();
      if (currentFocus > -1) {
        /*and simulate a click on the "active" item:*/
        if (x) x[currentFocus].click();
      }
    }
  });
  function closeAllLists(elmnt) {
    /*close all autocomplete lists in the document,
    except the one passed as an argument:*/
    var x = document.getElementsByClassName("autocomplete-items-dest");
    for (var i = 0; i < x.length; i++) {
      if (elmnt != x[i] && elmnt != inp) {
        x[i].parentNode.removeChild(x[i]);
      }
    }
  }
  /*execute a function when someone clicks in the document:*/
  document.addEventListener("click", function (e) {
    closeAllLists(e.target);
  });
}

function addOriginMarker() {
  if (originMarker != null) {
    map.removeLayer(originMarker);
  } else {
    map.flyTo([originLat, originLng], 8);
  }

  originMarker = L.marker([originLat, originLng], {
    icon: L.icon({
      iconUrl: 'assets/new_template/images/icons/maps-start.png',
      iconSize: [24, 24]
    }),
    draggable: true
  }).addTo(map);
  // markerList.push(originMarker);
  // markerLayers.addLayers(markerList);
  // map.addLayer(markerLayers);

  if (destinationMarker != null) {
    calculateRoute();
  }

  originMarker.on('dragend', function (event) {
    var marker = event.target;
    var position = marker.getLatLng();

    originLat = position.lat;
    originLng = position.lng;

    if (destinationLat != '')
      calculateRoute();
  });
}

function addDestinationMarker() {
  if (destinationMarker != null) {
    map.removeLayer(destinationMarker);
  }

  destinationMarker = L.marker([destinationLat, destinationLng], {
    icon: L.icon({
      iconUrl: 'assets/new_template/images/icons/maps-end.png',
      iconSize: [24, 24]
    }),
    draggable: true
  }).addTo(map);
  // markerList.push(destinationMarker);
  // markerLayers.addLayers(markerList);
  // map.addLayer(markerLayers);

  if (originMarker != null)
    calculateRoute();

  destinationMarker.on('dragend', function (event) {
    var marker = event.target;
    var position = marker.getLatLng();

    destinationLat = position.lat;
    destinationLng = position.lng;

    if (originLat != '')
      calculateRoute();
  });
}

function calculateRoute() {
  let originValue = $('#origin-input').val();
  let destinationValue = $('#destination-input').val();
  if (originValue && destinationValue) {
    layerSpinner.removeAttribute('hidden');
    $.ajax({
      url: "https://routingapi.rmto.ir/v1/routing",
      type: "GET",
      data: {
        origin: originLat + ',' + originLng,
        destination: destinationLat + ',' + destinationLng
      },
      dataType: "json",
      success: function (response) {
        // console.log(response);
        routes = response.routes;
        layerSpinner.setAttribute('hidden', '');
        // First remove remained layers, if there is any
        if ($(onlineTrafficLayer).length) {
          $(onlineTrafficLayer).each(function (index, element) {
            map.removeLayer(element);
          });
          onlineTrafficLayer = [];
        }
        for (let index = 0; index < popupArrays.length; index++) {
          map.removeLayer(popupArrays[index]);
        }
        popupArrays = [];
        // Show origin & destination markers on map
        for (let index = 1; index < response.routes.length; index++) {
          originLat = parseFloat(response.routes[index].cities[0].location[1]);
          originLng = parseFloat(response.routes[index].cities[0].location[0]);
          destinationLat = parseFloat(response.routes[index].cities[response.routes[index].cities.length - 1].location[1]);
          destinationLng = parseFloat(response.routes[index].cities[response.routes[index].cities.length - 1].location[0]);
          markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });
          // originMarker = L.marker([originLat, originLng], {
          //   icon: L.icon({
          //     iconUrl: 'assets/new_template/images/icons/maps-start.png',
          //     iconSize: [24, 24]
          //   }),
          // });
          // destinationMarker = L.marker([destinationLat, destinationLng], {
          //   icon: L.icon({
          //     iconUrl: 'assets/new_template/images/icons/maps-end.png',
          //     iconSize: [24, 24]
          //   }),
          // });
          // markerList.push(originMarker);
          // markerList.push(destinationMarker);
          // markerLayers.addLayers(markerList);
          // map.addLayer(markerLayers);

          // Show polyline on map
          let points = decode(response.routes[index].overview_polyline.points);
          // console.log(points);
          // Alternative routes
          onlineTrafficLayer.push(
            L.polyline(points,
              {
                color: '#888888',
                weight: 4,
                opacity: 0.7,
                lineJoin: 'round'
              }).addTo(map));
        }
        // Main Route
        originLat = parseFloat(response.routes[0].cities[0].location[1]);
        originLng = parseFloat(response.routes[0].cities[0].location[0]);
        destinationLat = parseFloat(response.routes[0].cities[response.routes[0].cities.length - 1].location[1]);
        destinationLng = parseFloat(response.routes[0].cities[response.routes[0].cities.length - 1].location[0]);
        markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });
        let points = decode(response.routes[0].overview_polyline.points);
        // Main route
        onlineTrafficLayer.push(
          L.polyline(points,
            {
              color: '#1e90ff',
              weight: 4,
              opacity: 0.7,
              lineJoin: 'round'
            }).addTo(map));
        map.fitBounds(onlineTrafficLayer[0].getBounds());
        showDistancePopup(response.routes);
      },
      error: function (xhr, status) {
        console.log("NOT Found :/");
      }
    });
  } else {
    console.log('NOT routing.');
  }
}

function showDistancePopup(routes) {
  var customOptions = {
    'maxWidth': '300',
    'maxHeight': '150',
    'className': 'customRouteEstimate'
  }

  for (let index = 0; index < routes.length; index++) {
    var popup = new L.Popup(customOptions);

    let divideBy = parseInt(routes[index].legs[0].steps.length / 2);
    // console.log(divideBy);
    let popupCoordinates = routes[index].legs[0].steps[divideBy].start_location;

    var popupLocation = new L.LatLng(popupCoordinates[1], popupCoordinates[0]);

    var popupContent = "<span style=\"color:#FF5630 ; font-size:11px\">مسافت: </span>" + routes[index].legs[0].distance.text +
      "<br> " + "<span style=\"color:#FF5630 ;font-size:11px\">زمان: </span>" + routes[index].legs[0].duration.text;

    popup.setLatLng(popupLocation);
    popup.setContent(popupContent);

    popupArrays.push(popup);
    map.addLayer(popup);
  }
}

function reverseRoute() {
  if (originMarker != null && destinationMarker != null) {
    // Remove route details
    if ($('.route-detail-container').length) {
      $('.route-detail-container').remove();

      $('.routing-box').css({
        'border-bottom-left-radius': '7px',
        'border-bottom-right-radius': '7px'
      });
    }

    // Change Input values
    let originInputValue = $('#origin-input').val();
    let destinationInputValue = $('#destination-input').val();
    let tempValue = originInputValue;
    $('#origin-input').val(destinationInputValue)
    $('#destination-input').val(tempValue)

    // Change Input Coordinates
    let tempLat = originLat;
    let tempLng = originLng;

    originLat = destinationLat;
    originLng = destinationLng;

    destinationLat = tempLat;
    destinationLng = tempLng;

    // Clear map
    clearMap();

    // Add origin & destination markers
    addOriginMarker();
    addDestinationMarker();

    // Re-route with new coordinates!
    calculateRoute();
  }
}

function toggleRouteDetails() {
  if (routes != null) {
    if (!$('.route-detail-container').length) {
      $('body').append('\
        <div class="route-detail-container">\
        </div>\
      ');
      for (let index = 0; index < routes.length; index++) {
        if (index == 0) {
          $('.route-detail-container').append('\
          <div class="route-detail-box" id="detail-box' + index + '">\
            <h2 class="routingDetailsClass">\
              <span class="routeDetails" onclick="routingToggleDetails(' + index + ')">جزئیات مسیر</span>\
              <span class="routeDetailsNumber">مسیر شماره ' + (index + 1) + '</span>\
              <br>\
              <span class="tripDetails">مسافت: </span>' + routes[index].legs[0].distance.text + '\
              <br>\
              <span class="tripDetails">زمان تقریبی: </span>' + routes[index].legs[0].duration.text + '\
            </h2>\
          </div>\
        ');
        } else {
          $('.route-detail-container').append('\
          <hr>\
          <div class="route-detail-box" id="detail-box' + index + '">\
            <h2 class="routingDetailsClass">\
              <span class="routeDetails" onclick="routingToggleDetails(' + index + ')">جزئیات مسیر</span>\
              <span class="routeDetailsNumber">مسیر شماره ' + (index + 1) + '</span>\
              <br>\
              <span class="tripDetails">مسافت: </span>' + routes[index].legs[0].distance.text + '\
              <br>\
              <span class="tripDetails">زمان تقریبی: </span>' + routes[index].legs[0].duration.text + '\
            </h2>\
          </div>\
        ');
        }
      }

      $('.routing-box').css({
        'border-bottom-left-radius': '0',
        'border-bottom-right-radius': '0'
      });
    } else {
      $('.route-detail-container').remove();

      $('.routing-box').css({
        'border-bottom-left-radius': '7px',
        'border-bottom-right-radius': '7px'
      });
    }
  }
}

function routingToggleDetails(routeNumber) {
  if (!$('#detail-box' + routeNumber).find('table').length) {
    for (let index = 0; index < routes.length; index++) {
      let detailBoxElement = $('#detail-box' + index);
      if (detailBoxElement.length) {
        detailBoxElement.find('table').remove();
      }
    }

    $('#detail-box' + routeNumber).append('\
      <table class="routing-detail-instructions">\
        <colgroup class="">\
          <col class="leaflet-routing-instruction-icon">\
          <col class="leaflet-routing-instruction-text">\
          <col class="leaflet-routing-instruction-distance">\
        </colgroup>\
        <tbody class="">\
        </tbody>\
      </table>\
    ');

    for (let index = 0; index < routes[routeNumber].legs[0].steps.length; index++) {
      switch (routes[routeNumber].legs[0].steps[index].maneuver) {
        case 'depart': {
          $('.routing-detail-instructions tbody').append('\
            <tr class="">\
              <td class=""><span class="leaflet-routing-icon leaflet-routing-icon-depart"></span></td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].instruction + '</td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].distance.text + '</td>\
            </tr>\
          ');
          break;
        }
        case 'slight-right': {
          $('.routing-detail-instructions tbody').append('\
            <tr class="">\
              <td class=""><span class="leaflet-routing-icon leaflet-routing-icon-bear-right"></span></td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].instruction + '</td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].distance.text + '</td>\
            </tr>\
          ');
          break;
        }
        case 'slight-left': {
          $('.routing-detail-instructions tbody').append('\
            <tr class="">\
              <td class=""><span class="leaflet-routing-icon leaflet-routing-icon-bear-left"></span></td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].instruction + '</td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].distance.text + '</td>\
            </tr>\
          ');
          break;
        }
        case 'sharp-left':
        case 'left': {
          $('.routing-detail-instructions tbody').append('\
            <tr class="">\
              <td class=""><span class="leaflet-routing-icon leaflet-routing-icon-sharp-left"></span></td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].instruction + '</td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].distance.text + '</td>\
            </tr>\
          ');
          break;
        }
        case 'sharp-right':
        case 'right': {
          $('.routing-detail-instructions tbody').append('\
            <tr class="">\
              <td class=""><span class="leaflet-routing-icon leaflet-routing-icon-sharp-right"></span></td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].instruction + '</td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].distance.text + '</td>\
            </tr>\
          ');
          break;
        }
        case 'arrive': {
          $('.routing-detail-instructions tbody').append('\
            <tr class="">\
              <td class=""><span class="leaflet-routing-icon leaflet-routing-icon-arrive"></span></td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].instruction + '</td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].distance.text + '</td>\
            </tr>\
          ');
          break;
        }
        case 'straight': {
          $('.routing-detail-instructions tbody').append('\
            <tr class="">\
              <td class=""><span class="leaflet-routing-icon leaflet-routing-icon-continue"></span></td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].instruction + '</td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].distance.text + '</td>\
            </tr>\
          ');
          break;
        }
        case 'exit-rotary': {
          $('.routing-detail-instructions tbody').append('\
            <tr class="">\
              <td class=""><span class="leaflet-routing-icon leaflet-routing-icon-enter-roundabout"></span></td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].instruction + '</td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].distance.text + '</td>\
            </tr>\
          ');
          break;
        }
        case 'uturn': {
          $('.routing-detail-instructions tbody').append('\
            <tr class="">\
              <td class=""><span class="leaflet-routing-icon leaflet-routing-icon-u-turn"></span></td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].instruction + '</td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].distance.text + '</td>\
            </tr>\
          ');
          break;
        }
        default: {
          $('.routing-detail-instructions tbody').append('\
            <tr class="">\
              <td class=""><span class="leaflet-routing-icon leaflet-routing-icon-continue"></span></td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].instruction + '</td>\
              <td class="">' + routes[routeNumber].legs[0].steps[index].distance.text + '</td>\
            </tr>\
          ');
          break;
        }
      }
    }
  } else {
    $('#detail-box' + routeNumber).find('table').remove();
  }
}

// START left menu boxes
function openLeftMenu(menu_index) {
  // First of all, clear all map contents
  isCoridor = false;
  clearMap();
  clearMosaferyar();
  if (menu_index != 1)
    closeRoutingBox();

  switch (menu_index) {
    // Routing
    case 1: {
      autoCompleteOriginElement.value = '';
      autoCompleteDestinationElement.value = '';

      if ($('.routing-box').css('display') == 'none') {
        $('#map-camera-select').val("00");
        openRoutingBox();
        if (windowWidth <= 1200) {
          $('.provincialcamera').css('left', '-310px');
          $('.traffic-box').css('left', '-310px');
          $('.maincoridr').css('left', '-95%');
          $('.onlinetraffic').css('left', '-95%');
          $('.relatedWebsites').css('left', '-95%');
          $('.emergencyNum').css('left', '-95%');
          $('.tripTime').css('left', '-95%');
        } else {
          $('.emergencyNum').css('left', '-40%');
          $('.provincialcamera').css('left', '-40%');
          $('.traffic-box').css('left', '-40%');
          $('.relatedWebsites').css('left', '-40%');
          if (windowWidth <= 992) {
            $('.maincoridr').css('left', '-65%');
          } else {
            $('.maincoridr').css('left', '-40%');
          }
          $('.onlinetraffic').css('left', '-40%');
          $('.tripTime').css('left', '-60%');
        }
      } else {
        closeRoutingBox();
      }
      break;
    }

    // Provincial Cameras
    case 2: {
      if (!$('.leaflet-routing-container-hide').length > 0)
        $('.leaflet-routing-collapse-btn').click();
      // Show Camera Province Box
      if ($('.provincialcamera').css('left') < '0') {

        $('.provincialcamera').css('left', '20px');
        $(".traffic-box > .form-map > svg #iran .map-selected").removeClass('map-selected');
        $('#map-traffic-select').val("00");

        if (windowWidth <= 768) {
          $('.traffic-box').css('left', '-310px');
          $('.maincoridr').css('left', '-95%');
          $('.emergencyNum').css('left', '-95%');
          $('.relatedWebsites').css('left', '-95%');
          $('.onlinetraffic').css('left', '-95%');
          $('.tripTime').css('left', '-95%');
        } else {
          $('.traffic-box').css('left', '-40%');
          if (windowWidth <= 992) {
            $('.maincoridr').css('left', '-65%');
          } else {
            $('.maincoridr').css('left', '-40%');
          }
          $('.emergencyNum').css('left', '-40%');
          $('.relatedWebsites').css('left', '-30%');
          $('.onlinetraffic').css('left', '-40%');
          $('.tripTime').css('left', '-60%');
        }

        // Get IRAN Map SVG if we didn't before.
        if (!$('.provincialcamera > .form-map > svg #iran').length) {
          cameraSpinner.removeAttribute('hidden');
          $.ajax({
            url: "https://141.ir/iranmap",
            type: 'GET'
          }).done(function (data) {
            cameraSpinner.setAttribute('hidden', '');
            $('.provincialcamera > .form-map').html('<span class="tooltiptextmap">Tooltip text</span>' + data);

            $(".provincialcamera > .form-map > svg #iran > path").mousemove(function (event) {
              let left = event.pageX - $('.provincialcamera > .form-map').offset().left + 20;
              let top = event.pageY - $('.provincialcamera > .form-map').offset().top;
              let mapSelectedId = $(this).attr('id').substring(3);
              $('.provincialcamera > .form-map > .tooltiptextmap').text($('#map-traffic-select [value=' + mapSelectedId + ']').text());
              $('.provincialcamera > .form-map > .tooltiptextmap').css({ top: top, left: left }).show();
            });
            $('.provincialcamera > .form-map > svg #iran > path').mouseout(function () {
              $('.provincialcamera > .form-map > .tooltiptextmap').hide();
            });

            $(".provincialcamera > .form-map > svg #iran path").on('click', function () {
              $(".provincialcamera > .form-map > svg #iran .map-selected").removeClass('map-selected');
              $(this).addClass('map-selected');
              let mapSelectedId = $(this).attr('id').substring(3);
              $('#map-camera-select').val(mapSelectedId);
              let provinceSelectedId = $('#map-camera-select').children("option:selected").attr('provinceId');
              let provinceSelectedLat = $('#map-camera-select').children("option:selected").attr('lat');
              let provinceSelectedLong = $('#map-camera-select').children("option:selected").attr('long');
              showCameraSlider(0, '/camera', provinceSelectedId, provinceSelectedLat, provinceSelectedLong);
            });
          });
        }
      } else {
        if (windowWidth <= '768') {
          $('.provincialcamera').css('left', '-310px');
        } else {
          $('.provincialcamera').css('left', '-40%');
        }
        $(".provincialcamera > .form-map > svg #iran .map-selected").removeClass('map-selected');
        $('#map-camera-select').val("00");
      }
      break;
    }

    // Online Traffic
    case 3: {
      if (!$('.leaflet-routing-container-hide').length > 0)
        $('.leaflet-routing-collapse-btn').click();

      if ($('.onlinetraffic').css('left') < '0') {
        $('.onlinetraffic').css('left', '20px');
        $(".traffic-box > .form-map > svg #iran .map-selected").removeClass('map-selected');
        $('#map-traffic-select').val("00");

        if (windowWidth <= 768) {
          $('.provincialcamera').css('left', '-310px');
          $('.traffic-box').css('left', '-310px');
          $('.maincoridr').css('left', '-95%');
          $('.emergencyNum').css('left', '-95%');
          $('.relatedWebsites').css('left', '-95%');
          $('.tripTime').css('left', '-95%');
        } else {
          $('.provincialcamera').css('left', '-40%');
          $('.traffic-box').css('left', '-40%');
          if (windowWidth <= 992) {
            $('.maincoridr').css('left', '-65%');
          } else {
            $('.maincoridr').css('left', '-40%');
          }
          $('.emergencyNum').css('left', '-40%');
          $('.relatedWebsites').css('left', '-30%');
          $('.tripTime').css('left', '-60%');
        }

        // Get IRAN Map SVG if we didn't before.
        if (!$('.onlinetraffic > .form-map > svg #iran').length) {
          trafficOnlineSpinner.removeAttribute('hidden');
          $.ajax({
            url: "https://141.ir/iranmap",
            type: 'GET'
          }).done(function (data) {
            trafficOnlineSpinner.setAttribute('hidden', '');
            $('.onlinetraffic > .form-map').html('<span class="tooltiptextmap">Tooltip text</span>' + data);

            $(".onlinetraffic > .form-map > svg #iran > path").mousemove(function (event) {
              let left = event.pageX - $('.onlinetraffic > .form-map').offset().left + 20;
              let top = event.pageY - $('.onlinetraffic > .form-map').offset().top;
              let mapSelectedId = $(this).attr('id').substring(3);
              $('.onlinetraffic > .form-map > .tooltiptextmap').text($('#map-traffic-select [value=' + mapSelectedId + ']').text());
              $('.onlinetraffic > .form-map > .tooltiptextmap').css({ top: top, left: left }).show();
            });
            $('.onlinetraffic > .form-map > svg #iran > path').mouseout(function () {
              $('.onlinetraffic > .form-map > .tooltiptextmap').hide();
            });

            $(".onlinetraffic > .form-map > svg #iran path").on('click', function () {
              $(".onlinetraffic > .form-map > svg #iran .map-selected").removeClass('map-selected');
              $(this).addClass('map-selected');
              let mapSelectedId = $(this).attr('id').substring(3);
              $('#map-camera-select').val(mapSelectedId);
              let provinceSelectedId = $('#map-camera-select').children("option:selected").attr('provinceId');
              let provinceSelectedLat = $('#map-camera-select').children("option:selected").attr('lat');
              let provinceSelectedLong = $('#map-camera-select').children("option:selected").attr('long');
              showTrafficLayer('/getonlinemapbyprovince', provinceSelectedId, provinceSelectedLat, provinceSelectedLong);
            });
          });
        }
      } else {
        if (windowWidth <= '768') {
          $('.onlinetraffic').css('left', '-310px');
        } else {
          $('.onlinetraffic').css('left', '-40%');
        }

        $(".onlinetraffic > .form-map > svg #iran .map-selected").removeClass('map-selected');
        $('#map-camera-select').val("00");
      }
      break;
    }

    // Graphical Traffic
    case 4: {
      if (!$('.leaflet-routing-container-hide').length > 0)
        $('.leaflet-routing-collapse-btn').click();
      if ($('.traffic-box').css('left') < '0') {
        $('.traffic-box').css('left', '20px');
        $(".traffic-box > .form-map > svg #iran .map-selected").removeClass('map-selected');

        if (windowWidth <= 768) {
          $('.provincialcamera').css('left', '-310px');
          $('.maincoridr').css('left', '-95%');
          $('.emergencyNum').css('left', '-95%');
          $('.relatedWebsites').css('left', '-95%');
          $('.onlinetraffic').css('left', '-95%');
          $('.tripTime').css('left', '-95%');
        } else {
          $('.provincialcamera').css('left', '-40%');
          if (windowWidth <= 992) {
            $('.maincoridr').css('left', '-65%');
          } else {
            $('.maincoridr').css('left', '-40%');
          }
          $('.emergencyNum').css('left', '-40%');
          $('.relatedWebsites').css('left', '-30%');
          $('.onlinetraffic').css('left', '-40%');
          $('.tripTime').css('left', '-60%');
        }

        // Get IRAN Map SVG if we didn't before.
        if (!$('.traffic-box > .form-map > svg #iran').length) {
          trafficSpinner.removeAttribute('hidden');
          $.ajax({
            url: "https://141.ir/iranmap",
            type: 'GET'
          }).done(function (data) {
            trafficSpinner.setAttribute('hidden', '');
            $('.traffic-box > .form-map').html('<span class="tooltiptextmap">Tooltip text</span>' + data);

            $(".traffic-box>.form-map>svg #iran>path").mousemove(function () {
              let left = event.pageX - $('.traffic-box>.form-map').offset().left + 20;
              let top = event.pageY - $('.traffic-box>.form-map').offset().top;
              let mapSelectedId = $(this).attr('id').substring(3);
              $('.traffic-box>.form-map>.tooltiptextmap').text($('#map-traffic-select [value=' + mapSelectedId + ']').text());
              $('.traffic-box>.form-map>.tooltiptextmap').css({ top: top, left: left }).show();
            });
            $('.traffic-box>.form-map>svg #iran>path').mouseout(function () {
              $('.traffic-box>.form-map>.tooltiptextmap').hide();
            });

            $(".traffic-box > .form-map > svg #iran path").on('click', function () {
              $(".traffic-box > .form-map > svg #iran .map-selected").removeClass('map-selected');
              $(this).addClass('map-selected');
              let mapSelectedId = $(this).attr('id').substring(3);
              $('#map-traffic-select').val(mapSelectedId);
              let provinceSelectedId = $('#map-traffic-select').children("option:selected").attr('provinceId');
              showTrafficPhotoBox(provinceSelectedId);
            });
          });
        }
      } else {
        if (windowWidth <= '768') {
          $('.traffic-box').css('left', '-310px');
        } else {
          $('.traffic-box').css('left', '-40%');
        }

        $(".traffic-box > .form-map > svg #iran .map-selected").removeClass('map-selected');
        $('#map-traffic-select').val("00");
      }
      break;
    }

    // Main Coridors
    case 5: {
      if (!$('.leaflet-routing-container-hide').length > 0)
        $('.leaflet-routing-collapse-btn').click();
      if ($('.maincoridr').css('left') < '0') {

        if (windowWidth <= 768) {
          $('.provincialcamera').css('left', '-310px');
          $('.traffic-box').css('left', '-310px');
          $('.emergencyNum').css('left', '-95%');
          $('.relatedWebsites').css('left', '-95%');
          $('.onlinetraffic').css('left', '-95%');
          $('.tripTime').css('left', '-95%');
          $('.maincoridr').css('left', '5%');
        } else {
          $('.provincialcamera').css('left', '-40%');
          $('.traffic-box').css('left', '-40%');
          $('.emergencyNum').css('left', '-40%');
          $('.relatedWebsites').css('left', '-30%');
          $('.onlinetraffic').css('left', '-40%');
          $('.tripTime').css('left', '-60%');
          $('.maincoridr').css('left', '20px');
        }

        $('#map-camera-select').val("00");
      } else {
        if (windowWidth <= '768') {
          $('.maincoridr').css('left', '-95%');
        } else if (windowWidth <= '992') {
          $('.maincoridr').css('left', '-65%');
        } else {
          $('.maincoridr').css('left', '-40%');
        }
      }
      if ($(".leaflet-popup-close-button").length) {
        $(".leaflet-popup-close-button")[0].click();
      }
      break;
    }

    // Trip Time
    case 6: {
      if (!$('.leaflet-routing-container-hide').length > 0)
        $('.leaflet-routing-collapse-btn').click();
      // Show Camera Province Box
      if ($('.tripTime').css('left') < '0') {
        $('.tripTime').css('left', '20px');
        $(".traffic-box > .form-map > svg #iran .map-selected").removeClass('map-selected');
        $('#map-traffic-select').val("00");

        if (windowWidth <= 768) {
          $('.provincialcamera').css('left', '-95%');
          $('.traffic-box').css('left', '-310px');
          $('.maincoridr').css('left', '-95%');
          $('.emergencyNum').css('left', '-95%');
          $('.relatedWebsites').css('left', '-95%');
          $('.onlinetraffic').css('left', '-95%');
        } else {
          $('.provincialcamera').css('left', '-40%');
          $('.traffic-box').css('left', '-40%');
          if (windowWidth <= 992) {
            $('.maincoridr').css('left', '-65%');
          } else {
            $('.maincoridr').css('left', '-40%');
          }
          $('.emergencyNum').css('left', '-40%');
          $('.relatedWebsites').css('left', '-30%');
          $('.onlinetraffic').css('left', '-40%');
        }

        if (tripTimeData == null) {
          $.post(API_ENDPOINT + '/gettrtime',
            {
              'device_type': returnDeviceType()
            },
            function (result) {
              //console.log(result);
              tripTimeData = result;
              showTripTimeRows();
            });
        } else {
          showTripTimeRows();
        }
      } else {
        if ($(window).width() <= '768')
          $('.tripTime').css('left', '-90%');
        else
          $('.tripTime').css('left', '-40%');
      }
      break;
    }

    // Emergency Numbers
    case 7: {
      if (!$('.leaflet-routing-container-hide').length > 0)
        $('.leaflet-routing-collapse-btn').click();
      if ($('.emergencyNum').css('left') < '0') {

        $('#map-camera-select').val("00");
        if (windowWidth <= 768) {
          $('.provincialcamera').css('left', '-310px');
          $('.traffic-box').css('left', '-310px');
          $('.maincoridr').css('left', '-95%');
          $('.onlinetraffic').css('left', '-95%');
          $('.emergencyNum').css('left', '5%');
          $('.relatedWebsites').css('left', '-95%');
          $('.tripTime').css('left', '-95%');
        } else {
          $('.provincialcamera').css('left', '-40%');
          $('.traffic-box').css('left', '-40%');
          $('.relatedWebsites').css('left', '-30%');
          if (windowWidth <= 992) {
            $('.maincoridr').css('left', '-65%');
          } else {
            $('.maincoridr').css('left', '-40%');
          }
          $('.onlinetraffic').css('left', '-40%');
          $('.tripTime').css('left', '-60%');
          $('.emergencyNum').css('left', '20px');
        }
      } else {
        if (windowWidth <= '768') {
          $('.emergencyNum').css('left', '-95%');
        } else {
          $('.emergencyNum').css('left', '-40%');
        }
      }
      break;
    }

    // Related websites
    case 8: {
      if (!$('.leaflet-routing-container-hide').length > 0)
        $('.leaflet-routing-collapse-btn').click();
      if ($('.relatedWebsites').css('left') < '0') {
        $('#map-camera-select').val("00");
        if (windowWidth <= 768) {
          $('.provincialcamera').css('left', '-310px');
          $('.traffic-box').css('left', '-310px');
          $('.maincoridr').css('left', '-95%');
          $('.onlinetraffic').css('left', '-95%');
          $('.relatedWebsites').css('left', '5%');
          $('.emergencyNum').css('left', '-95%');
          $('.tripTime').css('left', '-95%');
        } else {
          $('.emergencyNum').css('left', '-40%');
          $('.provincialcamera').css('left', '-40%');
          $('.traffic-box').css('left', '-40%');
          if (windowWidth <= 992) {
            $('.maincoridr').css('left', '-65%');
          } else {
            $('.maincoridr').css('left', '-40%');
          }
          $('.onlinetraffic').css('left', '-40%');
          $('.tripTime').css('left', '-60%');
          $('.relatedWebsites').css('left', '20px');
        }
      } else {
        if (windowWidth <= '768') {
          $('.relatedWebsites').css('left', '-95%');
        } else {
          $('.relatedWebsites').css('left', '-30%');
        }
      }
      break;
    }

    default:
      return;
  }
}
// END left menu boxes

// Remove Layers, Popups and Controls from map
function clearMap() {
  // Remove all controls
  if ($(routeControls).length) {
    $(routeControls).each(function (index, control) {
      map.removeControl(control);
      routeControls.splice(routeControls.indexOf(index), 1);
    });
  }

  // Remove Traffic Layer
  if ($(onlineTrafficLayer).length) {
    $(onlineTrafficLayer).each(function (index, element) {
      map.removeLayer(element);
    });
    onlineTrafficLayer = [];
  }

  // Remove all layers
  if (markerLayers != null) {
    let activeLayerElements = document.querySelectorAll('.activeLayer');
    if ($(activeLayerElements).length) {
      $(activeLayerElements).each(function (index, element) {
        $(element).find('img').attr('src', returnLayerImage(layerFlags.findIndex(function (flag) {
          return flag == 1;
        }), 1));
        $(element).removeClass('activeLayer');
      });
    }

    map.removeLayer(markerLayers);
    markerLayers = null;
    markerList = [];
    layerFlags = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
  }

  // Routing origin & destination markers
  if (originMarker != null)
    map.removeLayer(originMarker);
  if (destinationMarker != null)
    map.removeLayer(destinationMarker);

  // remove all popups
  $(".leaflet-popup-close-button").each(function (index, element) {
    element.click();
  });

  // Remove route details
  if ($('.route-detail-container').length) {
    $('.route-detail-container').remove();

    $('.routing-box').css({
      'border-bottom-left-radius': '7px',
      'border-bottom-right-radius': '7px'
    });
  }
}

function showCameraSlider(code, url, specificId, specificLat, specificLong) {
  var onCameraMarkerClick = function (e) {
    let cameraTitle = this.options.title;
    layerSpinner.removeAttribute('hidden');
    $.post(API_ENDPOINT + url + '/' + this.options.cameraId,
      {
        'device_type': returnDeviceType()
      },
      function (data) {
        //console.log('data', data);
        layerSpinner.setAttribute('hidden', '');
        if (data.status) {
          let dataLength = data.five_sorted.length;
          $("#slideshow-slide").find('.mySlides').remove();
          $("#slideshow-dot").find('.dot').remove();
          $(data.five_sorted).each(function (index, value) {
            let cameraDate = moment.unix(data.five_sorted[index].mtime).format("jYYYY/jM/jD");
            let cameraTime = moment.unix(data.five_sorted[index].mtime).format("HH:mm:ss");
            let divSlide = '<div class="mySlides"><div class="numbertext">'
              + dataLength + ' / ' + (index + 1) + '</div><img src="'
              + value['down_link'] + '"><div class="text">نام دوربین: ' + cameraTitle + ' - تاریخ: ' + cameraDate + '، ساعت: ' + cameraTime + '</div></div>';
            $('#slideshow-slide').append(divSlide);
            $('#slideshow-dot').append('<span class="dot" onclick="currentCameraSlide(' + (index + 1) + ')"></span>');
          });
          if (dataLength > 0) {
            cameraSlideIndex = 1;
            cameraShowSlides(cameraSlideIndex);
            $("#provincialCameraModal").css('display', 'block');
          }
        }
      });
  }

  map.flyTo([specificLat, specificLong], 7);

  if ($('.provincialcamera').css('left') < '0') {
    $('.provincialcamera').css('left', '20px');
  } else {
    if (windowWidth <= "768") {
      $('.provincialcamera').css('left', '-310px');
    } else {
      $('.provincialcamera').css('left', '-40%');
    }
  }

  if (markerLayers != null) {
    layerFlags = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    map.removeLayer(markerLayers);
    markerLayers = null;
    markerList = [];
  }

  layerSpinner.removeAttribute('hidden');
  $.post(API_ENDPOINT + url,
    {
      'device_type': returnDeviceType()
    },
    function (data) {
      layerSpinner.setAttribute('hidden', '');
      markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });

      if (code == 0) {
        for (var i = 0; i < data.length; i++) {
          if (data[i].ProvinceID == specificId || (specificId == 29 && data[i].ProvinceID == 32)) {
            var marker = L.marker(L.latLng(data[i].lat, data[i].lng), {
              icon: getMarkerIcon(code, data[i]),
              title: data[i].Title,
              cameraId: data[i].IPInfo
            }).on('click', onCameraMarkerClick);
            markerList.push(marker);
          }
        }
      }
      markerLayers.addLayers(markerList);
      map.addLayer(markerLayers);
    });
}

function showTrafficLayer(url, provinceSelectedId, provinceSelectedLat, provinceSelectedLong) {
  if (windowWidth <= "768") {
    $('.onlinetraffic').css('left', '-310px');
  } else {
    $('.onlinetraffic').css('left', '-40%');
  }

  $.post(API_ENDPOINT + url,
    {
      'id': provinceSelectedId,
      'device_type': returnDeviceType()
    },
    function (data) {
      // console.log(data);
      data.forEach(element => {
        // routeControls.push(L.Routing.control({
        //   waypoints: [
        //     L.latLng(parseFloat(element.src_lat), parseFloat(element.src_lng)),
        //     L.latLng(parseFloat(element.dst_lat), parseFloat(element.dst_lng))
        //   ],
        //   lineOptions: {
        //     addWaypoints: false,
        //     styles: [{
        //       color: returnTrafficColor(element.otf_color),
        //       opacity: 1,
        //       weight: 3
        //     }]
        //   },
        //   fitSelectedRoutes: false,
        //   draggableWaypoints: false,
        //   createMarker: function () { return null; },
        // }).addTo(map));

        var polylinePoints = decode(element.geometry_str);
        onlineTrafficLayer.push(L.polyline(polylinePoints, { color: returnTrafficColor(element.otf_color) }).addTo(map));
      });

      if ($('.leaflet-routing-container').length) {
        $('.leaflet-routing-container').remove();
      }
      if (markerLayers != null) {
        layerFlags = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        map.removeLayer(markerLayers);
        markerLayers = null;
        markerList = [];
      }
    });
  map.flyTo([provinceSelectedLat, provinceSelectedLong], 8);
}

function returnTrafficColor(colorCode) {
  switch (colorCode) {
    case '0': // عدم وجود اطلاعات
      return '#ACACAC';
    case '1': // جریان آزاد
      return '#48C649';
    case '2': // روان
      return '#48C649';
    case '3': // نیمه روان
      return '#48C649';
    case '4': // نیمه سنگین
      return '#FFF000';
    case '5': // سنگین
      return '#A20A09';
    case '6': // راه بندان
      return '#000000';
    case '7': // عدم وجود اطلاعات
      return '#ACACAC';
    default:
      return '#ACACAC';
  }
}

function showTrafficPhotoBox(provinceId) {
  $('#trafficPhotoModal .photo > img').attr('src', 'https://141.ir/wayonlinetraffic/' + provinceId);
  $('#trafficPhotoModal').css('display', 'block');
}

function showTripTimeRows() {
  numberOfPoints = 0;
  if ($('.triptimebox').length) {
    $('.test-tr').remove();
  }
  $(tripTimeData.rows).each(function (index, element) {
    // console.log(element.RID);
    $('.triptimebox').append(
      '<div class="test-tr" id=\'tr-row' + element.RID + '\' onclick="showRoad(' + element.RID + ' , \'' + element.R_N + '\')">\
        <span class="span1"> مسیر ' + element.R_N.substring(0, element.R_N.indexOf('به')) + '\
        </span>\
        <span class="colordot">\
          ...................\
        </span>\
        <img src="./assets/new_template/images/icons/car-2.svg">\
        <span class="colordot">\
          ................\
        </span>\
        <span class="span2">\
          ' + element.R_N.substring(element.R_N.indexOf('به') + 2, element.R_N.length) + '\
        </span>\
      </div>'
    );
  });
}

function showRoad(road_id, road_name) {
  // console.log(road_id);
  selectedRoadName = road_name;
  tripTimeSelectedData = [];
  numberOfCreatedRouts = 0;
  sum = 0;

  $(tripTimeData.data).each(function (index, element) {
    if (element.RID == road_id) {
      tripTimeSelectedData.push(element);
    }
  });
  $(tripTimeSelectedData).each(function (index, element) {
    // console.log('element', element);
    sum += parseInt(element.P_Dist);
    tripTimeRouting(element.Lat_From, element.Lng_From, element.Lat_To, element.Lng_To, index);
  });
  // console.log(tripTimeSelectedData.length);
  map.flyTo([tripTimeSelectedData[parseInt(tripTimeSelectedData.length / 2)].Lat_From, tripTimeSelectedData[parseInt(tripTimeSelectedData.length / 2)].Lng_From.substring(0, 8)], 9);
}

// Calculate routing + geometry parameter and show path on map
function tripTimeRouting(latfirst, lngfirst, latsecond, lngsecond, routeIndex) {
  numberOfCreatedRouts++;
  if ($('.tripTime').css('left') < '0') {
    $('.tripTime').css('left', '20px');
  } else {
    $('.tripTime').css('left', '-95%');
  }

  routeArray = [];
  popupArrays = [];
  routeControls.push(L.Routing.control({
    routeWhileDragging: true,
    reverseWaypoints: true,
    addWaypoints: false,
    fitSelectedRoutes: false,
    waypoints: [
      L.latLng(latfirst, lngfirst),
      L.latLng(latsecond, lngsecond)
    ],
    lineOptions: {
      styles: [{
        color: '#5789f2',
        opacity: 1,
        weight: 5
      }]
    },
    createMarker: function (i, wp) {
      isFirstRoute = false;
      // console.log('numberOfCreatedRouts: ', numberOfCreatedRouts);
      // console.log('tripTimeSelectedData.lenth: ', tripTimeSelectedData.length);
      if (numberOfCreatedRouts == 1) {
        return L.marker(wp.latLng, { icon: sourceIcon });
      }
      else if (numberOfCreatedRouts == tripTimeSelectedData.length) {
        numberOfCreatedRouts++;
        if (numberOfCreatedRouts == 3)
          return L.marker(wp.latLng, { icon: destinationIcon }).bindPopup("<span style=\"font-weight: bold\">مسیر " + tripTimeSelectedData[routeIndex].RP_N + "</span><br><span style=\"color:#FF5630 ; font-size:11px\">مسافت : </span>" + tripTimeSelectedData[routeIndex].P_Dist / 1000 + " کیلومتر" + "</span><br><span style=\"color:#FF5630 ; font-size:11px\">زمان سفر : </span>" + returnTripTime(tripTimeSelectedData[routeIndex].FT));
      }
      else if (numberOfCreatedRouts == tripTimeSelectedData.length + 1) {
        isFirstRoute = true;
        return L.marker(wp.latLng, { icon: sourceIcon });
      }
      else
        return L.marker(wp.latLng, { icon: destinationIcon }).bindPopup("<span style=\"font-weight: bold\">مسیر " + tripTimeSelectedData[routeIndex].RP_N + "</span><br><span style=\"color:#FF5630 ; font-size:11px\">مسافت : </span>" + tripTimeSelectedData[routeIndex].P_Dist / 1000 + " کیلومتر" + "</span><br><span style=\"color:#FF5630 ; font-size:11px\">زمان سفر : </span>" + returnTripTime(tripTimeSelectedData[routeIndex].FT));
    }
  }).on('routesfound', function (e) {
    if (isFirstRoute) {
      isFirstRoute = false;
      // console.log(sum);
      var customOptions = {
        'maxWidth': '300',
        'maxHeight': '150',
        'className': 'tripTimePopUp'
      }

      var popup = new L.Popup(customOptions);

      // var popupLocation = new L.LatLng(tripTimeSelectedData[tripTimeSelectedData.length - 1].Lat_From, tripTimeSelectedData[tripTimeSelectedData.length - 1].Lng_From);
      var popupLocation = new L.LatLng(
        e.routes[0].coordinates[0].lat,
        e.routes[0].coordinates[0].lng);

      var popupContent =
        "<div class=\"popup-marker\">\
          <div class=\"popupTitle\">\
          <img src=\"assets/new_template/images/icons/tr_timer.svg\" alt=\"\">\
          <span class=\"popupHeader\" style=\"font-weight: bold; color: #000\">مسیر " + selectedRoadName + "</span>\
            </div>" +
        "<div class=\"popupContent\">\
          <div class=\"popUpBox\" style=\"width:75px\">\
          <span>مسافت کل </span><div class=\"popUpBoxContent\">" +
        sum / 1000 + " کیلومتر" + "</div> </div>\
          <div class=\"popUpBox\" style=\"width:75px\">\
          <span>زمان کل </span><div class=\"popUpBoxContent\">" +
        returnTotalTime(tripTimeSelectedData[routeIndex].TFT) + "</div></div>\
          <br><span style=\"color:#FF5630 ;font-size:11px\">\
          </div>";

      popup.setLatLng(popupLocation);
      popup.setContent(popupContent);

      popupArrays.push(popup);
      map.addLayer(popup);
    }
  }).addTo(map));

  $('.leaflet-routing-container').css('display', 'none');
  $('.leaflet-routing-container-hide').css('display', 'none');
  if (markerLayers != null) {
    layerFlags = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    map.removeLayer(markerLayers);
    markerLayers = null;
    markerList = [];
  }
}

function returnTripTime(tr_time) {
  if (tr_time == null) {
    return "نامشخص";
  } else return (tr_time + " دقیقه");
}

function returnTotalTime(tr_time) {
  if (tr_time == '0') {
    return "نامشخص";
  } else return (tr_time + " دقیقه");
}

function currentCameraSlide(slideIndex) {
  cameraShowSlides(cameraSlideIndex = slideIndex);
}

$(document).ready(function () {
  // Routing functionality
  originInputAutoComplete(autoCompleteOriginElement);
  destinationInputAutoComplete(autoCompleteDestinationElement);

  // Download app modal for mobile/tablet users
  if ($(window).width() < 767)
    if (isFirstSeen())
      $("#downloadapp").modal('show')

  // Check if user comes from other page
  $.urlParam = function (name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)')
      .exec(window.location.search);

    return (results !== null) ? results[1] || 0 : false;
  }

  var queryParameterId = $.urlParam('id');
  if (queryParameterId >= 1 && queryParameterId <= 8) {
    // console.log('queryParameterId: ', queryParameterId);
    if (queryParameterId >= 1 && queryParameterId <= 6) {
      $(".menu_top > button:eq(" + (queryParameterId - 1) + ")").click();
    }
    openLeftMenu(parseInt(queryParameterId));
  }

  $('.exitemergency').on('click', function () {
    if ($('.emergencyNum').css('left') < '0') {

      $('#map-camera-select').val("00");

      if (windowWidth <= 768) {
        $('.provincialcamera').css('left', '-310px');
        $('.traffic-box').css('left', '-310px');
        $('.maincoridr').css('left', '-95%');
        $('.onlinetraffic').css('left', '-95%');
        $('.emergencyNum').css('left', '5%');
        $('.relatedWebsites').css('left', '-95%');
        $('.tripTime').css('left', '-95%');

      } else {
        $('.provincialcamera').css('left', '-40%');
        $('.traffic-box').css('left', '-40%');
        $('.relatedWebsites').css('left', '-30%');
        if (windowWidth <= 992) {
          $('.maincoridr').css('left', '-65%');
        } else {
          $('.maincoridr').css('left', '-40%');
        }
        $('.onlinetraffic').css('left', '-40%');
        $('.tripTime').css('left', '-60%');

        $('.emergencyNum').css('left', '20px');
      }
    } else {
      if (windowWidth <= '768') {
        $('.emergencyNum').css('left', '-95%');
      } else {
        $('.emergencyNum').css('left', '-40%');
      }
    }
  });
  //end emergnecy num index

  $('.exitewebsites').on('click', function () {
    if ($('.relatedWebsites').css('left') < '0') {

      $('#map-camera-select').val("00");

      if (windowWidth <= 768) {
        $('.provincialcamera').css('left', '-310px');
        $('.traffic-box').css('left', '-310px');
        $('.maincoridr').css('left', '-95%');
        $('.onlinetraffic').css('left', '-95%');
        $('.relatedWebsites').css('left', '5%');
        $('.emergencyNum').css('left', '-95%');
        $('.tripTime').css('left', '-95%');

      } else {
        $('.provincialcamera').css('left', '-40%');
        $('.traffic-box').css('left', '-40%');
        $('.emergencyNum').css('left', '-40%');
        if (windowWidth <= 992) {
          $('.maincoridr').css('left', '-65%');
        } else {
          $('.maincoridr').css('left', '-40%');
        }
        $('.onlinetraffic').css('left', '-40%');
        $('.tripTime').css('left', '-60%');

        $('.relatedWebsites').css('left', '20px');
      }
    } else {
      if (windowWidth <= '768') {
        $('.relatedWebsites').css('left', '-95%');
      } else {
        $('.relatedWebsites').css('left', '-30%');
      }
    }
  });
  //end related websites num index

  $('.relatedWebsites a').on('click', function () {
    if ($('.relatedWebsites').css('left') < '0') {

      $('#map-camera-select').val("00");

      if (windowWidth <= 768) {
        $('.provincialcamera').css('left', '-310px');
        $('.traffic-box').css('left', '-310px');
        $('.maincoridr').css('left', '-95%');
        $('.onlinetraffic').css('left', '-95%');
        $('.relatedWebsites').css('left', '5%');
        $('.emergencyNum').css('left', '-95%');
        $('.tripTime').css('left', '-95%');

      } else {
        $('.provincialcamera').css('left', '-40%');
        $('.traffic-box').css('left', '-40%');
        $('.emergencyNum').css('left', '-40%');
        if (windowWidth <= 992) {
          $('.maincoridr').css('left', '-65%');
        } else {
          $('.maincoridr').css('left', '-40%');
        }
        $('.onlinetraffic').css('left', '-40%');
        $('.tripTime').css('left', '-60%');

        $('.relatedWebsites').css('left', '20px');
      }
    } else {
      if (windowWidth <= '768') {
        $('.relatedWebsites').css('left', '-95%');
      } else {
        $('.relatedWebsites').css('left', '-30%');
      }
    }
  });
  //end related websites num index

  $("#map-traffic-select").on('change', function () {
    let mapSelectedId = $(this).val();
    $(".traffic-box > .form-map > svg #iran .map-selected").removeClass('map-selected');
    $(".traffic-box > .form-map > svg #iran #IR-" + mapSelectedId).addClass('map-selected');
    let provinceSelectedId = $(this).children("option:selected").attr('provinceId');
    showTrafficPhotoBox(provinceSelectedId);
  });

  $("#map-camera-select").on('change', function () {
    let mapSelectedId = $(this).val();
    $(".provincialcamera > .form-map > svg #iran .map-selected").removeClass('map-selected');
    $(".provincialcamera > .form-map > svg #iran #IR-" + mapSelectedId).addClass('map-selected');
    let provinceSelectedId = $(this).children("option:selected").attr('provinceId');
    let provinceSelectedLat = $('#map-camera-select').children("option:selected").attr('lat');
    let provinceSelectedLong = $('#map-camera-select').children("option:selected").attr('long');
    showCameraSlider(0, '/camera', provinceSelectedId, provinceSelectedLat, provinceSelectedLong);
  });

  $("#map-online-select").on('change', function () {
    let mapSelectedId = $(this).val();
    $(".provincialcamera > .form-map > svg #iran .map-selected").removeClass('map-selected');
    $(".provincialcamera > .form-map > svg #iran #IR-" + mapSelectedId).addClass('map-selected');
    let provinceSelectedId = $(this).children("option:selected").attr('provinceId');
    let provinceSelectedLat = $('#map-online-select').children("option:selected").attr('lat');
    let provinceSelectedLong = $('#map-online-select').children("option:selected").attr('long');
    showTrafficLayer('/getonlinemapbyprovince', provinceSelectedId, provinceSelectedLat, provinceSelectedLong);
  });

  $("#provincialCameraModal .close").on('click', function () {
    $(this).parent().parent().css('display', 'none');
    $("#slideshow-slide").find('.mySlides').remove();
    $("#slideshow-dot").find('.dot').remove();
  });

  $("#trafficPhotoModal .close").on('click', function () {
    $(this).parent().parent().css('display', 'none');
    $(".traffic-box > .form-map > svg #iran .map-selected").removeClass('map-selected');
    $('#map-traffic-select').val("00");
  });

  $('#provincialCameraModal #slideshow-slide .prev').on('click', function () {
    cameraShowSlides(cameraSlideIndex -= 1);
  });

  $('#provincialCameraModal #slideshow-slide .next').on('click', function () {
    cameraShowSlides(cameraSlideIndex += 1);
  });

  $('#provincialCameraModal > .camera-overlay').on('click', function () {
    $(this).parent().css('display', 'none');
    $("#slideshow-slide").find('.mySlides').remove();
    $("#slideshow-dot").find('.dot').remove();
  });

  $('#trafficPhotoModal > .traffic-overlay').on('click', function () {
    $(this).parent().css('display', 'none');
  });

  function cameraShowSlides(n) {
    let slides = $("#provincialCameraModal #slideshow-slide .mySlides");
    let dots = $("#provincialCameraModal #slideshow-dot .dot");
    if (n > slides.length) { cameraSlideIndex = 1 }
    if (n < 1) { cameraSlideIndex = slides.length }
    $(slides).each(function () {
      $(this).css('display', 'none')
    });
    $(dots).each(function () {
      $(this).removeClass('active')
    });
    $(slides[cameraSlideIndex - 1]).css('display', 'block');
    $(dots[cameraSlideIndex - 1]).addClass('active');
  }

  // Tehran - Ghaem-shahr -> Firoozkooh
  $("#firoozkooh").on('click', function () {
    isCoridor = true;
    map.flyTo([35.9936, 52.2647], 9);
    routing(35.7006, 51.4018, 36.4684, 52.8634);
  });

  // Tehran - Amol -> Haraaz
  $("#haraz").on('click', function () {
    isCoridor = true;
    map.flyTo([36.0269, 51.8994], 9);
    routing(35.7006, 51.4018, 36.47137, 52.34927);
  });

  // Tehran - Karaj -> Chaloos
  $("#chaloos").on('click', function () {
    isCoridor = true;
    map.flyTo([36.1223, 51.5176], 9);
    routing(35.6995, 51.3151, 36.6459, 51.4070);
  });

  // Tehran -> Mashhad
  $("#tehran-mashhad").on('click', function () {
    isCoridor = true;
    map.flyTo([35.933, 56.940], 7);
    routing(35.7006, 51.4018, 36.3002, 59.6070);
  });

  // Tehran -> Tabriz
  $("#tehran-tabriz").on('click', function () {
    isCoridor = true;
    map.flyTo([36.714, 49.052], 7);
    routing(35.7006, 51.4018, 38.0962, 46.2738);
  });

  // Tehran -> Ghazvin
  $("#tehran-ghazvin").on('click', function () {
    isCoridor = true;
    map.flyTo([35.9084, 50.8947], 9);
    routing(35.7006, 51.4018, 36.2666, 50.0070);
  });

  // Ghazvin -> Rasht
  $("#ghazvin-rasht").on('click', function () {
    isCoridor = true;
    map.flyTo([36.7727, 49.6994], 8);
    routing(36.2644, 50.0046, 37.2774, 49.5818);
  });

  // Tehran -> Bandarabbas
  $("#tehran-bandarabas").on('click', function () {
    isCoridor = true;
    map.flyTo([31.427, 54.470], 6);
    routing(35.7006, 51.4018, 27.1773, 56.2794);
  });

  // Tehran -> Esfahaan
  $("#tehran-esfahan").on('click', function () {
    isCoridor = true;
    map.flyTo([34.479, 51.647], 7);
    routing(35.7006, 51.4018, 32.67465, 51.67316);
  });

  // Esfahaan -> Shiraaz
  $("#esfahan-shiraz").on('click', function () {
    isCoridor = true;
    map.flyTo([31.044, 52.229], 7);
    routing(32.6707877, 51.6650002, 29.6060218, 52.5378041);
  });

  // Calculate routing + geometry parameter and show path on map
  function routing(latfirst, lngfirst, latsecond, lngsecond) {
    if ($('.maincoridr').css('left') < '0') {
      $('.maincoridr').css('left', '20px');
    } else {
      $('.maincoridr').css('left', '-95%');
    }

    if (routeControls.length) {
      $(routeControls).each(function (index, control) {
        map.removeControl(control);
        routeControls.splice(routeControls.indexOf(index), 1);
      });
    }
    routeArray = [];
    getRouteArray(latfirst, lngfirst, latsecond, lngsecond);

    routeControls.push(L.Routing.control({
      routeWhileDragging: true,
      reverseWaypoints: true,
      addWaypoints: false,
      waypoints: [
        L.latLng(latfirst, lngfirst),
        L.latLng(latsecond, lngsecond)
      ],
      lineOptions: {
        styles: [{
          color: '#5789f2',
          opacity: 1,
          weight: 5
        }]
      },
      createMarker: function (i, wp) {
        if (i == 0) {
          return L.marker(wp.latLng, { icon: sourceIcon });
        } else {
          return L.marker(wp.latLng, { icon: destinationIcon });
        }
      }
    }).addTo(map));

    if ($('.leaflet-routing-container').length) {
      $('.leaflet-routing-container').remove();
    }
    if (markerLayers != null) {
      layerFlags = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
      map.removeLayer(markerLayers);
      markerLayers = null;
      markerList = [];
    }
  }

  function getRouteArray(latfirst, lngfirst, latsecond, lngsecond) {
    $.get(MAP_ENDPOINT + '/route/v1/driving/' + lngfirst + ',' + latfirst + ';' + lngsecond + ',' + latsecond + '?overview=full&alternatives=true&steps=true', function (data) {
      // console.log(decode(data.routes[0].geometry, 6));
      routeArray = decode(data.routes[0].geometry, 6);
    });
  }

  if (windowWidth <= 768) {
    $('.leaflet-routing-collapse-btn').click();
  }

  if (windowWidth > 768) {
    fisheyeMenu({
      growRatio: 1.7,
      verticalLimit: 10,
      horizontalLimit: 1.8
    });
  }
});

// Decode Geometry Function
function decode(str, precision) {
  var index = 0,
    lat = 0,
    lng = 0,
    coordinates = [],
    shift = 0,
    result = 0,
    byte = null,
    latitude_change,
    longitude_change,
    factor = Math.pow(10, Number.isInteger(precision) ? precision : 5);

  // Coordinates have variable length when encoded, so just keep
  // track of whether we've hit the end of the string. In each
  // loop iteration, a single coordinate is decoded.
  while (index < str.length) {

    // Reset shift, result, and byte
    byte = null;
    shift = 0;
    result = 0;

    do {
      byte = str.charCodeAt(index++) - 63;
      result |= (byte & 0x1f) << shift;
      shift += 5;
    } while (byte >= 0x20);

    latitude_change = ((result & 1) ? ~(result >> 1) : (result >> 1));

    shift = result = 0;

    do {
      byte = str.charCodeAt(index++) - 63;
      result |= (byte & 0x1f) << shift;
      shift += 5;
    } while (byte >= 0x20);

    longitude_change = ((result & 1) ? ~(result >> 1) : (result >> 1));

    lat += latitude_change;
    lng += longitude_change;

    coordinates.push([lat / factor, lng / factor]);
  }

  return coordinates;
};

function returnDeviceType() {
  var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  // console.log('iOS', iOS);
  // console.log('windowWidth', windowWidth);
  if (windowWidth >= 1200) {
    // console.log('9');
    return 9;
  } else if (windowWidth >= 768) {
    if (iOS) {
      // console.log('8');
      return 8;
    } else {
      // console.log('4');
      return 4;
    }
  } else {
    if (iOS) {
      // console.log('7');
      return 7;
    } else {
      // console.log('3');
      return 3;
    }
  }
}

// Mosafer-yaar

function clearMosaferyar() {
  // Clear all input fields
  $('#first_part').val('');
  $('#second_part').val('');
  $('#state_code').val('');
  $('#serial_num').val('');
  $('#phone-num').val('');
  $('#captcha').val('');
  $('#verification-code').val('');

  // Return to first page of mosaferyar
  $('.modal-body .phone-container').hide();
  $('.modal-body .plate-container').hide();
  $('.modal-footer .next-button').text('مرحله بعد');
  $('.footer-buttons').hide();
  $('.modal-body .passenger-items').show();
}

$('#first_part').keyup(function () {
  if (this.value.length == this.maxLength) {
    $(this).next('#second_part').focus();
  }
});

$('#second_part').keyup(function () {
  if (this.value.length == this.maxLength) {
    $(this).next('#state_code').focus();
  }
});

$('#state_code').keyup(function () {
  if (this.value.length == this.maxLength) {
    $('#serial_num').focus();
  }
});

$('#phone-num').keyup(function () {
  if (this.value.length == this.maxLength) {
    $('#captcha').focus();
  }
});

function showBusTag() {
  $('.modal-body .passenger-items').hide('slow');
  $('.modal-body .plate-container').show('slow');
  $('.footer-buttons').show('slow');
}

$('.serial-number-label').mouseenter(function () {
  $('.help-text-container').show();
}).mouseleave(function () {
  $('.help-text-container').hide();
});

$('.footer-buttons .back-button').on('click', function () {
  if ($('.modal-body .plate-container').css('display') == 'block') {
    // We are in step 2, so we'll go to step 1
    $('.modal-body .plate-container').hide('slow');
    $('.footer-buttons').hide('slow');
    $('.modal-body .passenger-items').show('slow');
  } else {
    // We are in step 3 and we'll go to step 2
    $('.modal-body .phone-container').hide('slow');
    $('.modal-body .plate-container').show('slow');
    $('.modal-footer .next-button').text('مرحله بعد');
  }
});

$('.footer-buttons .next-button').on('click', function () {
  if ($('.modal-footer .next-button').text() == 'مرحله بعد') {
    // Validate inputs, then go to step 3
    if (!validatePlateInputs(1)) {
      $('.plate-error-alert').show('slow');
    } else if (!validatePlateInputs(2)) {
      $('.plate-error-alert').hide('slow');
      $('.serial-error-alert').show('slow');
    } else {
      $('.serial-error-alert').hide('slow');
      $('.modal-body .plate-container').hide('slow');
      $('.modal-body .phone-container').show('slow');
      $('.modal-footer .next-button').text('مکان یابی');
    }
    // markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });
    // let busMarker = L.marker(L.latLng(35.4, 51.7), {
    //   icon: L.icon({
    //     iconUrl: 'assets/new_template/images/markers/bus.png'
    //   }),
    // }).bindPopup(getPopupContent(13, {type: 'اسکانیا VIP', company: 'ایران گشت', origin: 'تهران', destination: 'بندر عباس', updated_at: '14-11-98, 15:55'}), { 'className': 'customWeather' }).openPopup();
    // markerList.push(busMarker);
    // markerLayers.addLayers(markerList);
    // map.addLayer(markerLayers);
    // map.flyTo([35.4, 51.7], 14);
  } else {
    // Req. to server, then show on map
    let mobile_number = $('.phone-number-input').val();

    if (mobile_number.length < 11) {
      $('.phone-number-error-alert').show('slow');
    } else if (!validatePlateInputs(3)) {
      $('.phone-number-error-alert').hide('slow');
      $('.captcha-error-alert').show('slow');
    } else if (!validatePlateInputs(4)) {
      $('.captcha-error-alert').hide('slow');
      $('.verify-code-error-alert').show('slow');
    } else {
      $('.verify-code-error-alert').hide('slow');
      $.ajax({
        url: 'https://141.ir/api/buspositionweb',
        type: 'POST',
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        data: {
          'pelak_number': pelak_number,
          'invoice_number': invoice_number,
          'verify_code': verify_code,
          'captcha': captcha_code,
          'mobile': mobile_number
        },
        statusCode: {
          419: function () {
            $('.phone-number-error-alert').hide('slow');
            $('.captcha-error-alert').show('slow');
          }
        },
      }).done(function (result) {
        // console.log(result);
        if (result.status == -1) {
          $('.server-error-alert').text('شماره پلاک اشتباه است.').show('slow');
        } else if (result.status == -2) {
          $('.server-error-alert').text('شماره سریال صورت وضعیت با شماره ثبت شده مطابقت ندارد.').show('slow');
        } else if (result.status == -3) {
          $('.server-error-alert').text('لطفا پس از دو دقیقه دوباره تلاش کنید.').show('slow');
        } else if (result.status == -4) {
          $('.server-error-alert').text('کد تایید وارد شده نادرست است.').show('slow');
        } else {
          $('.modal').modal('hide');
          if (markerLayers != null) {
            map.removeLayer(markerLayers);
            markerLayers = null;
            markerList = [];
          }
          markerLayers = L.markerClusterGroup({ disableClusteringAtZoom: MAP_MIN_ZOOM });
          let busMarker = L.marker(L.latLng(result.lat, result.lng), {
            icon: L.icon({
              iconUrl: 'assets/new_template/images/markers/bus.png'
            }),
          }).bindPopup(getPopupContent(13, result), { 'className': 'customWeather' }).openPopup();
          markerList.push(busMarker);
          markerLayers.addLayers(markerList);
          map.addLayer(markerLayers);
          map.flyTo([result.lat, result.lng], 14);
        }
      });
    }
  }
});

$('.phone-number-button').on('click', function () {
  let mobile_number = $('.phone-number-input').val();

  if (mobile_number.length < 11 || mobile_number.substring(0, 2) != '09') {
    $('.phone-number-error-alert').show('slow');
  } else {
    $('.phone-number-error-alert').hide('slow');
    $.post('https://141.ir/api/buspositionmobilecode', {
      mobile: mobile_number
    }, function (result) {
      // console.log(result);
      if (result.status == 0) {
        mobile = mobile_number;
        $('.sms-code-container').show('slow');
      }
    });
  }
});

function plateInput(_this) {
  _this.setCustomValidity('');
  $('.plate-error-alert').hide('slow');
}

function validatePlateInputs(step) {
  let first_part = $('#first_part').val();
  let second_part = $('#second_part').val();
  let state_code = $('#state_code').val();
  let serial_number = $('#serial_num').val();
  let verifyCode = $('.sms-code-input').val();
  let captcha = $('#captcha').val();
  // console.log(first_part.length);
  // console.log(second_part.length);
  // console.log(state_code.length);
  // console.log(serial_number.length);

  switch (step) {
    case 1: {
      // Validate plate number
      if ((first_part.length == 2) && (second_part.length == 3) && (state_code.length == 2)) {
        pelak_number = first_part + '-' + second_part + '-' + state_code;
        return true;
      }
      return false;
    }
    case 2: {
      // Validate serial number
      if (serial_number.length > 4) {
        invoice_number = serial_number;
        return true;
      }
      return false;
    }
    case 3: {
      // Validate captcha
      if (captcha.length < 4)
        return false;
      else {
        captcha_code = captcha;
        return true;
      }
    }
    case 4: {
      // Validate verify code number
      if (verifyCode.length < 6)
        return false;
      else {
        verify_code = verifyCode;
        return true;
      }
    }
  }
}

// End of mosafer-yaar

// Cookie-related functions

function setCookie(cname, cvalue, exdays) {
  var d = new Date();
  d.setTime(d.getTime() + (exdays * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toGMTString();
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function getCookie(cname) {
  var name = cname + "=";
  var decodedCookie = decodeURIComponent(document.cookie);
  var ca = decodedCookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
  }
  return "";
}

function isFirstSeen() {
  var firstSeen = getCookie("firstSeen");
  if (firstSeen != "") {
    // Do nothing!
    return false;
  } else {
    setCookie("firstSeen", firstSeen, 10 * 365);
    return true;
  }
}
