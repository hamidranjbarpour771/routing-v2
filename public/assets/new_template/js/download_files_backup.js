var year = "";
const trafficFilesSpinner = document.getElementById("traffic-files-spinner");
const tripTimesSpinner = document.getElementById("trip-times-spinner");

const windowWidth = $(window).width();

function returnDeviceType() {
  var iOS = /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;
  // console.log('iOS', iOS);
  // console.log('windowWidth', windowWidth);
  if (windowWidth >= 1200) {
    // console.log('9');
    return 9;
  } else if (windowWidth >= 768) {
    if (iOS) {
      // console.log('8');
      return 8;
    } else {
      // console.log('4');
      return 4;
    }
  } else {
    if (iOS) {
      // console.log('7');
      return 7;
    } else {
      // console.log('3');
      return 3;
    }
  }
}

function showContent(index) {
  var filesYears = [
    "1398",
    "1397",
    "1396",
    "1395",
    "1394",
    "1393",
    "1392",
    "1391",
    "1390",
    "1389"
  ];

  if (index == 1) {
    // Trip time files page only have 3 years
    filesYears = ["1397", "1396", "1395"];
  }

  for (i = 0; i < filesYears.length; i++) {
    // console.log(filesYears[i]);
    $("#yearListBtn1").append(
      '<button class="btn" id="yearBtn' +
        i +
        '"' +
        ' onclick="showLeftSide(' +
        i +
        ', this)">\
        <li><img src="../assets/new_template/images/icons/foldericon-orange.svg" alt=""> <span>سال ' +
        filesYears[i] +
        " </span>\
        </li>\
    </button>"
    );
  }
}

function showLeftSide(yearIndex, element) {
  $(element)
    .find("li > img")
    .attr("src", "../assets/new_template/images/icons/foldricon-white.svg");
  $("#triptimeLeftshow").css("display", "inline-block");
  $("#triptimeLeftshow").show();
  var activeElements = document.querySelectorAll(".activeButton");
  if (activeElements.length > 0) {
    for (let index = 0; index < activeElements.length; index++) {
      //console.log(activeElements[index]);
      $(".activeButton")
        .find("img")
        .attr(
          "src",
          "../assets/new_template/images/icons/foldericon-orange.svg"
        );
      activeElements[index].classList.remove("activeButton");
    }
  }

  var yearElement = document.getElementById("yearBtn" + yearIndex);
  yearElement.classList.add("activeButton");
  // console.log(yearElement);
  var slicedYear = yearElement.innerText.slice(7);
  year = slicedYear;
  // console.log(year);

  var fileElements = document.querySelectorAll(".responsive");
  var textElements = document.querySelectorAll(".responsiveText");
  if (fileElements.length > 0) {
    $(fileElements).remove();
  }
  if (textElements.length > 0) {
    $(textElements).remove();
  }
}

function getData(month, type) {
  var newMonth;
  if (month < 10) newMonth = "0" + month;
  else newMonth = month;
  var activeBtnElements = document.querySelectorAll(".monthButton");
  if (activeBtnElements.length > 0) {
    for (let index = 0; index < activeBtnElements.length; index++) {
      // console.log(activeBtnElements[index]);
      activeBtnElements[index].classList.remove("activeButton");
    }
  }
  for (let index = 0; index < activeBtnElements.length; index++) {
    activeBtnElements[month - 1].classList.add("activeButton");
  }

  // Remove previous files
  var fileElements = document.querySelectorAll(".responsive");
  var textElements = document.querySelectorAll(".responsiveText");
  if (fileElements != null) $(fileElements).remove();
  if (textElements != null) $(textElements).remove();

  // Show spinner
  if (type == 0) {
    tripTimesSpinner.removeAttribute("hidden");
  } else if (type == 1) {
    trafficFilesSpinner.removeAttribute("hidden");
  }
  // Request to Server
  $.post(
    "https://141.ir/api/downloadfiles",
    {
      type: type,
      month: newMonth,
      year: year,
      device_type: returnDeviceType()
    },
    function(result) {
      //console.log(result);
      // Remove spinner
      if (type == 0) {
        tripTimesSpinner.setAttribute("hidden", "");
      } else if (type == 1) {
        trafficFilesSpinner.setAttribute("hidden", "");
      }
      if (result.files.length > 0) {
        result.files.forEach(element => {
          $("#files").append(
            '<div class="responsive">\
                    <div class="gallery">\
                        <a target="_blank" href="' +
              element +
              '">\
                            <img src="../assets/new_template/images/icons/' +
              returnIcon(element.slice(element.lastIndexOf(".") + 1)) +
              '">\
                            <div class="desc">' +
              element.slice(36) +
              "</div>\
                        </a>\
                    </div>\
                </div>"
          );
        });
      } else {
        $("#files").append(
          '<div class="responsiveText">\
                <span>هیچ فایلی برای دانلود وجود ندارد.</span>\
                </div>'
        );
      }
    }
  );
}

function returnIcon(icon) {
  //console.log(icon);
  switch (icon) {
    case "xls":
    case "xlsx":
      return "xlsx.png";
    case "csv":
      return "csv.png";
    case "rar":
      return "rar.png";
    case "pdf":
      return "pdf.svg";
    default:
      return "rar.png";
  }
}

// img src change on hover ...

$(document).ready(function() {
  $("#yearListBtn1 button").mouseover(function() {
    $(this)
      .find("li img")
      .attr("src", "../assets/new_template/images/icons/foldricon-white.svg");
  });
  $("#yearListBtn1 button").mouseleave(function() {
    $(this)
      .find("li img")
      .attr("src", "../assets/new_template/images/icons/foldericon-orange.svg");
  });
  $("#triptimeLeftshow ul button").mouseover(function() {
    $(this)
      .find("li img")
      .attr("src", "../assets/new_template/images/icons/foldricon-white.svg");
  });
  $("#triptimeLeftshow ul button").mouseleave(function() {
    $(this)
      .find("li img")
      .attr("src", "../assets/new_template/images/icons/foldericon-orange.svg");
  });

  // Show/hide corona box for mobile
  if (windowWidth < 480) {
    if ($(".files-box").length) {
      $(".files-box").hide();
      $(".files-box-mobile").show();
    }
  }
});
