var mymap = L.map("mapid").setView([35.7096552, 51.4049619], 17);

L.tileLayer("https://testmap.141.ir/141map/{z}/{x}/{y}.png", {
  attribution: "",
}).addTo(mymap);

L.marker([35.7093552, 51.4049619])
  .addTo(mymap)
  .bindPopup("<b>مركز مديريت راههاي كشور</b>")
  .openPopup();
var popup = L.popup();

$("#MsgFile").on("change", function () {
  $(".uploadBox-content").text($("#MsgFile")[0].files[0].name);
  $(".uploadBox-content").show();
});

$(document).ready(function () {
  if ($(".send-message").css("display") != "none") {
    // $("#send-message-title").css({ color: "#ff5630" });
    $("#top-button1").css({ "border-bottom": "2px solid #ff5630" });
  } else {
    // $("#check-message-title").css({ color: "#ff5630" });
    $("#top-button2").css({ "border-bottom": "2px solid #ff5630" });
  }
});

$("#send-message-title").on("click", function () {
  // $("#send-message-title").css({ color: "#ff5630" });
  $("#top-button1").css({ "border-bottom": "2px solid #ff5630" });
  $("#top-button2").css({ "border-bottom": "none" });
  // $("#check-message-title").css({ color: "#000000" });

  $(".check-message").hide("slow");
  $(".send-message").show("slow");
});

$("#check-message-title").on("click", function () {
  // $("#check-message-title").css({ color: "#ff5630" });
  $("#top-button2").css({ "border-bottom": "2px solid #ff5630" });
  $("#top-button1").css({ "border-bottom": "none" });
  // $("#send-message-title").css({ color: "#000000" });

  $(".send-message").hide("slow");
  $(".check-message").show("slow");
});

function sendMessage() {
  let name = $("#fName").val();
  let mobile = $("#yEmail").val();
  let title = $("#MsgSubject").val();
  let body = $("#yText").val();
  let captcha = $("#captcha1").val();
  let postdata = {
    name: name,
    mobile: mobile,
    title: title,
    body: body,
    file:
      $("#MsgFile")[0].files.length > 0 ? $("#MsgFile")[0].files[0] : "false",
    captcha: captcha,
  };
  if (contactUsCheckInputFormValue(postdata)) {
    formData = new FormData();

    formData.append("name", postdata.name);
    formData.append("mobile", postdata.mobile);
    formData.append("title", postdata.title);
    formData.append("body", postdata.body);
    formData.append("file", postdata.file);
    formData.append("captcha", postdata.captcha);

    $.ajax({
      url: "/api/contactus",
      type: "POST",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      success: function (result) {
        $("#captcha1").removeClass("invalid-input");
        // alert("پیام شما باموفقیت ارسال شد!");
        Swal.fire("موفقیت آمیز", "پیام شما با موفقیت ارسال شد", "success");
        //console.log(result)
        setTimeout(function () {
          window.open("/contactus", "_self");
        }, 2000);
      },
      error: function (error) {
        if (
          error.responseJSON.errors != undefined &&
          error.responseJSON.errors.captcha != undefined
        ) {
          // alert("لطفا کد امنیتی را به درستی وارد کنید!");
          Swal.fire("خطا", "لطفا کد امنیتی را به درستی وارد کنید!", "error");
          $("#captcha2").addClass("invalid-input");
        } else {
          console.log(error);
          // alert("خطا در دریافت اطلاعات!");
          Swal.fire("خطا", "خطا در دریافت اطلاعات!", "error");
        }
        $(".captcha1").find("img").click();
      },
    });
  }
}

function contactUsCheckInputFormValue(data) {
  let validation = true;

  if (data.name == "") {
    validation = false;

    $("#fName").addClass("invalid-input");
  } else {
    $("#fName").removeClass("invalid-input");
  }

  if (data.title == "") {
    validation = false;

    $("#MsgSubject").addClass("invalid-input");
  } else {
    $("#MsgSubject").removeClass("invalid-input");
  }

  if (data.body == "") {
    validation = false;

    $("#yText").addClass("invalid-input");
  } else {
    $("#yText").removeClass("invalid-input");
  }

  if (data.captcha == "") {
    validation = false;

    $("#captcha1").addClass("invalid-input");
  } else {
    $("#captcha1").removeClass("invalid-input");
  }

  if (data.mobile == "" || !/^09\d{9}$/.test(data.mobile)) {
    validation = false;

    $("#yEmail").addClass("invalid-input");
  } else {
    $("#yEmail").removeClass("invalid-input");
  }

  return validation;
}

function checkMessage() {
  let code = $("#yCode").val();
  let phone = $("#yPhone").val();
  let captcha = $("#captcha2").val();

  let postData = {
    code: code,
    phone: phone,
    captcha: captcha,
  };

  if (checkFormValues(postData)) {
    formData = new FormData();

    formData.append("id", postData.code);
    formData.append("mobile", postData.phone);
    formData.append("captcha", postData.captcha);

    $.ajax({
      url: "/api/contactusresponse",
      type: "POST",
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      headers: {
        "X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content"),
      },
      success: function (result) {
        console.log(result);
        $("#captcha2").removeClass("invalid-input");
        if (result.status_code == 1) {
          // Barrasi Nashode
          Swal.fire(
            "نتیجه درخواست",
            ".درخواست شما هنوز توسط کارشناسان مرکز بررسی نشده است",
            "error"
          );
        } else if (result.status_code == 2) {
          // Dar Dast Barrasi
          Swal.fire(
            "نتیجه درخواست",
            ".درخواست شما توسط کارشناسان مرکز در حال پیگیری است",
            "warning"
          );
        }
        Swal.fire(
          "نتیجه درخواست",
          "درخواست شما توسط کارشناسان مرکز پیگیری شده و نتیجه از طریق پیامک به شماره تلفن شما ارسال شده است",
          "success"
        );
        $(".captcha2").find("img").click();
      },
      error: function (error) {
        if (
          error.responseJSON.errors != undefined &&
          error.responseJSON.errors.captcha != undefined
        ) {
          // alert("لطفا کد امنیتی را به درستی وارد کنید!");
          Swal.fire("خطا", "لطفا کد امنیتی را به درستی وارد کنید!", "error");
          $("#captcha2").addClass("invalid-input");
        } else {
          console.log(error);
          // alert("خطا در دریافت اطلاعات!");
          Swal.fire("خطا", "خطا در دریافت اطلاعات!", "error");
        }
        $(".captcha2").find("img").click();
      },
    });
  }
}

function checkFormValues(data) {
  let validation = true;

  if (data.code == "") {
    validation = false;
    $("#yCode").addClass("invalid-input");
  } else {
    $("#yCode").removeClass("invalid-input");
  }

  if (data.phone == "") {
    validation = false;
    $("#yPhone").addClass("invalid-input");
  } else {
    $("#yPhone").removeClass("invalid-input");
  }

  if (data.captcha == "") {
    validation = false;
    $("#captcha2").addClass("invalid-input");
  } else {
    $("#captcha2").removeClass("invalid-input");
  }

  return validation;
}
