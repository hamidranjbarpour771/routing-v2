<!DOCTYPE html>
<html lang="en">

<head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-CFKH5K032J"></script>
    <meta charset="UTF-8" />
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <meta content="width=device-width, initial-scale=1.0" name="viewport" />
    <meta content="ie=edge" http-equiv="X-UA-Compatible" />
    <meta name="robots" content="noindex, nofollow"/>
    <meta name="description"
        content="وب سایت رسمی مرکز مدیریت راه های کشور با قابلیت نمایش تصاویر دوربین های نظارتی سطح کشور، نمایش میزان تردد جاده ای، وضعیت آب و هوای جاده های کشور" />
    <meta name="keywords"
        content="141، سازمان راهداری، سازمان راهداری و حمل و نقل جاده ای، حمل و نقل، جاده، مرکز مدیریت راه های کشور، مرکز مدیریت راه ها، مسیریابی، کریدور" />
    <link rel="stylesheet" href="assets/new_template/css/animate.min.css" />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png?v=2" />
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png?v=2" />
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png?v=2" />
    <link rel="manifest" href="/site.webmanifest?v=2" />
    <link rel="mask-icon" href="/safari-pinned-tab.svg?v=2" color="#5bbad5" />
    <link rel="shortcut icon" href="/favicon.ico?v=2" />
    <meta name="apple-mobile-web-app-title" content="141" />
    <meta name="application-name" content="141" />
    <meta name="msapplication-TileColor" content="#00aba9" />
    <meta name="theme-color" content="#ffffff" />

    <meta name="google-site-verification" content="ZYrlepZj-VF6FYhb5koNyXIneH1JgwYWaZW17R0rBLs" />

    <title>مرکز مدیریت راه های کشور </title>

    <!-- STYLESHEET -->
    <link href="assets/new_template/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/new_template/css/leaflet.css" rel="stylesheet" />
    <link href="assets/new_template/css/MarkerCluster.css" rel="stylesheet" />
    <link href="assets/new_template/css/MarkerCluster.Default.css" rel="stylesheet" />
    <link href="assets/new_template/css/leafletReset.css" rel="stylesheet" />
    <link href="assets/new_template/css/index.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/new_template/css/font-awesome.min.css" />
    <style>
        .beacon {
            position: absolute;
            top: 50%;
            left: 38%;
            background-color: #007bff;
            height: 0.4em;
            width: 0.4em;
            border-radius: 50%;
            z-index: 1010;

        }

        .beacon-blue {
            background-color: #007bff;


        }

        .beacon-blue :before {
            position: absolute;
            content: "";
            height: 1em;
            width: 1em;
            left: 0;
            top: 0;
            background-color: transparent;
            border-radius: 50%;
            box-shadow: 0px 0px 2px 2px #2980b9;
            -webkit-animation: active 2s infinite linear;
            animation: active 2s infinite linear;
        }

        .beacon-green {
            background-color: #28a745;

        }

        .beacon-green:before {
            position: absolute;
            content: "";
            height: 0.3rem;
            width: 0.3rem;
            left: 0;
            top: 0;
            background-color: transparent;
            border-radius: 50%;
            box-shadow: 0px 0px 2px 2px #28a745;
            -webkit-animation: active 2s infinite linear;
            animation: active 2s infinite linear;
        }

        .beacon-yellow {
            background-color: #ffc107;


        }

        .beacon-yellow:before {
            position: absolute;
            content: "";
            height: 1em;
            width: 1em;
            left: 0;
            top: 0;
            background-color: transparent;
            border-radius: 50%;
            box-shadow: 0px 0px 2px 2px #2980b9;
            -webkit-animation: active 2s infinite linear;
            animation: active 2s infinite linear;
        }

        .beacon-red {
            background-color: #dc3545;


        }

        .beacon-red:before {
            position: absolute;
            content: "";
            height: 0.3rem;
            width: 0.3rem;
            left: 0;
            top: 0;
            background-color: transparent;
            border-radius: 50%;
            box-shadow: 0px 0px 2px 2px #dc3545;
            -webkit-animation: active 2s infinite linear;
            animation: active 2s infinite linear;
        }

        .beacon-red-fix {
            background-color: #dc3545;
        }

        .beacon-green-fix {
            background-color: #28a745;
        }

        @-webkit-keyframes active {
            0% {
                -webkit-transform: scale(0.1);
                opacity: 1;
            }

            70% {
                -webkit-transform: scale(3);
                opacity: 0;
            }

            100% {
                opacity: 0;
            }
        }

        @keyframes active {
            0% {
                transform: scale(0.1);
                opacity: 1;
            }

            70% {
                transform: scale(3);
                opacity: 0;
            }

            100% {
                opacity: 0;
            }
        }

        .announcement-box {
            top: 0px !important;
            display: none !important;
            right: 20px;
            opacity: 0.9;
            background-color: #ff5c03;
            /* opacity: 0.7; */
            z-index: 11 !important;
            position: fixed;
            overflow: hidden;
            padding: 15px;
            border-radius: 0 !important;
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
            max-width: 100% !important;
            width: 100% !important;
            max-height: 1% !important;
            transition-duration: 0.5s;
            transition-property: left;
            direction: rtl;
            /* border-bottom: 2px solid black;
      border-top: 2px solid black; */
            left: 50% !important;
            transform: translateX(-50%) !important;
        }

        .announcement-box p {
            position: absolute;
            width: 100%;
            height: 100%;
            top: -10px;
            margin: 0;
            line-height: 50px;
            text-align: center;
            /* Starting position */
            -moz-transform: translateX(-100%);
            -webkit-transform: translateX(-100%);
            transform: translateX(-100%);
            /* Apply animation to this element */
            -moz-animation: scroll-right 25s linear infinite;
            -webkit-animation: scroll-right 25s linear infinite;
            animation: scroll-right 25s linear infinite;
        }

        .menu_top {
            /* top: 30px !important */
        }

        .announcement-box:hover p {
            animation-play-state: paused;
            -webkit-animation-play-state: paused;
            -moz-animation-play-state: paused;
            -o-animation-play-state: paused;
        }

        /* Move it (define the animation) */
        @-moz-keyframes scroll-right {
            0% {
                -moz-transform: translateX(-100%);
            }

            100% {
                -moz-transform: translateX(100%);
            }
        }

        @-webkit-keyframes scroll-right {
            0% {
                -webkit-transform: translateX(-100%);
            }

            100% {
                -webkit-transform: translateX(100%);
            }
        }

        @keyframes scroll-right {
            0% {
                -moz-transform: translateX(-100%);
                /* Browser bug fix */
                -webkit-transform: translateX(-100%);
                /* Browser bug fix */
                transform: translateX(-100%);
            }

            100% {
                -moz-transform: translateX(100%);
                /* Browser bug fix */
                -webkit-transform: translateX(100%);
                /* Browser bug fix */
                transform: translateX(100%);
            }
        }

        .switch-tile-containertraffic {
            position: absolute;
            bottom: 82px;
            left: 70px;
            /* background-color: #ffffff; */
            z-index: 12;
            width: 45px;
            height: 45px;
            border-radius: 0.2rem;
            cursor: pointer;
        }

        .switch-tile-containertrafficonline {
            position: absolute;
            bottom: 82px;
            right: 29px;
            /* background-color: #ffffff; */
            z-index: 12;
            width: 45px;
            height: 45px;
            border-radius: 0.2rem;
            cursor: pointer;
        }

        .routing-box {
            top: 100px;
        }

        .routing-icon {
            top: 100px;
        }

        .provincialcamera {
            top: 100px;
        }

        .onlinetraffic {
            top: 100px;
        }

        .traffic-box {
            top: 100px;
        }

        .maincoridr {
            top: 100px;
        }

        .emergencyNum {
            top: 100px;

        }

        .relatedWebsites {
            top: 100px;
        }

        .helper-box {
            display: none;
            position: absolute;
            bottom: 75px;
            direction: rtl;
            right: 20px;
            opacity: 0.9;
            z-index: 10;
            background-color: #ffffff;
            white-space: nowrap;
            padding: 15px;
            border-radius: 7px;
            max-width: 296px;
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
            max-height: 200px;
            transition-duration: 0.5s;
            transition-property: left;
        }

        .showTrafficPic {
            background-image: url(../assets/new_template/images/tarficmap.png);
            width: 78px;
            height: 7px;
            background-size: 78px 7px;
            display: inline-block;
            margin-left: 4px;
            margin-right: 2px;
        }

        .textDiv {
            flex: 1 0 auto;
            padding-bottom:5px;
            line-height: 16px;
            vertical-align: top;
            font-size: 14px;
            font-weight: 600;
        }

        .TrafficDiv {
            flex: 1 0 auto;
            padding: 8px 12px;
            line-height: 16px;
            vertical-align: top;
            font-size: 14px;
            color: #8c8c8c;
            border-left: 1px solid #ececec;
            text-align: center;
            font-style: italic;
        }

        .mapLegend {
            display: none;
            bottom: 113px;
            right: 138px;
            opacity: 0.9;
            background-color: #ffffff;
            z-index: 10;
            position: absolute;
            padding: 15px;
            border-radius: 7px;
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
            max-width: 230px;
            max-height: 50%;
            transition-duration: 0.5s;
            transition-property: left;
            direction: rtl;
        }

        .route-share {
            width: 33px;
            border: 1px solid #ccc;
            border-radius: 4px;
            background-color: white;
            float: right;
            cursor: pointer;
            color: #ff5630;
            font-size: 17px;
            padding: 2px 9px !important;
            margin-right: 1rem;
        }

        .route-detail-boxx {
            overflow-y: unset !important;

        }

        .route-detail-containerprint {
            position: absolute;
            top: 1579px;
            right: 20px;
            opacity: 0.9;
            background-color: #ffffff;
            z-index: 12;
            /* padding: 15px; */
            border-bottom-left-radius: 7px;
            border-bottom-right-radius: 7px;
            box-shadow: 0 2px 4px 0 rgba(0, 0, 0, 0.1);
            width: 275px;
            max-height: 100%;
            transition-duration: 0.5s;
            transition-property: left;
            direction: rtl;
        }

        .dropdown-141:hover .dropdown-content {
            display: flex;
            flex-direction: column;
            top: 25px;
            border-radius: 5px;
        }

        .routing-custom-box {
            top: 100px;
        }

        .imagekoll {
            width: 44px;
        }

        @media only screen and (max-width: 576px) {
            .switch-tile-containertraffic {
                position: absolute;
                bottom: 80px;
                left: 67px;
                z-index: 12;
                width: 45px;
                height: 45px;
                border-radius: 0.2rem;
            }
        }

        .ResponsiveVerticalMenu {
            top: 100px;
        }

        @media only screen and (max-width: 768px) {
            .parent_menu_vertical {
                top: 93px;
            }
        }

        .shekayatprocess {
            color: red;
            padding: 2px
        }

        .shekayatprocess:hover {
            color: #afafaf
        }

        .newsContentLeft {
            margin-bottom: 100px !important;
        }

        .newsContentRight {
            margin-bottom: 100px !important;
        }

        @media only screen and (max-width: 1200px) {
            .helper-box {
                bottom: 151px;
            }
        }

        #problem-submit input {
            font-size: 13px
        }

        #problem-submit select {
            font-size: 13px
        }

        #problem-submit label {
            font-size: 14px
        }

        #problem-submit .modal-title {
            font-size: 14px
        }
        .autocomplete-items-dest {
            position: absolute;
            border: 1px solid #d4d4d4;
            border-top-color: rgb(212, 212, 212);
            border-top-style: solid;
            border-top-width: 1px;
            border-bottom-color: rgb(212, 212, 212);
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            top: 100%;
            left: 0;
            right: 31px;
            width: 200px;
            max-height: 200px;
            overflow-y: scroll;
            font-size: 12px;
            font-weight: 300;
            line-height: 20px;

        }
        .autocomplete-items {
            position: absolute;
            border: 1px solid #d4d4d4;
            border-top-color: rgb(212, 212, 212);
            border-top-style: solid;
            border-top-width: 1px;
            border-bottom-color: rgb(212, 212, 212);
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            left: 0;
            right: 31px;
            width: 200px;
            max-height: 200px;
            overflow-y: scroll;
            font-size: 12px;
            font-weight: 300;
            line-height: 20px;

        }
        .autocomplete-items-middle {
            position: absolute;
            border: 1px solid #d4d4d4;
            border-top-color: rgb(212, 212, 212);
            border-top-style: solid;
            border-top-width: 1px;
            border-bottom-color: rgb(212, 212, 212);
            border-bottom-style: solid;
            border-bottom-width: 1px;
            border-bottom: none;
            border-top: none;
            z-index: 99;
            top: 64%;
            left: 0;
            right: 31px;
            width: 200px;
            max-height: 200px;
            overflow-y: scroll;
            font-size: 12px;
            font-weight: 300;
            line-height: 20px;
        }
        .closeRouting::after {
        font-size: 20px;
        content: "\00d7";
        color: #000;
        display:none;
        }
        .imageCancel{
            width:25px;
        }
        .remove-middle-point {
        text-align: center;
        cursor: pointer;
        color: #ff5630;
        margin-top: 10px;
        font-weight: bold;
        font-size: 11px;
        }
        button:focus {
  outline: none;
}
    </style>
</head>

<body>
   
    <!-- END TOP MENU -->
    <!--apliaction modal -->
    <div class="modal fade" id="downloadapp">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">دانلود نرم افزار موبایل همراه 141</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        &times;
                    </button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <form>
                        <div class="form-row">
                            <div class="form-group">
                                <label class="textmodal">
                                    نسخه ی موبایل اپلیکیشن مرکز مدیریت راه های کشور با قابلیت
                                    هایی نظیر نمایش تصاویر دوربین های نظارتی در سراسر کشور،
                                    مسیریابی همراه با نمایش مسافت و زمان تقریبی و جزئیات مسیر،
                                    مشاهده اخبار متنی و شنیداری، کروکی ترافیک استان های کشور،
                                    دریافت اطلاعیه های مربوط به مسیرهای پرتردد و ... آماده
                                    دریافت از مارکت های مختلف می باشد.
                                </label>
                            </div>
                        </div>
                        <div class="form-row" id="mobilerow">
                            <div class="form-group col-md-12">
                                <img src="./assets/new_template/images/mobileImage/mobile_1.png"
                                    class="picmobile" />
                                <img src="./assets/new_template/images/mobileImage/mobile_2.png"
                                    class="picmobile" />
                            </div>
                        </div>
                        <a href="/download">
                            <button type="button" class="btn btn-primary float-left">
                                دانلود نرم افزار
                            </button></a>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <!-- Passenger Modal -->
    <div class="modal fade" id="passenger-modal">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <!-- Modal Header -->
                <div class="modal-header">
                    <img src="assets/new_template/images/layer-icons/passenger.svg" />
                    <h4 class="modal-title">مسافریار</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        &times;
                    </button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="passenger-items">
                        <div class="report-item-box" onclick="showBusTag()">
                            <div class="report-title">
                                <img src="assets/new_template/images/layer-icons/bus.svg" width="48px" />
                                <p class="bus-text">مکان یابی برخط اتوبوس</p>
                            </div>
                        </div>
                        <div class="report-item-box" onclick="window.open('https://141.ir/roadproblems');">
                            <div class="report-title">
                                <img src="assets/new_template/images/layer-icons/problem.svg" width="40px" />
                                <p class="problem-text">ارسال مشکلات حمل و نقل عمومی</p>
                            </div>
                        </div>
                    </div>
                    <div class="bus-info" id="bus-form">
                        <div class="plate-container">
                            <div class="car-plate-container">
                                <label>پلاک اتوبوس:</label>
                                <div class="car-plate">
                                    <img src="assets/new_template/images/car-sign.png" />
                                    <input id="first_part" name="first_part" type="tel" maxlength="2" step="1" value=""
                                        inputmode="numeric" tabindex="2" dir="ltr" class="car-plate-input1"
                                        pattern="\d{2}" required
                                        oninvalid="this.setCustomValidity('لطفا یک عدد دو رقمی وارد کنید')"
                                        oninput="plateInput(this)" />
                                    <input id="second_part" name="second_part" type="tel" maxlength="3" step="1"
                                        value="" inputmode="numeric" tabindex="3" dir="ltr" class="car-plate-input2"
                                        pattern="\d{3}" required
                                        oninvalid="this.setCustomValidity('لطفا یک عدد سه رقمی وارد کنید')"
                                        oninput="plateInput(this)" />
                                    <input id="state_code" name="state_code" type="tel" maxlength="2" step="1" value=""
                                        inputmode="numeric" tabindex="4" dir="ltr" class="car-plate-input3"
                                        pattern="\d{2}" required
                                        oninvalid="this.setCustomValidity('لطفا یک عدد دو رقمی وارد کنید')"
                                        oninput="plateInput(this)" />
                                </div>
                                <div class="alert alert-danger plate-error-alert" role="alert">
                                    لطفا پلاک را به درستی وارد کنید
                                </div>
                            </div>
                            {{-- <div class="serial-number-container">
                <label class="serial-number-label">شماره سریال صورت وضعیت:</label>
                <div class="help-text-container">
                  <div class="help-text-top-dialog"></div>
                  <p class="help-text-body">
                    شماره سریال صورت وضعیت یک عدد 5 یا 6 رقمی است که می توانید
                    آن را از شرکت تعاونی مسافربری و یا راننده دریافت کنید.
                  </p>
                </div>
                <div class="serial-number">
                  <input type="tel" maxlength="6" step="1" value="" inputmode="numeric" tabindex="5"
                    class="serial-number-input" id="serial_num" required
                    oninvalid="this.setCustomValidity('لطفا شماره سریال دریافتی را وارد کنید')"
                    oninput="this.setCustomValidity('')" />
                </div>
                <div class="alert alert-danger serial-error-alert" role="alert">
                  لطفا شماره سریال را به درستی وارد کنید
                </div>
              </div> --}}
                        </div>
                        <div class="phone-container">
                            <div class="phone-number-container">
                                <label for="" class="phone-number-label">شماره تلفن همراه خود را وارد کنید:</label>
                                <div class="phone-number-input-row">
                                    <input type="text" value="" maxlength="11" tabindex="1" class="phone-number-input"
                                        id="phone-num" required
                                        oninvalid="this.setCustomValidity('لطفا شماره تلفن همراه را وارد کنید')"
                                        oninput="this.setCustomValidity('')" />
                                </div>
                            </div>
                            <div class="alert alert-danger phone-number-error-alert" role="alert">
                                لطفا شماره موبایل را به درستی وارد کنید
                            </div>
                            <div class="captcha-container">
                                <label for="" class="captcha-label">کد امنیتی زیر را وارد کنید:</label>
                                <div class="captcha-input-row">
                                    <input type="text" value="" maxlength="6" tabindex="2" class="captcha-input"
                                        id="captcha" name="captcha" required
                                        oninvalid="this.setCustomValidity('لطفا کد امنیتی را وارد کنید')"
                                        oninput="this.setCustomValidity('')" />
                                    <img src="https://141.ir/captcha/image?789949575"
                                        style="cursor: pointer; width: 180px;" title="به روزرسانی تصویر"
                                        class="captcha-image"
                                        onclick="this.setAttribute('src','https://141.ir/captcha/image?789949575?_='+Math.random());var captcha=document.getElementById('captcha');if(captcha){captcha.focus()}" />
                                </div>
                            </div>
                            <div class="code-container">
                                <button type="button" class="btn btn-primary phone-number-button">
                                    دریافت کد
                                </button>
                            </div>
                            <div class="alert alert-danger captcha-error-alert" role="alert">
                                لطفا کد امنیتی را به درستی وارد کنید
                            </div>
                            <div class="sms-code-container">
                                <label class="sms-code-label">کد پیامک شده:</label>
                                <input type="tel" maxlength="6" step="1" value="" inputmode="numeric" tabindex="3"
                                    id="verification-code" class="sms-code-input" required
                                    oninvalid="this.setCustomValidity('لطفا کد پیامک شده را وارد کنید')"
                                    oninput="this.setCustomValidity('')" />
                            </div>
                            <div class="alert alert-danger verify-code-error-alert" role="alert">
                                لطفا کد پیامک شده را به درستی وارد کنید
                            </div>
                            <div class="alert alert-danger server-error-alert" role="alert"></div>
                        </div>
                        <!-- <div class="footer-buttons">
              <button type="button" class="btn btn-secondary back-button">بازگشت</button>
              <button type="button" class="btn btn-info next-button">مرحله بعد</button>
            </div> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- submit shekayat --}}
    <div class="modal fade" id="problem-submit">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="assets/new_template/images/layer-icons/car.svg" />
                    <h4 class="modal-title">ثبت مشکلات مشاهده شده جاده ای</h4>
                    <button type="button" class="close" data-dismiss="modal">
                        &times;
                    </button>
                </div>
                <div class="modal-body">
                    <form>
                        <div class="form-group row">
                            <div class="col form-group">
                                <label>استان</label>
                                <input type="text" class="form-control  province-name" disabled>
                            </div>
                            <div class="col form-group">
                                <label>نام محور</label>
                                <input type="text" class="form-control mehvar-name" disabled>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <label>محدوده</label>
                                <input type="text" class="form-control area-name">
                            </div>
                            <div class="col">
                                <label>موضوع</label>
                                <select class="form-control subject-name">
                                    <option selected value="0">انتخاب موضوع</option>
                                    <option value="1">چاله و ناهمواری در سطح راه </option>
                                    <option value="2">پاکسازی سطح راه (لاشه حیوانات)</option>
                                    <option value="3">پاکسازی سطح راه (جسم خارجی)</option>
                                    <option value="4">پاکسازی سطح راه (سنگ ریزه)</option>
                                    <option value="5">پاکسازی سطح راه (گازوِیل و مواد روغنی)</option>
                                    <option value="6">ایمن سازی حفاظها و علائم آسیب دیده و ناایمن صف خودرو</option>
                                    <option value="7">نواقص چراغ چشمک زن</option>
                                    <option value="8">خودرو از کار افتاده</option>
                                    <option value="9">خرابی روسازی</option>
                                    <option value="10">زمین لرزه</option>
                                    <option value="11">نواقص روشنایی</option>
                                    <option value="12">ریزش کوه</option>
                                    <option value="13">سایر</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <label>نام و نام خانوادگی</label>
                                <input type="text" class="form-control first-lastname">
                            </div>
                            <div class="col">
                                <label>شماره همراه</label>
                                <input type="text" class="form-control phone-name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col">
                                <label>توضیحات<small class="guids_complain">با ذکر مبدا و مقصد جهت را مشخص کنید.(مثال :تهران به قم یا قم به تهران)</small></label>
                                <textarea type="text" class="form-control description"></textarea>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="ml-1 btn btn-primary btn-submit-shekayat">ثبت</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">انصراف</button>
                </div>
            </div>
        </div>
    </div>
    <!-- MAP -->
    <section>
        <div id="map" class="map"></div>
        <!-- <div id="routingButton" onclick="window.open('https://routingapi.rmto.ir/web/index.html' + location.search)">
      <img src="assets/new_template/images/routing-icon.png" alt="">
    </div> -->
        <!-- this will show our spinner -->
        {{-- <div hidden id="layer-spinner" class="spinner"></div> --}}
    </section>
    <!-- END MAP -->

    <!-- VERTICAL MENU -->
    <button type="button" class="ResponsiveVerticalMenu" onclick="RespVerticalMenu(this)">
        <img src="assets/new_template/images/icons/layerla.svg" alt="" />
    </button>
    
    <!--END VERTICAL MENU -->

    <!-- Coridor Page-->
    <div class="maincoridr">
        <button type="button" class="closeProvincialcoridor" onclick="closeProvincialcoridorBox()"></button>
        <div class="form-coridor">
            <h5 class="title-coridor">کریدور های اصلی</h5>
            <div class="box-coridor">
                <div class="testcor" id="firoozkooh">
                    <span class="span1"> مسیر تهران - قائم شهر</span>
                    <span class="colordot">..............</span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">..............</span>
                    <span class="span2">فیروزکوه</span>
                </div>

                <div class="testcor" id="haraz">
                    <span class="span1"> مسیر تهران - آمل </span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        هراز
                    </span>
                </div>
                <div class="testcor" id="tehran-chaloos">
                    <span class="span1"> مسیر تهران </span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        شمال
                    </span>
                </div>
                <div class="testcor" id="chaloos">
                    <span class="span1"> مسیر تهران - کرج</span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        چالوس
                    </span>
                </div>
                <div class="testcor" id="ghom">
                    <span class="span1"> تهران</span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        قم
                    </span>
                </div>
                <div class="testcor" id="save">
                    <span class="span1"> تهران</span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        ساوه
                    </span>
                </div>


                <div class="testcor" id="tehran-mashhad">
                    <span class="span1"> مسیر تهران </span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        مشهد
                    </span>
                </div>

                <div class="testcor" id="tehran-tabriz">
                    <span class="span1"> مسیر تهران </span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        تبریز
                    </span>
                </div>

                <div class="testcor" id="tehran-ghazvin">
                    <span class="span1"> مسیر تهران </span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        قزوین
                    </span>
                </div>

                <div class="testcor" id="ghazvin-rasht">
                    <span class="span1"> مسیر قزوین </span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        رشت
                    </span>
                </div>

                <div class="testcor" id="tehran-bandarabas">
                    <span class="span1"> مسیر تهران </span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        بندر عباس
                    </span>
                </div>

                <div class="testcor" id="tehran-esfahan">
                    <span class="span1"> مسیر تهران </span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        اصفهان
                    </span>
                </div>

                <div class="testcor" id="esfahan-shiraz">
                    <span class="span1"> مسیر اصفهان </span>
                    <span class="colordot">
                        ..............
                    </span>
                    <img src="./assets/new_template/images/icons/car-2.svg" />
                    <span class="colordot">
                        ..............
                    </span>
                    <span class="span2">
                        شیراز
                    </span>
                </div>

            </div>
        </div>
    </div>
    <!-- end Coridor Page -->

    <!-- TripTime Page-->
    <div class="tripTime">
        <button type="button" class="closeTripTime" onclick="closeTripTimeBox()"></button>
        <div class="form-triptime">
            <h5 class="title-triptime">زمان سفر</h5>
            <div class="box-triptime">
                <div class="triptimebox"></div>
            </div>
        </div>
    </div>
    <!-- End TripTime Page-->

    <!-- Provincial Camera -->
    <div class="provincialcamera">
        <button type="button" class="closeProvincialcamera" onclick="closeProvincialcameraBox()"></button>
        <div class="form-provincial">
            <h5>دوربین‌های نظارتی</h5>
            <span>برای مشاهده ‌دوربین‌های نظارتی استان مورد نظر خود را انتخاب
                کنید:</span>
            <select id="map-camera-select" name="map-camera-select" class="dropdownostan">
                <option value="00">انتخاب استان</option>
                <option value="01" provinceId="6" lat="37.9211202" long="46.6821517">آذربایجان شرقی</option>
                <option value="02" provinceId="5" lat="37.7416484" long="45.0207638">آذربایجان غربی</option>
                <option value="03" provinceId="7" lat="38.4583983" long="47.9313001">اردبیل</option>
                <option value="04" provinceId="22" lat="33.196733" long="50.7884216">اصفهان</option>
                <option value="32" provinceId="14" lat="35.9413239" long="52.504026">البرز</option>
                <option value="05" provinceId="21" lat="33.1545696" long="46.7576343">ایلام</option>
                <option value="06" provinceId="28" lat="28.8936645" long="51.3204877">بوشهر</option>
                <option value="07" provinceId="2" lat="35.50219455" long="51.3269098234951">تهران</option>
                <option value="08" provinceId="25" lat="32.0163307" long="50.685709">چهارمحال و بختیاری</option>
                <option value="29" provinceId="24" lat="33.0006776" long="58.3262335">خراسان جنوبی</option>
                <option value="30" provinceId="4" lat="35.756361" long="59.1280992">خراسان رضوی</option>
                <option value="31" provinceId="16" lat="37.5378855" long="56.9526137">خراسان شمالی</option>
                <option value="10" provinceId="26" lat="31.5535141" long="49.0077168">خوزستان</option>
                <option value="11" provinceId="9" lat="36.515854" long="48.4777616">زنجان</option>
                <option value="12" provinceId="17" lat="35.4380386" long="54.8626294">سمنان</option>
                <option value="13" provinceId="30" lat="28.1292481" long="60.8236848">سیستان و بلوچستان</option>
                <option value="14" provinceId="3" lat="29.299051" long="53.218456">فارس</option>
                <option value="28" provinceId="13" lat="36.0156291" long="49.8398161">قزوین</option>
                <option value="26" provinceId="18" lat="34.7191915" long="51.0122844">قم</option>
                <option value="16" provinceId="10" lat="35.672803" long="47.0124376">کردستان</option>
                <option value="15" provinceId="29" lat="29.571858" long="57.301047">کرمان</option>
                <option value="17" provinceId="11" lat="34.3789744" long="46.7010122">کرمانشاه</option>
                <option value="18" provinceId="27" lat="30.8143476" long="50.8661454">کهگیلویه و بویراحمد</option>
                <option value="27" provinceId="15" lat="37.1984436" long="55.070672">گلستان</option>
                <option value="19" provinceId="8" lat="37.5115476" long="49.3554429849707">گیلان</option>
                <option value="20" provinceId="20" lat="33.5372643" long="48.2435197">لرستان</option>
                <option value="21" provinceId="1" lat="36.3159159" long="51.8968597">مازندران</option>
                <option value="22" provinceId="19" lat="34.5302705" long="49.7864561">مرکزی</option>
                <option value="23" provinceId="31" lat="27.7198095" long="56.335807">هرمزگان</option>
                <option value="24" provinceId="12" lat="34.973321" long="48.6555779">همدان</option>
                <option value="25" provinceId="23" lat="32.2452686" long="55.105172">یزد</option>
            </select>
        </div>
        <div hidden id="camera-spinner" class="spinner"></div>
        <div class="form-map"></div>
    </div>
    <!-- END provincialcamera PAGE -->

    <!-- traffic online ostani -->
    <div class="onlinetraffic">
        <button type="button" class="closeonlinetrafic" onclick="closeTraficonlineBox()"></button>
        <div class="form-traficonline">
            <h5>ترافیک آنلاین استانی</h5>
            <span>برای مشاهده ترافیک آنلاین استان مورد نظر خود را انتخاب کنید:</span>
            <select id="map-online-select" name="map-online-select" class="dropdownostan">
                <option value="00">انتخاب استان</option>
                <option value="kol">کل کشور</option>
                <option value="01" provinceId="6" lat="37.9211202" long="46.6821517">آذربایجان شرقی</option>
                <option value="02" provinceId="5" lat="37.7416484" long="45.0207638">آذربایجان غربی</option>
                <option value="03" provinceId="7" lat="38.4583983" long="47.9313001">اردبیل</option>
                <option value="04" provinceId="22" lat="33.196733" long="50.7884216">اصفهان</option>
                <option value="32" provinceId="14" lat="35.9413239" long="52.504026">البرز</option>
                <option value="05" provinceId="21" lat="33.1545696" long="46.7576343">ایلام</option>
                <option value="06" provinceId="28" lat="28.8936645" long="51.3204877">بوشهر</option>
                <option value="07" provinceId="2" lat="35.50219455" long="51.3269098234951">تهران</option>
                <option value="08" provinceId="25" lat="32.0163307" long="50.685709">چهارمحال و بختیاری</option>
                <option value="29" provinceId="24" lat="33.0006776" long="58.3262335">خراسان جنوبی</option>
                <option value="30" provinceId="4" lat="35.756361" long="59.1280992">خراسان رضوی</option>
                <option value="31" provinceId="16" lat="37.5378855" long="56.9526137">خراسان شمالی</option>
                <option value="10" provinceId="26" lat="31.5535141" long="49.0077168">خوزستان</option>
                <option value="11" provinceId="9" lat="36.515854" long="48.4777616">زنجان</option>
                <option value="12" provinceId="17" lat="35.4380386" long="54.8626294">سمنان</option>
                <option value="13" provinceId="30" lat="28.1292481" long="60.8236848">سیستان و بلوچستان</option>
                <option value="14" provinceId="3" lat="29.299051" long="53.218456">فارس</option>
                <option value="28" provinceId="13" lat="36.0156291" long="49.8398161">قزوین</option>
                <option value="26" provinceId="18" lat="34.7191915" long="51.0122844">قم</option>
                <option value="16" provinceId="10" lat="35.672803" long="47.0124376">کردستان</option>
                <option value="15" provinceId="29" lat="29.571858" long="57.301047">کرمان</option>
                <option value="17" provinceId="11" lat="34.3789744" long="46.7010122">کرمانشاه</option>
                <option value="18" provinceId="27" lat="30.8143476" long="50.8661454">کهگیلویه و بویراحمد</option>
                <option value="27" provinceId="15" lat="37.1984436" long="55.070672">گلستان</option>
                <option value="19" provinceId="8" lat="37.5115476" long="49.3554429849707">گیلان</option>
                <option value="20" provinceId="20" lat="33.5372643" long="48.2435197">لرستان</option>
                <option value="21" provinceId="1" lat="36.3159159" long="51.8968597">مازندران</option>
                <option value="22" provinceId="19" lat="34.5302705" long="49.7864561">مرکزی</option>
                <option value="23" provinceId="31" lat="27.7198095" long="56.335807">هرمزگان</option>
                <option value="24" provinceId="12" lat="34.973321" long="48.6555779">همدان</option>
                <option value="25" provinceId="23" lat="32.2452686" long="55.105172">یزد</option>
            </select>
        </div>
        <div hidden id="onlinetraffic-spinner" class="spinner"></div>
        <div class="form-map"></div>
        {{-- <div class="switch-tile-containertrafficonline" onclick="showtraffickol()">
            <img class="imagekoll" src="assets/new_template/images/icons/traffic-lights.svg"
                title="نمایش ترافیک آنلاین استانی">
        </div> --}}
        {{-- cameback --}}
    </div>

    <!-- Graphical Traffic Map -->
    <div class="traffic-box">
        <button type="button" class="closeProvincialtraffic" onclick="closeProvincialtrafficBox()"></button>
        <div class="form-traffic-box">
            <h5> نقشه شماتیک ترافیک</h5>
            <span> برای مشاهده نقشه گرافیک استان مورد نظر خود کلیک کنید
            </span>
            <select id="map-traffic-select" name="map-traffic-select" class="dropdownostan">
                <option value="00">انتخاب استان</option>
                <option value="01" provinceId="6">آذربایجان شرقی</option>
                <option value="02" provinceId="5">آذربایجان غربی</option>
                <option value="03" provinceId="7">اردبیل</option>
                <option value="04" provinceId="22">اصفهان</option>
                <option value="32" provinceId="14">البرز</option>
                <option value="05" provinceId="21">ایلام</option>
                <option value="06" provinceId="28">بوشهر</option>
                <option value="07" provinceId="2">تهران</option>
                <option value="08" provinceId="25">چهارمحال و بختیاری</option>
                <option value="29" provinceId="24">خراسان جنوبی</option>
                <option value="30" provinceId="4">خراسان رضوی</option>
                <option value="31" provinceId="16">خراسان شمالی</option>
                <option value="10" provinceId="26">خوزستان</option>
                <option value="11" provinceId="9">زنجان</option>
                <option value="12" provinceId="17">سمنان</option>
                <option value="13" provinceId="30">سیستان و بلوچستان</option>
                <option value="14" provinceId="3">فارس</option>
                <option value="28" provinceId="13">قزوین</option>
                <option value="26" provinceId="18">قم</option>
                <option value="16" provinceId="10">کردستان</option>
                <option value="15" provinceId="29">کرمان</option>
                <option value="17" provinceId="11">کرمانشاه</option>
                <option value="18" provinceId="27">کهگیلویه و بویراحمد</option>
                <option value="27" provinceId="15">گلستان</option>
                <option value="19" provinceId="8">گیلان</option>
                <option value="20" provinceId="20">لرستان</option>
                <option value="21" provinceId="1">مازندران</option>
                <option value="22" provinceId="19">مرکزی</option>
                <option value="23" provinceId="31">هرمزگان</option>
                <option value="24" provinceId="12">همدان</option>
                <option value="25" provinceId="23">یزد</option>
            </select>
        </div>
        <!-- this will show our spinner -->
        <div hidden id="traffic-spinner" class="spinner"></div>
        <div class="form-map"></div>
    </div>

    <!-- The Modal -->
    <div id="provincialCameraModal" class="provincialcamera-modal">
        <div class="camera-overlay"></div>
        <!-- Modal content -->
        <div class="modal-content">
            <span class="close"></span>
            <div class="slideshow">
                <div id="slideshow-slide" class="slideshow-container">
                    <a class="prev">&#10095;</a>
                    <a class="next">&#10094;</a>
                </div>
                <br />
                <div id="slideshow-dot" style="text-align: center;"></div>
            </div>
        </div>
    </div>
    <div id="trafficPhotoModal" class="trafficphoto-modal">
        <div class="traffic-overlay"></div>
        <!-- Modal content -->
        <div class="modal-content">
            <span class="close"></span>
            <div class="photo">
                <img src="https://141.ir/wayonlinetraffic/28" />
            </div>
        </div>
    </div>

    <!-- The VMS Modal -->
    <div class="modal fade" id="Vmsmodal">
        <div class="modal-dialog modal-dialog-centered modal-lg">
            <div class="modal-content modal-css modalvms">
                <!-- Modal Header -->
                <div class="modal-header" id="header-vms">
                    <!-- <h4 class="modal-title">تابلوهای روا</h4> -->
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div id="lights"></div>
                    <div>
                        <div class="form-row">
                            <!-- <div class="form-group col-md-1">
                <div class="lightrow">
                  <div class="bulb rowvertical"></div>
                  <div class="bulb rowvertical"></div>
                  <div class="bulb rowvertical"></div>
                  <div class="bulb rowvertical"></div>
      

              </div>
              </div> -->
                            <div style="height:430px" class="form-group col-md-12 scroll-left" id="texyvms"></div>
                            <!-- <div class="form-group col-md-1">
                <div class="lightroww">
                  <div class="bulb rowvertical"></div>
                  <div class="bulb rowvertical"></div>
                  <div class="bulb rowvertical"></div>
                  <div class="bulb rowvertical"></div>
      

              </div>
              </div> -->
                        </div>
                    </div>
                    <div id="lightss"></div>
                    <!-- Modal footer -->
                </div>
            </div>
        </div>
    </div>

    <!-- Emergency Numbers -->
    <div class="emergencyNum">
        <h5>شماره تلفن‌های ضروری</h5>
        <span class="exitemergency"></span>
        <div class="form-emergnecynum">
            <div class="phoneContent">
                <ul>
                    <li>
                        <span>اطلاع رسانی راههای کشور</span>
                        <span>141</span>
                    </li>
                    <li>
                        <span>آتش نشانی</span>
                        <span>125</span>
                    </li>
                    <li>
                        <span>سازمان اورژانس کشور</span>
                        <span>115</span>
                    </li>
                    <li>
                        <span>ستاد خبری وزارت اطلاعات</span>
                        <span>113</span>
                    </li>
                    <li>
                        <span>اطلاعات پرواز فرودگاه امام خمینی</span>
                        <span>096330</span>
                    </li>
                    <li>
                        <span>پلیس</span>
                        <span>110</span>
                    </li>
                    <li>
                        <span>ندای امداد جمعیت هلال احمر</span>
                        <span>112</span>
                    </li>
                    <li>
                        <span>سازمان هواشناسی</span>
                        <span>134</span>
                    </li>
                    <li>
                        <span>ستاد مرکزی هماهنگی خدمات سفر</span>
                        <span>09629</span>
                    </li>

                    <li>
                        <span>اطلاعات راه آهن</span>
                        <span>1539</span>
                    </li>

                    <li>
                        <span>اطلاعات پروازهای کشور</span>
                        <span>096330</span>
                    </li>
                    <li>
                        <span>امداد خودرو سایپا </span>
                        <span>096550</span>
                    </li>
                    <li>
                        <span>امدادخودرو ایران خودرو</span>
                        <span>096440</span>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Emergency Numbers -->

    <!-- Related websites -->
    <div class="relatedWebsites">
        <h5>سایت‌های مرتبط</h5>
        <span class="exitewebsites"></span>
        <div class="form-relatedwebsites">
            <div class="websitesContent">
                <ul>
                    <li>
                        <a href="https://www.leader.ir/fa" target="_blank">پایگاه اطلاع رسانی دفتر مقام معظم رهبری</a>
                    </li>
                    <li>
                        <a href="http://www.rmto.ir/" target="_blank">سازمان راهداری و حمل و نقل جاده
                            ای</a>
                    </li>
                    <li>
                        <a href="https://www.mrud.ir" target="_blank">وزارت راه و شهرسازی</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- End Related websites -->

    <!-- Routing Icon -->
    <div class="routing-icon" onclick="openRoutingBox()">
        <img src="assets/new_template/images/icons/routing-icon.svg" width="27px" height="27px" />
    </div>

    <!-- Routing Box -->
    <div class="routing-box">
        <h5>مسیریابی</h5>
        <h3 id="textlan" style="font-size: 11px;display:none;color: #ff5630;">لطفا زبان سیستم را فارسی کنید </h3>
        <div class="routing-input-container">
            <div class="inputs-container">
                <img src="assets/new_template/images/icons/start-dot.png" style="float: right; margin-left: 15px;" />
                <input type="text" placeholder="مبدا" value="" class="origin-place" id="origin-input"
                    style="float: right;" autocomplete="off" />
                    <span class="remove-middle-point" style="float: right;" onclick="closeُStartPoint()">&#10005;</span>

            </div>
            <div class="middle-point-container">
                <img src="assets/new_template/images/icons/mid-dot.png" style="float: right; margin-left: 15px;" />
                <input type="text" placeholder="میانی" value="" class="middle-place" id="middle-input"
                    style="float: right;" autocomplete="off" />
                <span class="remove-middle-point" style="float: right;" onclick="closeMiddlePoint()">&#10005;</span>
            </div>
            <div class="inputs-container">
                <img src="assets/new_template/images/icons/end-dot.png" style="float: right; margin-left: 15px;" />
                <input type="text" placeholder="مقصد" value="" class="destination-place" id="destination-input"
                    style="float: right;" autocomplete="off" />
                    <span class="remove-middle-point" style="float: right;" onclick="closeEndPoint()">&#10005;</span>
            </div>
            <!-- <input type="button" class="btn btn-primary" id="route-button" onclick="routing()" value="مسیریابی"> -->
        </div>
        <div class="routing-options">
            <button class="reverse-route-button" onclick="reverseRoute()" type="button"
                title="جابجایی مبدا و مقصد"></button>
            <button class="add-route-button" onclick="addMiddlePoint()" type="button"
                title="افزودن نقطه میانی"></button>
        
          
        </div>
    </div>
    <!-- End Routing Box -->

    <!-- Announcement Box -->
    {{-- <div class="announcement-box">
    <button type="button" class="closeAnnouncementBox" onclick="closeAnnouncementBox()"></button>
    <img src="assets/new_template/images/coronavirus/coronavirus.jpg" width="200px"
      alt="اطلاعیه-ویروس کرونا-کرونا-قرنطینه" />
    <p>
      اطلاعات مربوط به مقابله با بیماری کرونا
    </p>
    <button class="showAnnouncement" onclick="window.open('/coronavirus' + location.search)">
      مشاهده صفحه
    </button>
  </div> --}}
    <!-- End Announcement Box -->
    <!-- routing custom Box -->
    <div class="routing-custom-box">
        <button type="button" class="closeCustomRoutingBox" onclick="closeCustomRoutingBox()"></button>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox" value="دوربین" name="cameraCheckBox" id="cameraCheckBox">
            <label for="cameraCheckBox" class="routingLabels">دوربین
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox" value="پمپ بنزین" name="gasCheckBox" id="gasCheckBox">
            <label for="gasCheckBox" class="routingLabels">پمپ بنزین
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox" value="راهدارخانه" name="rmtoCheckBox" id="rmtoCheckBox">
            <label for="rmtoCheckBox" class="routingLabels">راهدارخانه
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox markerCheckList" value="انسداد" name="blockageCheckBox"
                id="blockageCheckBox" data_box="3">
            <label for="blockageCheckBox" class="routingLabels">انسداد
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox markerCheckList" value="کارگاه جاده ای "
                name="workshopCheckBox" id="workshopCheckBox" data_box="4">
            <label for="workshopCheckBox" class="routingLabels">کارگاه جاده ای
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox markerCheckList" value="تصادفات " name="accidentCheckbox"
                id="accidentCheckbox" data_box="5">
            <label for="accidentCheckbox" class="routingLabels">تصادفات
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox markerCheckList" value="آب و هوا " name="weatherCheckbox"
                id="weatherCheckbox" data_box="6">
            <label for="weatherCheckbox" class="routingLabels">آب و هوا
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox markerCheckList" value="امداد خودرو " name="emdadCheckbox"
                id="emdadCheckbox" data_box="7">
            <label for="emdadCheckbox" class="routingLabels">امداد خودرو
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox markerCheckList" value="تعمیرگاه " name="CarFixCheckbox"
                id="CarFixCheckbox" data_box="8">
            <label for="CarFixCheckbox" class="routingLabels">تعمیرگاه
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox markerCheckList" value="خدمات " name="helpCheckbox"
                id="helpCheckbox" data_box="9">
            <label for="helpCheckbox" class="routingLabels">خدمات
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox markerCheckList" value="مساجد " name="mosqueCheckBox"
                id="mosqueCheckBox" data_box="10">
            <label for="mosqueCheckBox" class="routingLabels">مساجد
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox markerCheckList" value="بیمارستان " name="hospitalCheckbox"
                id="hospitalCheckbox" data_box="11">
            <label for="hospitalCheckbox" class="routingLabels">بیمارستان
        </div>
        <div class="flex-container">
            <input type="checkbox" class="routingCheckBox markerCheckList" value="تردد شمار " name="otfCheckbox"
                id="otfCheckbox" data_box="12">
            <label for="otfCheckbox" class="routingLabels">تردد شمار
        </div>
        <p>
            اطلاعات مربوط به مسیر درخواستی
        </p>

    </div>
    <!-- End routing custom Box -->

    <div class="helper-box">
        <p class="textDiv">راهنمای نقشه</p>
        <!-- <span class="TrafficDiv"><span>زیاد</span>
            <div class="showTrafficPic"></div><span>کم</span>
        </span> -->
        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #006400;"></i>
                جریان آزاد                
        </p>
        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #48C649;"></i>
                روان                
        </p>
        <p style="font-size:14px;margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #FFF000;"></i>
            نیمه روان               
        </p>
        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #ff8c00;"></i>
                نیمه سنگین               
        </p>
        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #A20A09;"></i>
            سنگین               
        </p>
        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #000000;"></i>
راه بندان                
        </p>
        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #ACACAC;"></i>
               نامشخص               
        </p>
    </div>
    <!-- legend for map  -->
    <div class="mapLegend">
        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #4572e0;"></i>
            آزادراه
        </p>
        <p style="font-size:14px;margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #48C649;"></i>
            بزرگراه
        </p>
        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #A20A09;"></i>
            راه اصلی
        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color:#FF5F1F;"></i>
            راه فرعی
        </p>

        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-circle" aria-hidden="true" style="font-size: 12px;color: #ACACAC;"></i>
            راه درون شهری
        </p>
        <p style="font-size:14px; margin-bottom: 5px;">
            <i class="fa fa-square-o" aria-hidden="true" style="font-size: 12px;"></i>
            مرکز استان
        </p>

    </div>
    <!-- legend for map  -->
    <!-- Change Tile Layers -->
    {{-- <div class="switch-tile-container" onclick="switchTileLayer()">
        <img src="assets/new_template/images/tile-icons/dark.jpg" title="حالت تاریک" />
    </div> --}}
    {{-- <div class="switch-tile-containertraffic" onclick="showtraffickol()">
        <img class="imagekoll" src="assets/new_template/images/icons/traffic-lights.svg"
            title="نمایش ترافیک آنلاین استانی">
    </div> --}}
    <!-- End Change Tile Layers -->

    <!--  Bottom MENU -->
    {{-- <footer>
        <div class="menu_bottom">
            <div class="socialMedia">
                <a href="http://witel.ir/" target="_blank">
                    <img alt="وایتل" src="assets/new_template/images/icons/witell.svg"
                        title="شرکت ویرا ارتباطات یکتا (وایتل)" />
                </a>

                <a href="https://www.instagram.com/141.ir/" target="_blank">
                    <img alt="اینستاگرام" src="assets/new_template/images/icons/instagram.svg" title="اینستاگرام" /></a>

                <a target="_blank" href="https://www.aparat.com/141.ir"><img alt="آیارات"
                        src="assets/new_template/images/icons/aparat.svg" title="آیارات" /></a>

                <!-- <a target="_blank" href="https://sapp.ir/141.ir"><img alt="سروش"
            src="assets/new_template/images/icons/soroush.svg" title="پیام رسان سروش"></a> -->
            </div>
            <div class="footerLinks">
                <a href="/aboutus">درباره ما</a>
                <a class="blink_me" href="/contactus">ثبت و پیگیری شکایات</a>
                <a href="/faq">پرسش های متداول</a>
            </div>
        </div>
        <div class="footer_licences d-flex justify-content-between px-3">
            <span>
                <span class="witelContent">
                    تمامی حقوق این وبسایت متعلق به
                    <a href="http://141.ir/" target="_blank"><span class="witel">
                            مرکز مدیریت راه های کشور </span></a>
                    می باشد</span>
                <span class="witelContentYear" id="currentYear"></span>
            </span>
            <span>مرز های بین المللی، استانی و حوزه شهری در این نقشه <Strong>سندیت ندارد</Strong></span>
        </div>
    </footer> --}}
    <!-- END Bottom MENU -->
    <script src="assets/new_template/js/fontawesome.js"></script>
    <script src="assets/new_template/js/jquery-3.4.1.min.js"></script>
    <script src="assets/new_template/js/bootstrap.min.js?v=0.2"></script>
    <script src="assets/new_template/js/leaflet.js"></script>
    <script src="assets/new_template/js/Control.Geocoder.js"></script>
    <script src="assets/new_template/js/leaflet-routing-machine.js"></script>
    <script src="assets/new_template/js/leaflet.markercluster.js"></script>
    <script src="assets/new_template/js/statics.js"></script>
    <script src="assets/new_template/js/moment.min.js?ver=0.1"></script>
    <script src="assets/new_template/js/moment-jalaali.js?ver=0.1"></script>
    <script src="assets/new_template/js/common.js?ver=0.1"></script>
    <script src="assets/new_template/js/sweetalert2js.js"></script>
    <script src="assets/new_template/js/Polyline.encode.js"></script>
    <script src="assets/new_template/js/index_ytel.js?ver=18"></script>

</body>

</html>
